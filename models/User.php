<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $password_hash
 * @property string $password
 * @property integer $status
 * @property string $email
 * @property integer $is_deletable
 * @property string $phone
 * @property int $market_id Точка продажи
 * @property int $gender Пол
 * @property string $birthday_date Дата рождения
 * @property string $created_at Дата создания
 * @property string $updated_at Дата изменения
 * @property string $role Роль
 *
 * @property Market $market
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const ROLE_ADMIN = 0;
    const ROLE_USER = 1;
    const ROLE_MANAGER = 2;

    public $password;
    public $password_repeat;

    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'name',
                'login',
                'is_deletable',
                'password',
                'password_hash',
                'market_id',
                'password_repeat',
                'created_at',
                'phone',
                'email',
                'gender',
                'role',
                'birthday_date'
            ],
            self::SCENARIO_EDIT => [
                'name',
                'login',
                'is_deletable',
                'password',
                'password_hash',
                'phone',
                'market_id',
                'updated_at',
                'phone',
                'email',
                'gender',
                'role',
                'birthday_date'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password', 'password_repeat'], 'required', 'on' => self::SCENARIO_DEFAULT],
//            ['login', 'match', 'pattern' => '/^[a-z]+([-_]?[a-z0-9]+){0,2}$/i', 'message' => '{attribute} должен состоять только из латинских букв и цифр'],
//            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/', 'message' => '{attribute} не соответствует всем параметрам безопасности'],
            [['login'], 'unique'],
            [['is_deletable', 'gender', 'role'], 'integer'],
            [['login', 'password_hash', 'password', 'name', 'phone', 'email', 'avatar'], 'string', 'max' => 255],
            [
                ['market_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Market::className(),
                'targetAttribute' => ['market_id' => 'id']
            ],
            [
                'password_repeat',
                'compare',
                'compareAttribute' => 'password',
                'message' => 'Пароль и повтор пароля не совпадают.'
            ],
            ['email', 'email'],
            ['birthday_date', 'date'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if ($uid == $this->id) {
            Yii::$app->session->setFlash('error',
                "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if ($this->is_deletable == false) {
            Yii::$app->session->setFlash('error',
                "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }

    }

    public static function getUsersName()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->created_at = date('Y-m-d H:i:s', time());
        } else {
            $this->updated_at = date('Y-m-d H:i:s', time());
        }

        if ($this->birthday_date) {
            $this->birthday_date = date('Y-m-d', strtotime($this->birthday_date));
        }

        if (parent::beforeSave($insert)) {

            if ($this->password != null) {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'ФИО',
            'password_hash' => 'Password Hash',
            'password' => 'Пароль',
            'password_repeat' => 'Повтор пароля',
            'status' => 'Статус',
            'email' => 'Email',
            'is_deletable' => 'Удаляемый',
            'phone' => 'Телефон',
            'market_id' => 'Точка продажи',
            'birthday_date' => 'Дата рождения',
            'gender' => 'Пол',
            'role' => 'Роль',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }

    /**
     * @return string
     */
    public function getRealAvatarPath()
    {
        return $this->avatar != null ? $this->avatar : 'img/nouser.png';
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(Market::className(), ['id' => 'market_id']);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getManagersList()
    {
        return ArrayHelper::map(self::findAll(['role' => self::ROLE_USER]), 'id', 'login');
    }

    public function getRoleLabels()
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_USER => 'Пользователь',
            self::ROLE_MANAGER => 'Менеджер'
        ];
    }

    public function getGenderLabels()
    {
        return [
            self::GENDER_MALE => 'Мужской',
            self::GENDER_FEMALE => 'Женский',
        ];
    }
}
