<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "finance".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property double $amount Сумма
 * @property int $type Тип
 * @property int $payment_type Способ оплаты
 * @property int $client_id Клиент
 * @property int $manager_id Менеджер
 * @property int $market_id Точка продаж
 * @property string $datetime Дата и время
 *
 * @property Client $client
 * @property User $manager
 * @property Market $market
 * @property Order $order
 */
class Finance extends ActiveRecord
{
    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'datetime',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'type', 'payment_type', 'client_id', 'manager_id', 'market_id'], 'integer'],
            [['amount'], 'number'],
            [['datetime'], 'safe'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_id' => 'id']],
            [['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => Market::className(), 'targetAttribute' => ['market_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'amount' => 'Сумма',
            'type' => 'Тип',
            'payment_type' => 'Способ оплаты',
            'client_id' => 'Клиент',
            'manager_id' => 'Менеджер',
            'market_id' => 'Точка продаж',
            'datetime' => 'Дата и время',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert){
            $this->datetime = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_CREDIT => 'Расход',
            self::TYPE_DEBIT => 'Доход',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(Market::className(), ['id' => 'market_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
