<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RemainSearch represents the model behind the search form about `app\models\Remain`.
 */
class RemainSearch extends Remain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'market_id', 'ingredient_id', 'has_in_storage'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Remain::find();

        $query->joinWith(['ingredient']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'market_id' => $this->market_id,
            'ingredient_id' => $this->ingredient_id,
            'amount' => $this->amount,
            'ingredient.has_in_storage' => $this->has_in_storage,
        ]);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andWhere(['market_id' => Yii::$app->user->identity->market_id]);
        }

        return $dataProvider;
    }
}
