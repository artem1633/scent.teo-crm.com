<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "aroma_group".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $parent_id Идентификатор родительской группы
 *
 * @property AromaGroup $parent
 * @property AromaGroup[] $aromaGroups
 * @property Perfume[] $perfumes
 */
class AromaGroup extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aroma_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => AromaGroup::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'parent_id' => 'Идентификатор родительской группы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(AromaGroup::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAromaGroups()
    {
        return $this->hasMany(AromaGroup::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfumes()
    {
        return $this->hasMany(Perfume::className(), ['aroma_group_id' => 'id']);
    }

    /**
     * Возвращает список групп
     * @param bool $only_parents Флаг, получить только родителей
     * @return array
     */
    public function getList($only_parents)
    {
        if ($only_parents){
            $query = AromaGroup::find()->andWhere(['parent_id' => null]);
        } else {
            $query = AromaGroup::find();
        }
        return ArrayHelper::map($query->all(), 'id', 'name');
    }
}
