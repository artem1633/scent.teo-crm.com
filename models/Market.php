<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "market".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $open_date Дата отрытия
 * @property string $city Город
 * @property string $supervisor Супервайзер
 * @property string $address Адрес
 *
 * @property User[] $managers
 */
class Market extends ActiveRecord
{
    /** @var string */
    public $managers;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'market';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city', 'supervisor', 'address'], 'string', 'max' => 255],
            ['open_date', 'date'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'open_date' => 'Дата открытия',
            'city' => 'Город',
            'supervisor' => 'Супервизор',
            'address' => 'Адрес',
            'managers' => 'Менеджеры',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->open_date){
            $this->open_date = date('Y-m-d', strtotime($this->open_date));
        }

        return parent::beforeSave($insert);
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagers()
    {
        return User::find()->andWhere(['role' => User::ROLE_MANAGER, 'market_id' => $this->id]);
    }

    public function getManagerList()
    {
        return ArrayHelper::map(User::find()->andWhere(['role' => User::ROLE_MANAGER, 'market_id' => $this->id])->all(),
            'id', 'name');
    }
}
