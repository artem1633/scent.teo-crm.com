<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'solvent', 'client_id', 'formula_id', 'manager_id', 'client_mark'], 'integer'],
            [['value', 'price', 'prime_cost'], 'number'],
            [['created_at', 'all_time'], 'safe'],
            [['client_name', 'formula_name', 'manager_name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = [
            'attributes' => [
                'all_time' => [
                    'asc' => ['all_time' => SORT_ASC],
                    'desc' => ['all_time' => SORT_DESC],
                ],
                'created_at',
                'client_id',
                'formula_id',
                'price',
                'prime_cost',
                'manager_id',
                'client_mark',
                'client_name',
                'formula_name',
                'manager_name',
            ],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'solvent' => $this->solvent,
            'value' => $this->value,
            'price' => $this->price,
            'client_id' => $this->client_id,
            'formula_id' => $this->formula_id,
            'prime_cost' => $this->prime_cost,
            'manager_id' => $this->manager_id,
            'client_mark' => $this->client_mark,
        ]);

        $query->andFilterWhere(['like', 'created_at', $this->created_at]);

        $query->joinWith('client')->andFilterWhere([
            'OR',
            ['LIKE', 'client.firstname', $this->client_name],
            ['LIKE', 'client.lastname', $this->client_name]
        ]);

        $query->joinWith('formula f')->andFilterWhere([
            'LIKE',
            'f.name',
            $this->formula_name
        ]);

        $query->joinWith('manager m')->andFilterWhere([
            'LIKE',
            'm.name',
            $this->manager_name
        ]);

        return $dataProvider;
    }
}
