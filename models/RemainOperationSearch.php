<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RemainOperation;

/**
 * RemainOperationSearch represents the model behind the search form about `app\models\RemainOperation`.
 */
class RemainOperationSearch extends RemainOperation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'remain_id', 'method', 'type', 'article_id', 'manager_id'], 'integer'],
            [['quantity', 'amount_last'], 'number'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RemainOperation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('remain');

        $query->andFilterWhere([
            'id' => $this->id,
            'remain_id' => $this->remain_id,
            'method' => $this->method,
            'type' => $this->type,
            'article_id' => $this->article_id,
            'quantity' => $this->quantity,
            'amount_last' => $this->amount_last,
            'datetime' => $this->datetime,
            'manager_id' => $this->manager_id,
        ]);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andWhere(['remain.market_id' => Yii::$app->user->identity->market_id]);
        }

        return $dataProvider;
    }
}
