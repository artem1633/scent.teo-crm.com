<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_ingredient".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property int $ingredient_id Ингредиент
 * @property double $dilute Процент разбавления
 * @property double $count Кол-во
 * @property int $is_main Основной ингредиент
 * @property string $comment Комментарий
 * @property int $can_delete Можно удалить из формулы
 * @property int $increase_value На сколько увеличиваем
 * @property int $decrease_value На сколько уменьшаем
 *
 * @property float $costPrice Себестоимость
 *
 * @property Ingredient $ingredient
 * @property Order $order
 */
class OrderIngredient extends ActiveRecord
{

    public $increase_value;
    public $decrease_value;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'ingredient_id', 'is_main', 'can_delete'], 'integer'],
            [['dilute', 'count', 'increase_value', 'decrease_value'], 'number'],
            [['comment'], 'string'],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'ingredient_id' => 'Ингредиент',
            'dilute' => 'Процент разбавления',
            'count' => 'Кол-во',
            'is_main' => 'Основной ингредиент',
            'comment' => 'Комментарий',
            'can_delete' => 'Можно удалить из формулы',
            'increase_value' => 'Увеличить на',
            'decrease_value' => 'Уменьшить на',
        ];
    }

    /**
     * @return float
     */
    public function getCostPrice()
    {
        return -1;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
