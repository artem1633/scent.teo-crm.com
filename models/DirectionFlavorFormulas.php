<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "direction_flavor_formulas".
 *
 * @property int $id
 * @property int $direction_flavor_id
 * @property int $formulas_id
 *
 * @property DirectionFlavor $directionFlavor
 * @property Formulas $formulas
 */
class DirectionFlavorFormulas extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'direction_flavor_formulas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['direction_flavor_id', 'formulas_id'], 'integer'],
            [['direction_flavor_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirectionFlavor::className(), 'targetAttribute' => ['direction_flavor_id' => 'id']],
            [['formulas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Formulas::className(), 'targetAttribute' => ['formulas_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'direction_flavor_id' => 'Direction Flavor ID',
            'formulas_id' => 'Formulas ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectionFlavor()
    {
        return $this->hasOne(DirectionFlavor::className(), ['id' => 'direction_flavor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulas()
    {
        return $this->hasOne(Formulas::className(), ['id' => 'formulas_id']);
    }
}
