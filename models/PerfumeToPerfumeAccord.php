<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "perfume_to_perfume_accord".
 *
 * @property int $id
 * @property int $perfume_id Парфюм
 * @property int $perfume_accord_id Аккорд
 * @property int $rate Позиция (относительно остальных аккордов)
 *
 * @property PerfumeAccord $perfumeAccord
 * @property Perfume $perfume
 */
class PerfumeToPerfumeAccord extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfume_to_perfume_accord';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perfume_id', 'perfume_accord_id', 'rate'], 'integer'],
            [['perfume_accord_id'], 'exist', 'skipOnError' => true, 'targetClass' => PerfumeAccord::className(), 'targetAttribute' => ['perfume_accord_id' => 'id']],
            [['perfume_id'], 'exist', 'skipOnError' => true, 'targetClass' => Perfume::className(), 'targetAttribute' => ['perfume_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perfume_id' => 'Парфюм',
            'perfume_accord_id' => 'Аккорд',
            'rate' => 'Позиция',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfumeAccord()
    {
        return $this->hasOne(PerfumeAccord::className(), ['id' => 'perfume_accord_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfume()
    {
        return $this->hasOne(Perfume::className(), ['id' => 'perfume_id']);
    }
}
