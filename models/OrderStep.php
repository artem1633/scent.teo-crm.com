<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_step".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property int $user_id Менеджер
 * @property int $step Этап
 * @property string $datetime Дата и время
 *
 * @property Order $order
 * @property User $user
 */
class OrderStep extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_step';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'datetime',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'user_id',
                'value' => Yii::$app->user->identity->getId()
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'step'], 'integer'],
            [['datetime'], 'safe'],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'user_id' => 'Менеджер',
            'step' => 'Этап',
            'datetime' => 'Дата и время',
        ];
    }

    public function beforeSave($insert)
    {
        $this->datetime = date('Y-m-d H:i:s', time());
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Получает веремя, затраченное на шаг заказа
     * @return int кол-во секунд
     */
    public function getDuration()
    {
        $start = strtotime($this->datetime);

        /** @var OrderStep $next_record */
        $next_record = self::find()->andWhere(['>=', 'step', ($this->step + 1)])
                ->andWhere(['order_id' => $this->order_id])
                ->andWhere(['>', 'id', $this->id])
                ->one() ?? null;

        if (!$next_record) {
            return 0;
        }

        $end = strtotime($next_record->datetime);

        Yii::info('Начало шага ' . $this->step . ': ' . $this->datetime, 'test');
        Yii::info('Конец шага' . $this->step . ': ' . $next_record->datetime, 'test');

        $diff = abs($end - $start);

        Yii::info('Разница (сек.): ' . $diff, 'test');

//        $hour = (int)(date('H', $diff));
//        Yii::info('Часов: ' . $hour, 'test');

        $min = (int)(date('i', $diff));
        Yii::info('Минут: ' . $min, 'test');

        $sec = (int)(date('s', $diff));
        Yii::info('Секунд: ' . $sec, 'test');

        $result = '';


//        if ((int)$hour) {
//            $result = $hour . ' ч.';
//        }

        if ($min) {
            $result .= ' ' . $min . ' мин.';
        }

        if ($sec) {
            $result .= ' ' . abs($sec) . ' сек.';
        }

        Yii::info('Итого получилось: ' . $result, 'test');


        return $result;
    }

    /**
     * Повторный шаг или нет
     * @return bool
     */
    public function isRepeated()
    {
        return self::find()
            ->andWhere(['step' => $this->step, 'order_id' => $this->order_id])
            ->andWhere(['<', 'id', $this->id])
            ->exists();
    }


}