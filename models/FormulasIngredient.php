<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "formulas_ingredient".
 *
 * @property int $id
 * @property int $formula_id Формула
 * @property int $ingredient_id Ингредиент
 * @property double $dilute Процент разбавления
 * @property double $count Кол-во
 * @property int $is_main Основной ингредиент
 * @property string $comment Комментарий
 * @property integer $can_delete Можно удалить из формулы
 * @property integer $has_in_storage
 *
 * @property Formulas $formula
 * @property Ingredient $ingredient
 */
class FormulasIngredient extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'formulas_ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['formula_id', 'ingredient_id', 'is_main', 'can_delete', 'has_in_storage'], 'integer'],
            [['dilute', 'count'], 'number'],
            [['comment'], 'string'],
            [['formula_id'], 'exist', 'skipOnError' => true, 'targetClass' => Formulas::className(), 'targetAttribute' => ['formula_id' => 'id']],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'formula_id' => 'Формула',
            'ingredient_id' => 'Ингредиент',
            'dilute' => 'Процент разбавления',
            'has_in_storage' => 'Есть на складе',
            'count' => 'Кол-во',
            'is_main' => 'Основной ингредиент',
            'can_delete' => 'Можно удалить из формулы',
            'comment' => 'Комментарий',
        ];
    }

    public function beforeSave($insert)
    {

        if ($this->dilute){
            $this->dilute = $this->dilute / 100;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $ingredient = $this->ingredient;
        $importantFlovers = $ingredient->ingredientImportantFlovers;

        foreach ($importantFlovers as $flover){
            $formulaImportantFlover = FormulasImportantFlover::findOne(['formulas_id' => $this->formula_id, 'important_flover_id' => $flover->important_flover_id]);
            if($formulaImportantFlover == null){
                $formulaImportantFlover = new FormulasImportantFlover(['formulas_id' => $this->formula_id, 'important_flover_id' => $flover->important_flover_id, 'formula_ingredient_id' => $this->id]);
                $formulaImportantFlover->save(false);
            }
        }

    }

    /**
     * @inheritdoc
     */
    public static function isMainLabels()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormula()
    {
        return $this->hasOne(Formulas::className(), ['id' => 'formula_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }
}
