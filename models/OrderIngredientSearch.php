<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrderIngredientSearch represents the model behind the search form about `app\models\OrderIngredient`.
 */
class OrderIngredientSearch extends OrderIngredient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'ingredient_id'], 'integer'],
            [['dilute', 'count'], 'number'],
            [['is_main', 'comment', 'can_delete'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderIngredient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['ingredient']);

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'ingredient_id' => $this->ingredient_id,
            'dilute' => $this->dilute,
            'count' => $this->count,
        ]);

        $query->andFilterWhere(['like', 'is_main', $this->is_main])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'can_delete', $this->can_delete]);

        return $dataProvider;
    }
}
