<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $step Этап
 * @property int $type Тип заказа
 * @property int $solvent Растворитель
 * @property double $value Объем
 * @property int $client_id Клиент
 * @property int $current_flavors_count Текущие кол-во ароматов
 * @property float $price Цена
 * @property int $style_flavor_id Стиль аромата
 * @property int $intensive_flavor_id Интенсивность аромата
 * @property int $season_id Сезон
 * @property int $direction_id Направление
 * @property int $formula_id Формула
 * @property int $client_mark Оценка клиента
 * @property string $mark_client_comment Комментарий клиента
 * @property int $manager_id Менеджер
 * @property int $market_id Точка продажи
 * @property int $like_flavor_flower_id Аромат каких цветов нравится
 * @property int $like_flavor_fruit_id Аромат каких фруктов нравится
 * @property int $like_flavor_spice_id Аромат каких специй нравится
 * @property int $like_flavor_drink_id Аромат каких напитков нравится
 * @property int $like_flavor_tree_id Аромат каких деревьев нравится
 * @property int $like_flavor_food_id Аромат каких еды нравится
 * @property int $nextHardStep
 * @property int $price_id Концентрация
 * @property float $costPrice Себестоимость
 * @property float $base_order_id Заказ на основании которого создается новый заказ
 * @property int $next_step Номер следующего шага
 * @property int $step_time Время начала шага
 * @property int $created_at Время создания заказа
 * @property int $ended_at Время завершения заказа
 * @property int $all_time Общее время заказа
 * @property int $prime_cost Себестоиость
 * @property int $client_name Фамилия имя клиента
 * @property int $formula_name Наименование формулы
 * @property int $manager_name Имя менеджера
 * @property int $important_flavor Что важно при выборе аромата
 *
 * @property Finance[] $finances
 * @property Client $client
 * @property Formulas $formula
 * @property Market $market
 * @property IntensiveFlavor $intensiveFlavor
 * @property string $scenarioByStep
 * @property User $manager
 * @property Season $season
 * @property DirectionFlavor $direction
 */
class Order extends ActiveRecord
{

    public $likeFlavorFlower;
    public $likeFlavorFruit;
    public $likeFlavorSpice;
    public $likeFlavorDrink;
    public $likeFlavorTree;
    public $likeFlavorFood;
    public $notLikeFlavor;

    public $likePerfume;
//    public $importantFlavor;
    public $base_order_id;


    public $all_time;
    public $prime_cost;
    public $client_name;
    public $formula_name;
    public $manager_name;


    const TYPE_NEW = 1;
    const TYPE_REPEAT = 2;

    const SOLVENT_TYPE_ETHANOL = 0;
    const SOLVENT_TYPE_DPG = 1;
    const SOLVENT_TYPE_WAX = 2;

    const CLIENT_SEX_MALE = 0;
    const CLIENT_SEX_FEMALE = 1;

    //Действия для логирования
    const ACTION_FILL = 1;
    const ACTION_DELETE = 2;
    const ACTION_UPDATE = 3;
    const ACTION_NOTHING = 4;

    const SCENARIO_START = 'scenario_start';
    const SCENARIO_CLIENT = 'scenario_client';
    const SCENARIO_FLOWER = 'scenario_flower';
    const SCENARIO_INGREDIENTS = 'scenario_ingredients';
    const SCENARIO_TASTING = 'scenario_tasting';
    const SCENARIO_MIXING_AND_TASTING = 'scenario_mixing_and_tasting';
    const SCENARIO_CORRECT = 'scenario_correct';
    const SCENARIO_PREPARATION = 'scenario_preparation';
    const SCENARIO_MARK = 'scenario_mark';
    const SCENARIO_HISTORY = 'scenario_history';
    const SCENARIO_FINISH = 'scenario_finish';

    //Важность при выборе ароммата
    const IMPORTANCE_NOTHING = 0;
    const IMPORTANCE_BRAND = 1;
    const IMPORTANCE_TAIL = 2;
    const IMPORTANCE_INTENSIVE = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'manager_id',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_START => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'price_id',
                'value',
                'base_order_id',
            ],
            self::SCENARIO_CLIENT => [
                'step',
                'client_id',
            ],
            self::SCENARIO_FLOWER => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'likeFlavorFlower',
                'likeFlavorFruit',
                'likeFlavorSpice',
                'likeFlavorDrink',
                'likeFlavorTree',
                'likeFlavorFood',
                'notLikeFlavor',
                'likePerfume',
                'current_flavors_count',
                'style_flavor_id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
                'important_flavor',
            ],
            self::SCENARIO_INGREDIENTS => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'current_flavors_count',
                'style_flavor_ id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
//                'formula_id',
            ],
            self::SCENARIO_TASTING => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'current_flavors_count',
                'style_flavor_ id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
                'formula_id',
            ],
            self::SCENARIO_CORRECT => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'current_flavors_count',
                'style_flavor_ id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
                'formula_id',
            ],
            self::SCENARIO_MIXING_AND_TASTING => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'current_flavors_count',
                'style_flavor_ id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
                'formula_id',
            ],
            self::SCENARIO_PREPARATION => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'current_flavors_count',
                'style_flavor_ id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
                'formula_id',
            ],
            self::SCENARIO_MARK => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'current_flavors_count',
                'style_flavor_ id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
                'formula_id',
                'client_mark',
                'mark_client_comment'
            ],
            self::SCENARIO_HISTORY => [
                'step',
                'nextHardStep',
                'type',
                'solvent',
                'client_id',
                'current_flavors_count',
                'style_flavor_ id',
                'intensive_flavor_id',
                'season_id',
                'direction_id',
                'formula_id',
                'client_mark',
                'mark_client_comment'
            ],
            self::SCENARIO_FINISH => [],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['type', 'solvent', 'client_id', 'price_id', 'base_order_id', 'next_step'],
                'integer'
            ],
            [['value', 'price'], 'number'],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                [
                    'current_flavors_count',
                    'style_flavor_id',
                    'intensive_flavor_id',
                    'season_id',
                    'direction_id',
                ],
                'required',
//                'on' => self::SCENARIO_FLOWER
            ],
            [
                ['current_flavors_count', 'style_flavor_id', 'intensive_flavor_id', 'season_id', 'direction_id'],
                'integer'
            ],

            [['formula_id'], 'required', 'message' => 'Необходимо выбрать формулу'],
            [['formula_id'], 'integer'],

            [['client_mark'], 'required'],
            [['client_mark'], 'integer'],
            [['mark_client_comment'], 'string'],

            [
                ['manager_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['manager_id' => 'id']
            ],

            [
                [
                    'likeFlavorFlower',
                    'likeFlavorFruit',
                    'likeFlavorSpice',
                    'likeFlavorDrink',
                    'likeFlavorTree',
                    'likeFlavorFood'
                ],
                'safe'
            ],

            [['step', 'nextStepHard'], 'safe'],
            [['value', 'price_id'], 'required'],
//            ['client_id', 'required', 'on' => self::SCENARIO_CLIENT]
            [
                [
                    'likePerfume',
                    'likeFlavorFlower',
                    'likeFlavorFruit',
                    'likeFlavorSpice',
                    'likeFlavorDrink',
                    'likeFlavorTree',
                    'likeFlavorFood',
                    'notLikeFlavor',
                    'important_flavor',
                ],
                'required'
            ],
            [
                ['type'],
                function ($attribute, $params) {
                    if ($this->step == 1) {
                        if ($this->type == self::TYPE_REPEAT && !$this->client_id) {
                            $this->addError('client_id', 'Для перехода к следующему шагу выберите клиента');
                        }

                        if ($this->type == self::TYPE_REPEAT && !$this->base_order_id) {
                            $this->addError('base_order_id', 'Для перехода к следующему шагу выберите заказ клиента');
                        }
                    }
                }
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип заказа',
            'solvent' => 'Растворитель',
            'value' => 'Объем',
            'client_id' => 'Клиент',
//            'clientName' => 'Имя',
//            'clientLastName' => 'Фамилия',
//            'clientEmail' => 'Email',
//            'clientSex' => 'Пол',
//            'clientPhone' => 'Телефон',
//            'clientDateBirth' => 'Дата рождения',
//            'clientHasAllergic' => 'Аллергия',
//            'clientAllergicDesc' => 'Описание Аллергии',
            'current_flavors_count' => 'Сколько ароматов в вашей коллекции сейчас',
            'style_flavor_id' => 'Стиль аромата',
            'intensive_flavor_id' => 'Интенсивность аромата',
            'season_id' => 'Сезон',
            'formula_id' => 'Формула',
            'direction_id' => 'Направление',
            'client_mark' => 'Оценка клиента',
            'mark_client_comment' => 'Комментарий клиента',
            'price' => 'Цена',
            'manager_id' => 'Менеджер',
            'market_id' => 'Точка продажи',
            'likePerfume' => 'Парфюмы которые нравятся',
//            'importantFlavor' => 'Что для вас важно при выборе аромата',
            'likeFlavorFlower' => 'Ароматы каких цветов нравится',
            'likeFlavorFruit' => 'Ароматы каких фруктов нравится',
            'likeFlavorSpice' => 'Ароматы каких специй нравится',
            'likeFlavorDrink' => 'Ароматы каких напитков нравится',
            'likeFlavorTree' => 'Ароматы каких деревьев нравится',
            'likeFlavorFood' => 'Ароматы какой еды нравятся',
            'notLikeFlavor' => 'Ароматы которые не нравятся',
            'price_id' => 'Прайс',
            'created_at' => 'Создан',
            'ended_at' => 'Завершен',
            'all_time' => 'Время исполнения',
            'prime_cost' => 'Себестоимость',
            'client_name' => 'Клиент',
            'formula_name' => 'Формула',
            'manager_name' => 'Менеджер',
            'important_flavor' => 'Что для вас важно при выборе аромата',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_NEW => 'Новый парфюм',
            self::TYPE_REPEAT => 'Повторный'
        ];
    }

    /**
     * @return array
     */
    public static function actionsLabels()
    {
        return [
            self::ACTION_FILL => 'Заполнено',
            self::ACTION_DELETE => 'Удалено',
            self::ACTION_UPDATE => 'Изменено',
            self::ACTION_NOTHING => 'Пропущено',
        ];
    }

    /**
     * @return array
     */
    public static function solventLabels()
    {
        return [
            self::SOLVENT_TYPE_ETHANOL => 'Этанол',
            self::SOLVENT_TYPE_DPG => 'Дипропиленгликоль',
            self::SOLVENT_TYPE_WAX => 'Воск'
        ];
    }

    /**
     * @return array
     */
    public static function sexLabels()
    {
        return [
            self::CLIENT_SEX_MALE => 'Мужской',
            self::CLIENT_SEX_FEMALE => 'Женский',
        ];
    }

    /**
     * @return array
     */
    public static function steps()
    {
        return [
            1 => self::SCENARIO_START,
            2 => self::SCENARIO_CLIENT,
            3 => self::SCENARIO_FLOWER,
            4 => self::SCENARIO_INGREDIENTS,
            5 => self::SCENARIO_TASTING,
            6 => self::SCENARIO_MIXING_AND_TASTING,
            7 => self::SCENARIO_CORRECT,
            8 => self::SCENARIO_PREPARATION,
            9 => self::SCENARIO_MARK,
            10 => self::SCENARIO_HISTORY,
            11 => self::SCENARIO_FINISH,
        ];
    }

    /**
     * @return array
     */
    public function stepLabels()
    {
        return [
            1 => 'Общая информация',
            2 => 'Анкета клиента',
            3 => 'Анкета аромата',
            4 => 'Дегустация ингредиентов',
            5 => 'Подбор формулы',
            6 => 'Смешивание и дегустация',
            7 => 'Корректировка аромата',
            8 => 'Приготовление аромата',
            9 => 'Оценка',
            10 => 'История заказа',
            11 => 'Завершение'
        ];
    }

    public static function getScenarioIndex($scenario)
    {
        $steps = self::steps();

        for ($i = 0; $i < count($steps); $i++) {
            $scnr = $steps[$i];
            if ($scenario == $scnr) {
                return $i;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        if ($this->isNewRecord) {
            $this->market_id = $identity->market_id;
            $this->created_at = date('Y-m-d H:i:s', time());
        } else {
            if ($this->step == 10) {
                if (!$this->ended_at) {
                    //Сохраняем вермя завершения заказа
                    $this->ended_at = date('Y-m-d H:i:s', time());
                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function nextStep()
    {
        Yii::info('This Next Step function', 'test');
        $this->scenario = $this->steps()[$this->step];

        switch ($this->scenario) {
            case self::SCENARIO_START:
                if ($this->type == self::TYPE_REPEAT && !$this->client_id) {
                    //Если выбран повторный заказ и не указан клиент
                    $this->step = 1;
                    $this->next_step = 2;
                } elseif ($this->type == self::TYPE_REPEAT && !$this->base_order_id) {
                    //Если повторный заказ и не выбран базовый заказ
                    $this->step = 1;
                    $this->next_step = 2;
                } elseif ($this->base_order_id) {
                    //Если выбран базовый заказ переходим к восьмому шагу (Приготовление аромата)
                    $this->copyOrder();
                    $this->step = 8;
                    $this->next_step = 9;
                } else {
                    $this->step = 2;
                    $this->next_step = 3;
                };
                break;
            case self::SCENARIO_CLIENT:
                $client_model = Client::findOne($this->client_id) ?? null;

                if (!$client_model) $client_model = new Client();

                //Сохраняем клиента
                if ($client_model->load(Yii::$app->request->post()) && $client_model->save()) {
                    Yii::info('Client saved success', 'test');
                    $this->client_id = $client_model->id;
                } else {
                    if ($client_model->hasErrors()) {
                        Yii::error($client_model->errors, '_error');
                    } else {
                        Yii::warning('Данные клиента не получены', 'test');
                    }
                }
                $this->step = 3;
                $this->next_step = 4;
                break;
            case  self::SCENARIO_FLOWER:
                //Шаг 3 Анкета аромата
                Yii::info('Шаг 3 Анкета аромата', 'test');

                OrderImportantFlover::deleteAll(['order_id' => $this->id]);

                $fields = $this->getTypeToFieldList();

                //Сохраняем значения полей
                foreach ($fields as $type => $field) {
                    if ($this->$field != null) {

//                        Yii::info($field . ' start', 'test');

                        foreach ($this->$field as $flavor) {

//                            Yii::info($flavor, 'test');

                            if ($flavor != 0) {
                                $importantFlavor = new OrderImportantFlover([
                                    'order_id' => $this->id,
                                    'important_flover_id' => $flavor,
                                    'type' => $type,
                                ]);
                                $importantFlavor->save(false);
                            }
                        }
                    }
                }

                OrderPerfume::deleteAll([
                    'order_id' => $this->id,
                ]);

                if ($this->likePerfume != null) {
                    foreach ($this->likePerfume as $perfume) {
                        if ($perfume != 0) {
                            $orderPerfume = new OrderPerfume([
                                'order_id' => $this->id,
                                'perfume_id' => $perfume
                            ]);
                            $orderPerfume->save(false);
                        }
                    }
                    Yii::info('OrderPerfume finish', 'test');
                }

                if ($this->client_id != null) {
                    $sex = $this->client->male;
                } else {
                    $sex = null;
                }

                $formulasSearchModel = new FormulasSearch();
                $formulasSearchModel->sex = $sex;
                $formulasSearchModel->searchComplicated($this);

                $ingredients = $formulasSearchModel->ingredients;
                Yii::info('Найденные ингредиенты:', 'test');
                Yii::info(json_decode(json_encode($ingredients), true), 'test');

                if ($ingredients != null) {
                    $counter = 1;
                    foreach ($ingredients as $ingredient) {
                        if (!OrderFormulasIngredient::find()
                            ->andWhere([
                                'order_id' => $this->id,
                                'formulas_ingredient_id' => $ingredient->id
                            ])->exists()) {
                            //Если ингридиент не найден, сохраняем
                            $model = new OrderFormulasIngredient([
                                'order_id' => $this->id,
                                'formulas_ingredient_id' => $ingredient->id,
                                'mark' => $counter,
                            ]);
                            $model->save(false);
                            $counter++;
                        }

                    }
                }

                if ($this->hasErrors()) {
                    $this->step = 3;
                    $this->next_step = 4;
                } else {
                    $this->step = 4;
                    $this->next_step = 5;
                }
                break;
            case self::SCENARIO_INGREDIENTS:
                //Шаг 4 Дегустация ингредиентов
                Yii::info('Шаг 4 Дегустация ингредиентов', 'test');

                /** @var OrderFormulasIngredient $top */
                $top = OrderFormulasIngredient::find()->where(['order_id' => $this->id])->orderBy('mark asc')->one();
                if ($top == null) {
                    return false;
                }
                $this->formula_id = $top->formulasIngredient->formula_id;
//                $parentFormula = Formulas::findOne($top->formulasIngredient->formula_id);
//                $formula = new Formulas();
//                $formula->attributes = $parentFormula->attributes;
//                foreach ($parentFormula->formulasIngredients )
                Yii::info($this->attributes, 'test');
                Yii::info('Формула: ' . $this->formula_id, 'test');

                $this->step = 5;
                $this->next_step = 6;
                break;
            case self::SCENARIO_TASTING:
                //Шаг 5. Подбор формулы
                Yii::info('Шаг 5. Подбор формулы', 'test');

                $formula = $this->formula ?? null;

                if (!$formula) {
//                    $this->addError('formula_id', 'Не выбрана формула');
                    $this->step = 5;
                    $this->next_step = 6;
                    break;
                }

                //Пишем ингредиенты выбранной формулы в order_ingredient
                Yii::info('Выбрана формула: ' . $formula->name, 'test');

                //Удаляем ингридиенты заказа, если есть
                OrderIngredient::deleteAll(['order_id' => $this->id]);

                foreach ($formula->ingredients as $ingredient) {
                    Yii::info('Сохраняем ингредиент: ' . $ingredient->name_rus, 'test');

                    //Добавляем
                    $order_ingredient = new OrderIngredient();
                    $order_ingredient->order_id = $this->id;
                    $order_ingredient->ingredient_id = $ingredient->id;

                    $formula_ingredients = FormulasIngredient::find()
                            ->andWhere(['formula_id' => $formula->id, 'ingredient_id' => $ingredient->id])
                            ->one() ?? null;

                    Yii::info($formula_ingredients->attributes, 'test');

                    /** @var FormulasIngredient $formula_ingredients */
                    if ($formula_ingredients) {
                        $order_ingredient->dilute = $formula_ingredients->dilute;
                        $order_ingredient->count = $formula_ingredients->count;
                        $order_ingredient->is_main = $formula_ingredients->is_main;
                        $order_ingredient->comment = $formula_ingredients->comment;
                        $order_ingredient->can_delete = $formula_ingredients->can_delete;
                    }

                    if (!$order_ingredient->save()) {
                        Yii::error($order_ingredient->errors, '_error');
                    }
                }

                $this->step = 6;
                $this->next_step = 7;
                break;
            case self::SCENARIO_MIXING_AND_TASTING:
                // Шаг 6 Смешивание и дегустация
                Yii::info('Шаг 6. Смешивание и дегустация', 'test');

                $this->step = 7;
                $this->next_step = 8;
                break;
            case self::SCENARIO_CORRECT:
                //Шаг 7 Корректировка аромата
                Yii::info('Шаг 7. Корректировка аромата', 'test');

                $this->step = 8;
                $this->next_step = 9;
                break;
            case self::SCENARIO_PREPARATION:
                //Шаг 8 Приготовление аромата
                Yii::info('Шаг 8 Приготовление аромата', 'test');

                $ingredients = OrderIngredient::find()->where(['order_id' => $this->id])->all();
                /** @var OrderIngredient $ingredient */
                foreach ($ingredients as $ingredient) {
                    /** @var Remain $remain */
                    $remain = Remain::find()->where([
                        'market_id' => $this->manager->market_id,
                        'ingredient_id' => $ingredient->ingredient_id
                    ])->one();

                    if ($remain) {
                        $remain->amount = $remain->amount - $ingredient->count;
                        $remain->save(false);

                        $operation = new RemainOperation([
                            'remain_id' => $remain->id,
                            'method' => RemainOperation::METHOD_ORDER,
                            'type' => RemainOperation::TYPE_WRITE_OFF,
                            'quantity' => $ingredient->count,
                            'amount_last' => $remain->amount,
                            'order_id' => $this->id,
                        ]);

                        $operation->save(false);
                    }
                }

                $cost = $this->costPrice;

                $finance = new Finance([
                    'order_id' => $this->id,
                    'amount' => $cost,
                    'type' => Finance::TYPE_DEBIT,
                    'client_id' => $this->client_id,
                    'manager_id' => $this->manager_id,
                    'market_id' => $this->manager->market_id,
                ]);
                $finance->save(false);
                $this->step = 9;
                $this->next_step = 10;
                break;
            case self::SCENARIO_MARK:
                Yii::info('Шаг 9. Оценка', 'test');
                $this->step = 10;
                $this->next_step = 10;
                break;
            case self::SCENARIO_HISTORY:
                $this->step = 10;
                $this->next_step = 10;
                break;
        }

        Yii::info('Current scenario: ' . $this->scenario, 'test');
        Yii::info('This End Next Step function', 'test');

        return true;
    }

    public function isCurrentLastStep()
    {
//        return $this->step != null ? ($this->step == (count(self::steps()[$this->step]))-1) : false;
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinances()
    {
        return $this->hasMany(Finance::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormula()
    {
        return $this->hasOne(Formulas::className(), ['id' => 'formula_id']);
    }

    /**
     * @return float
     */
    public function getCostPrice()
    {
        $sum = 0;

        /** @var OrderIngredient[] $orderIngredients */
        $orderIngredients = OrderIngredient::find()->where(['order_id' => $this->id])->all();

        foreach ($orderIngredients as $orderIngredient) {
            $ingredient = $orderIngredient->ingredient;

            $sum += $ingredient->const_price * $orderIngredient->count;
        }

        return $sum;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(Market::className(), ['id' => 'market_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirection()
    {
        return $this->hasOne(DirectionFlavor::className(), ['id' => 'direction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntensiveFlavor()
    {
        return $this->hasOne(IntensiveFlavor::className(), ['id' => 'intensive_flavor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Season::className(), ['id' => 'season_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStyleFlavor()
    {
        return $this->hasOne(StyleFlavor::className(), ['id' => 'style_flavor_id']);
    }

    public function getPrice()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id']);
    }

    /**
     * Расчитывает сумму заказа
     */
    public function calcPrice()
    {
        Yii::info($this->attributes, 'test');

        if (!$this->price_id || !$this->value) {
            return [
                'success' => 0,
                'data' => 'Для рачета выберите объем и концентрацию'
            ];
        }

        /** @var PriceCost $price_cost */
        $price_cost = PriceCost::find()
                ->andWhere([
                    'price_id' => $this->price_id,
                    'type' => $this->type,
                    'volume' => $this->value
                ])->one() ?? null;

        if (!$price_cost) {
            return [
                'success' => 0,
                'data' => 'Ошибка расчета цены! Услуга в прайсе не найдена'
            ];
        }

        return [
            'success' => 1,
            'data' => $price_cost->cost,
        ];

    }

    /**
     * Копирует данные из базового заказа в основной
     * @return bool
     */
    public function copyOrder()
    {
        Yii::info('Копируем базовый заказ', 'info');

        //копируем сам заказ
        $base_order = Order::findOne($this->base_order_id)->attributes ?? null;

        if (!$base_order) {
            Yii::error('Базовый заказ не найден, копирование отменено', '_error');
            return null;
        }
        $base_order['id'] = null;

        foreach ($base_order as $key => $value) {
            if (!ArrayHelper::getValue($this->toArray(), $key)) {
                //Если в текущем заказе поле не заполнено, присваиваем значение из базового заказа
                $this->$key = $value;
            }
        }

        if (!$this->save()) {
            Yii::error($this->errors, '_error');
        }

        //Копируем всё остальное из связанных таблиц


        /** @var array $table_names Наименования таблиц, которые связаны с заказом */
        $table_names = ['order_perfume', 'order_ingredient', 'order_important_flover', 'order_formulas_ingredient'];

        //Перебираем все таблицы в которых нужно копировать данные заказа
        /** @var string $table_name */
        foreach ($table_names as $table_name) {
            Yii::info('Таблица: ' . $table_name, 'test');

            $class_name = $this->getClassName($table_name);

            //Получаем все записи таблицы для базовго заказа
            /** @var ActiveQuery $items */
            $items = $class_name::find()->andwhere(['order_id' => $this->base_order_id]);

            //Перебираем все найденые строки базового заказа
            /** @var Model $item */
            foreach ($items->each() as $item) {
                Yii::info('Модель: ', 'test');

                $attributes = $item->attributes;

                Yii::info($attributes, 'test');

                /** @var ActiveRecord $new_model */
                $new_model = new $class_name;

                //Перебираем все поля строки базового заказа
                /** @var int|string $attribute Значение поля */
                /** @var string $key наименование поля */
                foreach ($attributes as $key => $attribute) {
                    if ($key == 'id') {
                        //Если поле - это id, то пропускаем
                        continue;
                    } elseif ($key == 'order_id') {
                        //Если order_id переписываем поле новым заказом
                        $new_model->$key = $this->id;
                    } else {
                        //Сохраняем старое значение поля в новую запись
                        $new_model->$key = $item->$key;
                    }
                }

                //Сохраняем данные
                if (!$new_model->save()) {
                    Yii::error($new_model->errors, '_error');
                }
            }
        }


        return true;
    }

    private function getClassName($table_name)
    {
        $className = str_replace('_', ' ', $table_name);
        $className = ucwords($className);
        $className = str_replace(' ', '', $className);
        $className = 'app\models\\' . $className;

        Yii::info('Класс: ' . $className, 'test');

        return $className;
    }

    /**
     * Получает сценарий сохранения для модели
     * @return mixed|null|string
     */
    public function getScenarioByStep()
    {
        $scenario = self::steps()[$this->step] ?? null;

        Yii::info($scenario, 'test');
        return $scenario;
    }

    /**
     * Возвращает общее время потраченное на заказ
     * @return string
     */
    public function getAllTime()
    {
        Yii::info('getAllTime() function', 'test');
        $diff = strtotime($this->ended_at) - strtotime($this->created_at);
//        Yii::info('$diff = ' . $diff, 'test');
//        Yii::info(strtotime($this->ended_at), 'test');
//        Yii::info($this->ended_at, 'test');
//        Yii::info(strtotime($this->created_at), 'test');
//        Yii::info($this->created_at, 'test');
//        Yii::info($diff, 'test');
        Yii::info('start: ' . $this->created_at, 'test');
        Yii::info('end: ' . $this->ended_at, 'test');
        Yii::info('разница: ' . $diff, 'test');

        $result = (int)($diff / 60) . ' мин.';

        return $result;
    }

    /**
     * Получает общую стоимость ингридиентов
     * @return array
     */
    public function getIngredientCost()
    {
        $order_ingredients = OrderIngredient::find()->andWhere(['order_id' => $this->id]);

        $ingredients = [];
        $total_cost = 0;

        /** @var OrderIngredient $order_ingredient */
        foreach ($order_ingredients->each() as $order_ingredient) {
            /** @var Ingredient $ingredient */
            $ingredient = $order_ingredient->ingredient;

            $volume = $order_ingredient->count;
            if ($ingredient->cost_price_value) {
                $cost = round($order_ingredient->count / $ingredient->cost_price_value * $ingredient->const_price, 2);
            } else {
                $cost = round($order_ingredient->count * $ingredient->const_price, 2);
            }

            $ingredients[$ingredient->name_rus] = [
                'volume' => $volume . ' мл',
                'cost' => $cost . ' руб.',
            ];
            $total_cost += $cost;
        }

        return [
            'all' => $total_cost . ' руб.',
            'ingredients' => $ingredients,
        ];
//        return [
//            'all' => '55 руб',
//            'ingredients' => [
//                'Ингредиент 1' => [
//                    'volume' => '1 мл',
//                    'cost' => '5 руб',
//                ],
//                'Ингредиент 2' => [
//                    'volume' => '2 мл',
//                    'cost' => '10 руб',
//                ],
//            ]
//        ];
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'id');
    }

    /**
     * Возвращает связанные значения типа аромата и имя переменной
     * @return array
     */
    private function getTypeToFieldList()
    {
        return [
            OrderImportantFlover::TYPE_FLOWER => 'likeFlavorFlower',
            OrderImportantFlover::TYPE_FRUIT => 'likeFlavorFruit',
            OrderImportantFlover::TYPE_SPICE => 'likeFlavorSpice',
            OrderImportantFlover::TYPE_DRINK => 'likeFlavorDrink',
            OrderImportantFlover::TYPE_TREE => 'likeFlavorTree',
            OrderImportantFlover::TYPE_FOOD => 'likeFlavorFood',
            OrderImportantFlover::TYPE_NOT_LIKE => 'notLikeFlavor',
        ];
    }

    /**
     * Получает идентификаторы запахов из анкеты клиента, присваивает переменным для вывода в третьем шаге заказа (Анкета ароматов)
     * @return void
     */
    public function prepareQuestionnaireFlavors()
    {
        $fields = $this->getTypeToFieldList();

        foreach ($fields as $type => $field) {
            //Получаем набор запахов по каждому направлению
            $this->$field = OrderImportantFlover::find()
                ->andWhere([
                    'type' => $type,
                    'order_id' => $this->id
                ])
                ->select('important_flover_id')
                ->asArray()
                ->column();
        }

        Yii::info('End prepare flavors', 'test');
        Yii::info($this->attributes, 'test');

    }

    /**
     * Получает идентификаторы парфюмов из анкеты клиента для вывода в третьем шаге заказа (Анкета ароматов)
     */
    public function prepareLikePerfume()
    {
        $like_perfume = OrderPerfume::find()
            ->andWhere(['order_id' => $this->id])
            ->select('perfume_id')
            ->asArray()
            ->column();

        Yii::info('Любимые парфюмы', 'test');
        Yii::info($like_perfume, 'test');

        $this->likePerfume = $like_perfume;
    }

    public function importantLabels()
    {
        return [
            0 => '',
            self::IMPORTANCE_NOTHING => 'Не выбрано',
            self::IMPORTANCE_BRAND => 'Бренд',
            self::IMPORTANCE_TAIL => 'Шлейф',
            self::IMPORTANCE_INTENSIVE => 'Стойкость',
        ];
    }

    /**
     * Сохраняет время начала шага
     * @return bool
     */
    public function loggingStep()
    {
        Yii::info('loggingStep() function', 'test');

        if ($this->step > 9) {
            if (!$this->ended_at) {
                Yii::info('Завершение заказа не указано. Указываем время.', 'test');
                $this->ended_at = date('Y-m-d H:i:s', time());
            }
            $this->step = 10;
            if (!$this->save(false)) {
                Yii::error($this->errors, '_error');
            }
        }

        //Проверяем записан или нет последний шаг
        $exist = OrderStep::find()->andWhere(['order_id' => $this->id])->andWhere(['>', 'step', 9])->exists();
        if ($exist) {
            Yii::info('Шаг 10 уже имеется. Пропускаем', 'test');

            //Если 10 шаг уже есть в логах - пропускаем запись
            return false;
        }


        Yii::info('Добавляем в лог. Шаг: ' . $this->step, 'test');

        $order_step = new OrderStep();
        $order_step->order_id = $this->id;
        $order_step->user_id = $this->manager_id;
        $order_step->step = $this->step;
        if (!$order_step->save()) {
            Yii::error($order_step->errors, '_error');
        }

        return true;
    }

    public function setStepInfo2($current_step)
    {
        Yii::info('setStepInfo() function', 'test');
        Yii::info('$current_step: ' . $current_step, 'test');

        $session = Yii::$app->session;

        //Если текущий шаг - это первый шаг, то предыдущий шаг равен первому шагу
        $current_step > 1 ? $prev_step = $current_step - 1 : $prev_step = $current_step;

        //Если текущий шаг последний шаг - следующий шаг равен последнему
        $current_step == 10 ? $next_step = 10 : $next_step = $current_step + 1;

        $session->set('current_step', $current_step);
        $session->set('next_step', $next_step);
        $session->set('prev_step', $prev_step);

        Yii::info('Session:', 'test');
        Yii::info($session, 'test');

    }


    //    public function nextStep()
//    {
//        Yii::info('Next Step', 'test');
//        if ($this->step == null) {
//            $this->scenario = self::SCENARIO_START;
////            return false;
//        }
//
//        if ($this->validate() == false) {
//            Yii::info('Validate False', 'test');
//
////            var_dump($this->errors);
////            exit;
//
//            if ($this->nextHardStep != null) {
//                $this->step = $this->nextHardStep;
//                $this->save(false);
//                return true;
//            }
//            return false;
//        } else {
//            Yii::info('Validate True', 'test');
//
//            if ($this->isCurrentLastStep()) {
//                Yii::info('isCurrentLastStep', 'test');
//                // echo 'ok';
//                // exit;
//
//                // VarDumper::dump($this, 10, true);
//
//                return $this->save(false);
//            } else {
//                Yii::info($this->toArray(), 'test');
//
//                $this->step = self::getScenarioIndex($this->scenario);
//                $this->scenario = self::steps()[$this->step];
//
//                if ($this->scenario == self::SCENARIO_START) {
//                    $this->client_id = $this->old_client_id;
//                    $price = $this->calcPrice();
//                    if ($price['success'] == 1) {
//                        $this->price = $price['data'];
//                    } else {
//                        $this->price = 0;
//                    }
//
//                    if ($this->type == self::TYPE_REPEAT) {
//                        $this->nextHardStep = 6;
//                    }
//                }
//
//                if ($this->scenario == self::SCENARIO_CLIENT && $this->type == self::TYPE_NEW) {
//                    Yii::info('Type New and Scenario Client', 'test');
//
//                    if ($this->client_id === null) {
//                        $client = new Client();
//                    } else {
//                        $client = Client::findOne($this->client_id);
//                    }
//
//                    if ($client == null) {
//                        $client = new Client();
//                    }
//
//                    $client->firstname = $this->clientName;
//                    $client->lastname = $this->clientLastName;
//                    $client->email = $this->clientEmail;
//                    $client->birthday = $this->clientDateBirth;
//                    $client->phone = $this->clientPhone;
//                    $client->allergy = $this->clientHasAllergic;
//                    $client->allergy_name = $this->clientAllergicDesc;
//                    $client->save(false);
//
//                    $this->client_id = $client->id;
//                }
//
//                if ($this->scenario == self::SCENARIO_TASTING) {
//                    OrderIngredient::deleteAll(['order_id' => $this->id]);
//
//                    /** @var FormulasIngredient[] $ingredients */
//                    $ingredients = FormulasIngredient::find()->where(['formula_id' => $this->formula_id])->all();
//
//                    foreach ($ingredients as $ingredient) {
//                        $orderIngredient = new OrderIngredient();
//                        $orderIngredient->order_id = $this->id;
//                        $orderIngredient->ingredient_id = $ingredient->ingredient_id;
//                        $orderIngredient->dilute = $ingredient->dilute;
//                        $orderIngredient->count = $ingredient->count;
//                        $orderIngredient->is_main = $ingredient->is_main;
//                        $orderIngredient->comment = $ingredient->comment;
//                        $orderIngredient->can_delete = $ingredient->can_delete;
//                        $orderIngredient->save(false);
//                    }
//                }
//
//                if ($this->scenario == self::SCENARIO_FLOWER) {
//                    OrderImportantFlover::deleteAll(['order_id' => $this->id]);
//                    if ($this->likeFlavorFlower != null) {
//                        foreach ($this->likeFlavorFlower as $flavor) {
//                            $importantFlover = new OrderImportantFlover([
//                                'order_id' => $this->id,
//                                'important_flover_id' => $flavor,
//                                'type' => OrderImportantFlover::TYPE_FLOWER,
//                            ]);
//                            $importantFlover->save(false);
//                        }
//                    }
//                    if ($this->likeFlavorTree != null) {
//                        foreach ($this->likeFlavorTree as $flavor) {
//                            $importantFlover = new OrderImportantFlover([
//                                'order_id' => $this->id,
//                                'important_flover_id' => $flavor,
//                                'type' => OrderImportantFlover::TYPE_TREE,
//                            ]);
//                            $importantFlover->save(false);
//                        }
//                    }
//                    if ($this->likeFlavorSpice != null) {
//                        foreach ($this->likeFlavorSpice as $flavor) {
//                            $importantFlover = new OrderImportantFlover([
//                                'order_id' => $this->id,
//                                'important_flover_id' => $flavor,
//                                'type' => OrderImportantFlover::TYPE_SPICE,
//                            ]);
//                            $importantFlover->save(false);
//                        }
//                    }
//                    if ($this->likeFlavorFood != null) {
//                        foreach ($this->likeFlavorFood as $flavor) {
//                            $importantFlover = new OrderImportantFlover([
//                                'order_id' => $this->id,
//                                'important_flover_id' => $flavor,
//                                'type' => OrderImportantFlover::TYPE_FOOD,
//                            ]);
//                            $importantFlover->save(false);
//                        }
//                    }
//                    if ($this->likeFlavorDrink != null) {
//                        foreach ($this->likeFlavorDrink as $flavor) {
//                            $importantFlover = new OrderImportantFlover([
//                                'order_id' => $this->id,
//                                'important_flover_id' => $flavor,
//                                'type' => OrderImportantFlover::TYPE_DRINK,
//                            ]);
//                            $importantFlover->save(false);
//                        }
//                    }
//                    if ($this->likeFlavorFruit != null) {
//                        foreach ($this->likeFlavorFruit as $flavor) {
//                            $importantFlover = new OrderImportantFlover([
//                                'order_id' => $this->id,
//                                'important_flover_id' => $flavor,
//                                'type' => OrderImportantFlover::TYPE_FRUIT,
//                            ]);
//                            $importantFlover->save(false);
//                        }
//                    }
//                    if ($this->notLikeFlavor != null) {
//                        foreach ($this->notLikeFlavor as $flavor) {
//                            $importantFlover = new OrderImportantFlover([
//                                'order_id' => $this->id,
//                                'important_flover_id' => $flavor,
//                                'type' => OrderImportantFlover::TYPE_NOT_LIKE,
//                            ]);
//                            $importantFlover->save(false);
//                        }
//                    }
//
//
//                    OrderPerfume::deleteAll(['order_id' => $this->id]);
//                    if ($this->likePerfume != null) {
//                        foreach ($this->likePerfume as $perfume) {
//                            $orderPerfume = new OrderPerfume([
//                                'order_id' => $this->id,
//                                'perfume_id' => $perfume
//                            ]);
//                            $orderPerfume->save(false);
//                        }
//                    }
//                }
//
//                if ($this->scenario == self::SCENARIO_FLOWER) {
//                    if ($this->client_id != null) {
//                        $sex = $this->client->male;
//                    } else {
//                        $sex = null;
//                    }
//
//                    $formulasSearchModel = new FormulasSearch();
//                    $formulasSearchModel->sex = $sex;
//                    $formulasSearchModel->searchComplicated($this);
//
//                    $ingredients = $formulasSearchModel->ingredients;
//
//                    if ($ingredients != null) {
//                        $counter = 1;
//                        foreach ($ingredients as $ingredient) {
//                            $model = new OrderFormulasIngredient([
//                                'order_id' => $this->id,
//                                'formulas_ingredient_id' => $ingredient->id,
//                                'mark' => $counter,
//                            ]);
//                            $model->save(false);
//                            $counter++;
//                        }
//                    }
//                }
//
//                if ($this->scenario == self::SCENARIO_INGREDIENTS) {
//                    /** @var OrderFormulasIngredient $top */
//                    $top = OrderFormulasIngredient::find()->where(['order_id' => $this->id])->orderBy('mark asc')->one();
////
////                if($top == null){
////                    return false;
////                }
//
////                $this->formula_id = $top->formulasIngredient->formula_id;
//                    $this->formula_id = 6;
//                }
//
//                if ($this->scenario == self::SCENARIO_PREPARATION) {
//                    $ingredients = OrderIngredient::find()->where(['order_id' => $this->id])->all();
//
//                    foreach ($ingredients as $ingredient) {
//                        /** @var Remain $remain */
//                        $remain = Remain::find()->where([
//                            'market_id' => $this->manager->market_id,
//                            'ingredient_id' => $ingredient->ingredient_id
//                        ])->one();
//
//                        if ($remain) {
//                            $remain->amount = $remain->amount - $ingredient->count;
//                            $remain->save(false);
//
//                            $operation = new RemainOperation([
//                                'remain_id' => $remain->id,
//                                'method' => RemainOperation::METHOD_ORDER,
//                                'type' => RemainOperation::TYPE_WRITE_OFF,
//                                'quantity' => $ingredient->count,
//                                'amount_last' => $remain->amount,
//                                'order_id' => $this->id,
//                            ]);
//
//                            $operation->save(false);
//                        }
//                    }
//
//                    $cost = $this->costPrice;
//
//                    $finance = new Finance([
//                        'order_id' => $this->id,
//                        'amount' => $cost,
//                        'type' => Finance::TYPE_DEBIT,
//                        'client_id' => $this->client_id,
//                        'manager_id' => $this->manager_id,
//                        'market_id' => $this->manager->market_id,
//                    ]);
//                    $finance->save(false);
//                }
//
//                if ($this->scenario == self::SCENARIO_MIXING_AND_TASTING) {
//                    $this->nextHardStep = 5;
//                }
//
//                if ($this->nextHardStep != null) {
//                    $this->step = $this->nextHardStep;
//                }
//
//                Yii::info($this->attributes, 'test');
//
//                $this->save(false);
//
//                $step = new OrderStep([
//                    'order_id' => $this->id,
//                    'step' => $this->step,
//                ]);
//                $step->save(false);
//            }
//        }
//        // if($this->isCurrentLastStep()){
//        // return $this->save(false);
//        // } else {
//
//
////        $this->scenario = self::steps()[$this->step];
////         $this->step = $this->step+1;
////        $this->save(false);
////        exit;
//        // $this->scenario = self::typeSteps()[$this->format][$this->step];
//        // return false;
//        // }
//
//        return true;
//    }
}
