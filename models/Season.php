<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "season".
 *
 * @property int $id
 * @property string $name
 */
class Season extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'season';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @param string $seasons Сезоны. Разделитель "|"
     * @return string
     */
    public function getNames($seasons)
    {
        $ids = explode('|', $seasons);

        return implode(', ', self::find()->andWhere(['IN', 'id', $ids])->select('name')->column());
    }

    /**
     * Получает все сезоны
     * @return array
     */
    public function getList()
    {
        return ArrayHelper::map(Season::find()->all(), 'id', 'name');
    }

    /**
     * Получает наименования сезонов из строки формата 2|4|5|6
     * @param $seasons
     * @return string
     */
    public function getSeasons($seasons)
    {
        $season_ids = explode('|', $seasons);

        $arr_seasons = [];

        foreach ($season_ids as $id){
            $arr_seasons[] = Season::findOne($id)->name ?? null;
        }

        return implode(', ', $arr_seasons);
    }
}
