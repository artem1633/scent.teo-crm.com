<?php

namespace app\models;

use app\models\query\ChangeLogQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;

/**
 * This is the model class for table "change_log".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property int $step Раздел (номер)
 * @property string $field Поле
 * @property int $action Действие
 * @property string $old_value Старое значение
 * @property string $new_value Новое значение
 * @property string $event_time Время события
 *
 * @property Order $order
 */
class ChangeLog extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'change_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'step', 'action'], 'integer'],
            [['old_value', 'new_value'], 'string'],
            [['event_time'], 'safe'],
            [['field'], 'string', 'max' => 255],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'step' => 'Раздел (номер)',
            'field' => 'Поле',
            'action' => 'Действие',
            'old_value' => 'Старое значение',
            'new_value' => 'Новое значение',
            'event_time' => 'Время события',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return ChangeLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChangeLogQuery(get_called_class());
    }

    /**
     * Запись лога событий
     * @param array $post_data данные из POST запроса
     * @return array
     */
    public function log($post_data)
    {
        Yii::info($post_data, 'test');

        Yii::$app->response->format = Response::FORMAT_JSON;

        $order_id = $post_data['order_id'];
        $step = $post_data['step'];

        //Полуаем имя поля
        try {
            preg_match('/\[([\s\S]+?)]/', $post_data['name'], $matches);
            $name_field = $matches[1];
        } catch (\Exception $e) {
            Yii::warning('Логирование. Имя поля не найдено', 'test');
            return ['success' => false, 'data'=>['Логирование. Имя поля не найдено']];
        }

        //Получаем имя класса
        try {
            preg_match('/((.+?))\[/', $post_data['name'], $matches);
            $class_name = $matches[2];
        } catch (\Exception $e) {
            Yii::warning('Логирование. Имя класса не найдено', 'test');
            return ['success' => false, 'data'=>['Логирование. Имя класса не найдено']];
        }


        Yii::info('Name field: ' . $name_field, 'test');
        Yii::info('Class name: ' . $class_name, 'test');

        $old_value = $post_data['old_value'] ?? null;
        $new_value = $post_data['new_value'] ?? null;

        $log = new ChangeLog();

        $class_name = 'app\models\\' . $class_name;
        //Получаем лейбл для поля
        $log->field = (new $class_name())->attributeLabels()[$name_field];
        $log->order_id = $order_id;
        $log->old_value = $old_value;
        $log->new_value = $new_value;
        $log->step = $step;

        if ($log->new_value === null) {
            //Если новое значение вообще не пришло - значит удаление
            $log->action = Order::ACTION_DELETE;
        } elseif ($log->new_value && !$log->old_value) {
            //Если старого значения нет, а новое есть
            $log->action = Order::ACTION_FILL;
        } elseif ($log->old_value === $log->new_value) {
            //Если старое значение совпадает с новым
            $log->action = Order::ACTION_NOTHING;
        } elseif ($log->old_value !== $log->new_value) {
            //Если старое значение не совпадает с новым
            $log->action = Order::ACTION_UPDATE;
        }

        Yii::info($log->attributes, 'test');

        if (!$log->save()){
            Yii::error($log->errors, '_error');
            return ['success' => false, 'data' => ['Ошибка записи: ' . implode($log->errors)]];
        }

        return ['success' => true];
    }
}
