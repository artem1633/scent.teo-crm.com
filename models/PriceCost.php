<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "price_cost".
 *
 * @property int $id
 * @property int $price_id Прайс
 * @property int $type Тип (новое создание/повторное создание)
 * @property int $volume Объем, (мл)
 * @property double $cost Цена
 * @property int $currency Валюта
 * @property array $types Типы
 * @property array $volumes Объемы
 * @property array $costs цены
 *
 * @property Price $price
 */
class PriceCost extends ActiveRecord
{
    const TYPE_NEW_CREATION = 1;
    const TYPE_RE_CREATION = 2;

    /** @var array */
    public $types;

    /** @var array */
    public $volumes;

    /** @var array */
    public $costs;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_cost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_id', 'type', 'volume', 'currency'], 'integer'],
            [['cost'], 'number'],
            [
                ['price_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Price::className(),
                'targetAttribute' => ['price_id' => 'id']
            ],
            [['types', 'volumes', 'costs'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_id' => 'Прайс',
            'type' => 'Тип',
            'volume' => 'Объем, (мл)',
            'cost' => 'Цена',
            'currency' => 'Валюта',
            'types' => 'Тип',
            'volumes' => 'Объем, (мл)',
            'costs' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id']);
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return [
            self::TYPE_NEW_CREATION => 'Новое создание',
            self::TYPE_RE_CREATION => 'Повторное создание',
        ];
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->getTypes()[$this->type];
    }

    /**
     * @param $price_id
     * @return ActiveRecord[]
     */
    public function getByType($price_id)
    {
        return self::find()
            ->andWhere(['type' => $this->type])
            ->andWhere(['price_id' => $price_id])
            ->all() ?? null;
    }

    /**
     * @return array
     */
    public static function getVolumesList()
    {
        $query = self::find()
            ->select('volume')
            ->asArray()
            ->groupBy('volume');


        $list_volume = [];
        foreach ($query->each() as $item){
            $volume = $item['volume'];
            \Yii::info($volume, 'test');

            $list_volume[$volume] = $volume . ' мл.';
        }

        return $list_volume;
    }
}
