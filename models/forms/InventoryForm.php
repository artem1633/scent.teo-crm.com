<?php

namespace app\models\forms;

use app\models\Inventory;
use app\models\Remain;
use app\models\RemainOperation;
use Yii;
use yii\base\Model;

/**
 * Class InventoryForm
 * @package app\models\forms
 */
class InventoryForm extends Model
{
    const SCENARIO_INGREDIENTS = 'ingredients';

    /**
     * @var int
     */
    public $marketId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var array
     */
    public $remains;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        if(Yii::$app->user->identity->isSuperAdmin()){
            return [
                self::SCENARIO_DEFAULT => ['marketId', 'name', 'comment'],
                self::SCENARIO_INGREDIENTS => ['marketId', 'name', 'comment', 'remains'],
             ];
        } else {
            return [
                self::SCENARIO_DEFAULT => ['name', 'comment'],
                self::SCENARIO_INGREDIENTS => ['name', 'comment', 'remains'],
            ];
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marketId', 'name', 'comment'], 'required'],
            [['marketId'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['comment'], 'string'],
            [['remains'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'marketId' => 'Точка продаж',
            'name' => 'Наименование',
            'comment' => 'Комментарий',
            'remains' => 'Новые остатки'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $this->marketId = Yii::$app->user->identity->market_id;
        }
    }

    /**
     *
     */
    public function invent()
    {
        $inventory = new Inventory([
            'market_id' => $this->marketId,
            'name' => $this->name,
            'comment' => $this->comment,
        ]);
        $inventory->save();

        foreach ($this->remains as $remainId => $amount)
        {
            $remain = Remain::findOne($remainId);

            if($remain){
                $newAmount = $amount - $remain->amount;

                if($newAmount > 0 ){
                    $operation = new RemainOperation([
                        'remain_id' => $remainId,
                        'method' => RemainOperation::METHOD_INVENTORY,
                        'type' => RemainOperation::TYPE_COMING,
                        'quantity' => abs($newAmount),
                        'amount_last' => $remain->amount,
                        'inventory_id' => $inventory->id,
                    ]);

                    $operation->save(false);

//                    $inventoryRemainOperation = new InventoryRemainOperation([
//                        'inventory_id' => $inventory->id,
//                        'remain_operation_id' => $operation->id,
//                    ]);
//                    $inventoryRemainOperation->save(false);
                } else if($newAmount < 0) {
                    $operation = new RemainOperation([
                        'remain_id' => $remainId,
                        'method' => RemainOperation::METHOD_INVENTORY,
                        'type' => RemainOperation::TYPE_WRITE_OFF,
                        'quantity' => abs($newAmount),
                        'amount_last' => $remain->amount,
                        'inventory_id' => $inventory->id,
                    ]);

                    $operation->save(false);

//                    $inventoryRemainOperation = new InventoryRemainOperation([
//                        'inventory_id' => $inventory->id,
//                        'remain_operation_id' => $operation->id,
//                    ]);
//                    $inventoryRemainOperation->save(false);
                }

                $remain->amount += $newAmount;
                $remain->save(false);
            }
        }
    }


}