<?php

namespace app\models\forms;

use app\models\Developer;
use app\models\GroupIngredient;
use app\models\ImportantFlover;
use app\models\Ingredient;
use app\models\LevelTriangleFlavor;
use app\models\SubstanceType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class IngredientImportForm extends Model
{
    public $file;

    private $developers;

    private $triangleLevels;

    private $substanceTypes;

    private $importantFlovers;

    private $groups;

    public $skipCount = 0;

    public $savedCount = 0;

    public $totalCount = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->developers = ArrayHelper::map(Developer::find()->all(), 'id', 'name');
        $this->triangleLevels = ArrayHelper::map(LevelTriangleFlavor::find()->all(), 'id', 'name');
        $this->substanceTypes = ArrayHelper::map(SubstanceType::find()->all(), 'id', 'name');
        $this->importantFlovers = ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name');
        $this->groups = ArrayHelper::map(GroupIngredient::find()->all(), 'id', 'name');

    }

    public function rules()
    {
        return [
            [['file'], 'required'],
            ['file', 'file'],
        ];
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function import()
    {
        $file = UploadedFile::getInstance($this, 'file');
        $path = $file->tempName;

        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = IOFactory::load($path);

        $sheet = $spreadsheet->getActiveSheet();
        foreach ($sheet->getRowIterator() as $row) {

            if ($row->getRowIndex() == 1) {
                continue;
            }

            $cells = $row->getCellIterator();

            $model = new Ingredient();

            foreach ($cells as $cell) {
                if ($cell->getColumn() == "A") {
                    $model->substance_type_id = $this->getSubstanceTypeFromCell($cell);
                }
//                if ($cell->getColumn() == "B") {
//                    $model->importantFlovers = $this->getImportantFloverFromCell($cell);
//                }
                if ($cell->getColumn() == "C") {
                    $model->name_rus = $cell->getValue();
                }
                if ($cell->getColumn() == "D") {
                    $model->cas = $cell->getValue();
                }
                if ($cell->getColumn() == "E") {
                    $model->levelTriangleFlavors = $this->getTriangleLevelFromCell($cell);
                }
                if ($cell->getColumn() == "F") {
                    $model->developer_id = $this->getDeveloperFromCell($cell);
                }
                if ($cell->getColumn() == "G") {
                    $model->const_price = $cell->getValue();
                }
                if ($cell->getColumn() == "H") {
                    $model->cost_price_value = $cell->getValue();
                }
                if ($cell->getColumn() == "I") {
                    $model->input_level = (int)$cell->getValue() / 100;
                }
                if ($cell->getColumn() == "J") {
                    $model->substantivity = $cell->getValue();
                }
                if ($cell->getColumn() == "K") {
                    $model->description = $cell->getValue();
                }
                if ($cell->getColumn() == "L") {
                    $model->description_additional = $cell->getValue();
                }
                if ($cell->getColumn() == "M") {
                    //Группа ингредиента
                    $model->groups = $this->getGroupsFromCell($cell);
                }
            }

            $this->totalCount++;
            if ($model->save(false)) {
                $this->savedCount++;
            } else {
                $this->skipCount--;
            }
        }

        return true;
    }

    /**
     * @param $cell
     * @return string
     */
    private function getDeveloperFromCell($cell)
    {
        $name = $cell->getValue();

        if (in_array($name, array_values($this->developers)) == false) {
            $developer = new Developer(['name' => $name]);
            $developer->save(false);
            $this->developers[$developer->id] = $developer->name;
        } else {
            $developer = Developer::find()->where(['name' => $name])->one();
        }

        return $developer->id;
    }

    private function getTriangleLevelFromCell($cell)
    {
        \Yii::info($this->triangleLevels, 'test');

        $names = explode('/', $cell->getValue());
        $pks = [];

        foreach ($names as $name) {
//            $name = str_replace('.', '', $name);
            $name = trim($name);
//            $name = preg_replace('/^ +| +$|( ) +/m', '$1', $name);
            $name = preg_replace('/[^а-яё]/ui', '', $name);

            if (in_array($name, array_values($this->triangleLevels)) === false) {
                if (!$name) {
                    continue;
                }

                $level = new LevelTriangleFlavor(['name' => $name]);
                $level->save(false);
                $pks[] = $level->id;
                \Yii::info('Добадвляем в массив уровней: ' . $name, 'test');

                $this->triangleLevels[$level->id] = $level->name;
            } else {
                $pks[] = array_flip($this->triangleLevels)[$name];
            }
        }

        return $pks;
    }

    private function getSubstanceTypeFromCell($cell)
    {
        $name = $cell->getValue();

        if (in_array($name, array_values($this->substanceTypes)) == false) {
            $type = new SubstanceType(['name' => $name]);
            $type->save(false);
            $this->substanceTypes[$type->id] = $type->name;
        } else {
            $type = SubstanceType::find()->where(['name' => $name])->one();
        }

        return $type->id;
    }

    private function getImportantFloverFromCell($cell)
    {
        $names = explode('/', $cell->getValue());
        $pks = [];

        foreach ($names as $name) {
            $name = str_replace('.', '', $name);
            $name = preg_replace('/^ +| +$|( ) +/m', '$1', $name);
            if (in_array($name, array_values($this->importantFlovers)) == false) {
                $flover = new ImportantFlover(['name' => $name]);
                $flover->save(false);
                $pks[] = $flover->id;
                $this->importantFlovers[$flover->id] = $flover->name;
            } else {
                $pks[] = array_flip($this->importantFlovers)[$name];
            }
        }

        return $pks;
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл импорта',
        ];
    }

    private function getGroupsFromCell($cell)
    {
        \Yii::info('Группы: ' . $cell, 'test');
        $groups = explode('/', $cell->getValue());
        $pks = [];

        foreach ($groups as $group) {
            $group = str_replace('.', '', $group);
            $group = preg_replace('/^ +| +$|( ) +/m', '$1', $group);

            if (in_array($group, array_values($this->groups)) == false) {
                if (!$group) {
                    continue;
                }

                $i_group = new GroupIngredient(['name' => $group]);
                $i_group->save(false);
                $pks[] = $i_group->id;
                $this->groups[$i_group->id] = $i_group->name;
            } else {
                $pks[] = array_flip($this->groups)[$group];
            }
        }

        return $pks;
    }
}