<?php

namespace app\models\forms;

use app\models\Ingredient;
use app\models\Remain;
use app\models\RemainOperation;
use Yii;
use yii\base\Model;

/**@property int $orderId Заказ */

class RemainOperationForm extends Model
{
    /**
     * @var int
     */
    public $marketId;

    /**
     * @var int
     */
    public $ingredientId;

    /**
     * @var int
     */
    public $articleId;

    /**
     * @var int
     */
    public $type;

    /**
     * @var float
     */
    public $quantity;

    /**
     * @var int
     */
    public $orderId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['marketId', 'ingredientId', 'type', 'quantity'], 'required'],
            [['marketId', 'ingredientId', 'type', 'articleId'], 'integer'],
            [['quantity'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'marketId' => 'Магазин',
            'ingredientId' => 'Ингредиент',
            'type' => 'Тип',
            'quantity' => 'Кол-во',
            'articleId' => 'Статья',
        ];
    }

    /**
     * @return boolean
     */
    public function operate()
    {
        if($this->validate()) {
            Yii::info('Validate success', 'test');

            $remain = Remain::find()->where(['market_id' => $this->marketId, 'ingredient_id' => $this->ingredientId])->one();

            iF($remain == null){
                $remain = new Remain([
                    'market_id' => $this->marketId,
                    'ingredient_id' => $this->ingredientId,
                    'amount' => 0,
                ]);
                if (!$remain->save(false)){
                    Yii::error($remain->errors, '_error');
                }
            }

            $operation = new RemainOperation([
                'type' => $this->type,
                'remain_id' => $remain->id,
                'amount_last' => $remain->amount,
                'article_id' => $this->articleId,
                'quantity' => $this->quantity,
            ]);

            if (!$operation->save(false)){
                Yii::error($operation->errors, '_error');

            }

            if($this->type == RemainOperation::TYPE_COMING){

                $remain->amount += $this->quantity;

            } else if($this->type == RemainOperation::TYPE_WRITE_OFF){

                $remain->amount -= $this->quantity;

            }

            $remain->save(false);
            $ingredient_model = Ingredient::findOne(['id' => $this->ingredientId]);

            if ($remain->amount > 0){
                //Проставляем ингредиенту признак наличия на складе
                $ingredient_model->has_in_storage = 1;
            } else {
                $ingredient_model->has_in_storage = 0;
            }

            if (!$ingredient_model->save()){
                Yii::error($ingredient_model->errors, '_error');
            }

            return true;
        } else {
            Yii::error($this->errors, '_error');
        }

        return false;
    }
}