<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "remain".
 *
 * @property int $id
 * @property int $market_id Точка продаж
 * @property int $ingredient_id Ингридиент
 * @property double $amount Количество
 * @property int $has_in_storage Есть на складе
 *
 * @property Ingredient $ingredient
 * @property Market $market
 * @property RemainOperation[] $remainOperations
 */
class Remain extends ActiveRecord
{
    public $order_id;
    public $has_in_storage;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remain';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['market_id', 'ingredient_id'], 'integer'],
            [['amount'], 'number'],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
            [['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => Market::className(), 'targetAttribute' => ['market_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'market_id' => 'Точка продаж',
            'ingredient_id' => 'Ингридиент',
            'amount' => 'Количество',
            'has_in_storage' => 'Есть на складе',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(Market::className(), ['id' => 'market_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemainOperations()
    {
        return $this->hasMany(RemainOperation::className(), ['remain_id' => 'id']);
    }
}
