<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "age".
 *
 * @property int $id
 * @property string $name Наименование
 */
class Age extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'age';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    public function getList()
    {
        return  ArrayHelper::map(Age::find()->all(), 'id', 'name');
    }

    /**
     * Получает наименования периодов вораста из строки формата 2|4|5|6
     * @param $ages
     * @return string
     */
    public function getAges($ages)
    {
        $ages_ids = explode('|', $ages);

        $arr_ages = [];

        foreach ($ages_ids as $id){
            $arr_ages[] = Age::findOne($id)->name ?? null;
        }

        return implode(', ', $arr_ages);
    }
}
