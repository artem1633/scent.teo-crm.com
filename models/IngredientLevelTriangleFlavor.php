<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingredient_level_triangle_flavor".
 *
 * @property int $id
 * @property int $ingredient_id
 * @property int $level_triangle_flavor_id
 *
 * @property Ingredient $ingredient
 * @property LevelTriangleFlavor $levelTriangleFlavor
 */
class IngredientLevelTriangleFlavor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient_level_triangle_flavor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ingredient_id', 'level_triangle_flavor_id'], 'integer'],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
            [['level_triangle_flavor_id'], 'exist', 'skipOnError' => true, 'targetClass' => LevelTriangleFlavor::className(), 'targetAttribute' => ['level_triangle_flavor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ingredient_id' => 'Ingredient ID',
            'level_triangle_flavor_id' => 'Level Triangle Flavor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevelTriangleFlavor()
    {
        return $this->hasOne(LevelTriangleFlavor::className(), ['id' => 'level_triangle_flavor_id']);
    }
}
