<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "formulas".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $author Автор
 * @property int $sex Пол
 * @property string $seasons Идентификаторы сезонов
 * @property string $created_at
 * @property int $created_by
 * @property string $description_emotion
 * @property string $description_use
 * @property string $ages Идентификаторы возрастов
 * @property string $styles Идентификаторы стилей
 * @property array $directionFlovers Направления запаха
 * @property array $importantFlovers Слышимые ноты запаха
 * @property string $style_flavors Стили аромата
 *
 * @property User $createdBy
 * @property Formulas $parent
 * @property FormulasImportantFlover[] $formulasImportantFlovers
 * @property DirectionFlavorFormulas[] $directionFlavorFormulas
 * @property Ingredient[] $ingredients
 * @property FormulasIngredient[] $formulasIngredients
 */
class Formulas extends ActiveRecord
{
    const SEX_MALE = 0;
    const SEX_FEMALE = 1;
    const SEX_UNISEX = 2;

//    const AGE_15_18 = 0;
//    const AGE_19_25 = 1;
//    const AGE_26 = 2;
//    const AGE_30 = 3;

    public $directionFlovers;

    public $importantFlovers;

    public $baseIngredients;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'formulas';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'created_by',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sex', 'created_by'], 'integer'],
            [['created_at', 'ages', 'seasons', 'style_flavors'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['description_emotion', 'description_use', 'author'], 'string'],
            [
                ['created_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['created_by' => 'id']
            ],
            [['directionFlovers', 'importantFlovers', 'baseIngredients'], 'safe'],
            [
                [
                    'name',
                    'sex',
                    'seasons',
                    'description_emotion',
                    'description_use',
                    'style_flavors',
                    'ages',
                    'author',
                    'directionFlovers',
//                    'importantFlovers',

                ],
                'required'
            ],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Formulas::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительский аромат',
            'name' => 'Наименование',
            'sex' => 'Пол',
            'ages' => 'Возраст',
            'seasons' => 'Сезон',
            'directionFlovers' => 'Направление',
            'importantFlovers' => 'Слышимые ноты',
            'baseIngredients' => 'Ингредиенты',
            'author' => 'Автор',
            'created_at' => 'Создано',
            'created_by' => 'Менеджер',
            'description_emotion' => 'Эмоциональное описание',
            'description_use' => 'Применение',
            'style_flavors' => 'Стиль аромата',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->ages){
            $this->ages = implode('|', $this->ages);
        }
        if ($this->seasons){
            $this->seasons = implode('|', $this->seasons);
        }
        if ($this->style_flavors){
            $this->style_flavors = implode('|', $this->style_flavors);
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->directionFlovers != null) {
            DirectionFlavorFormulas::deleteAll(['formulas_id' => $this->id]);
            foreach ($this->directionFlovers as $directionFlover) {
                (new DirectionFlavorFormulas([
                    'formulas_id' => $this->id,
                    'direction_flavor_id' => $directionFlover
                ]))->save();
            }
        }

        if ($this->importantFlovers != null) {
            FormulasImportantFlover::deleteAll(['formulas_id' => $this->id]);
            foreach ($this->importantFlovers as $formulasImportantFlover) {
                (new FormulasImportantFlover([
                    'formulas_id' => $this->id,
                    'important_flover_id' => $formulasImportantFlover
                ]))->save();
            }
        }

        if ($insert) {
            if ($this->baseIngredients != null) {
                foreach ($this->baseIngredients as $baseIngredient) {
                    (new FormulasIngredient([
                        'formula_id' => $this->id,
                        'ingredient_id' => $baseIngredient,
                        'is_main' => 1
                    ]))->save();
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function sexLabels()
    {
        return [
            self::SEX_MALE => 'Мужской',
            self::SEX_FEMALE => 'Женский',
            self::SEX_UNISEX => 'Унисекс',
        ];
    }

    /**
     * int $id Идентификатор формулы
     * {@inheritdoc}
     */
    public static function ageLabels($id)
    {
//        return [
//            self::AGE_15_18 => '15-18',
//            self::AGE_19_25 => '19-25',
//            self::AGE_26 => '26',
//            self::AGE_30 => '30',
//        ];
        $formula = Formulas::findOne($id);
        $ids = explode('|', $formula->ages);

        return implode(', ', Age::find()->select('name')->andWhere(['IN', 'id', $ids])->column());

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Formulas::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulasImportantFlovers()
    {
        return $this->hasMany(FormulasImportantFlover::class, ['formulas_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectionFlavorFormulas()
    {
        return $this->hasMany(DirectionFlavorFormulas::class, ['formulas_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportantFlavors()
    {
        return $this->hasMany(ImportantFlover::className(), ['id' => 'important_flover_id'])
            ->via('formulasImportantFlovers');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id' => 'ingredient_id'])
            ->via('formulasIngredients');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulasIngredients()
    {
        return $this->hasMany(FormulasIngredient::className(), ['formula_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulaAges()
    {
        $age_ids =  explode('|', $this->ages);

        return Age::find()->andWhere(['IN', 'id', $age_ids]);

    }

    /**
     * Получает и возвращает строкой направления для формулы
     * @param int $id Идентификатор формулы
     * @return null|string
     */
    public function getStringDirectionFlavors($id)
    {
        $model = self::findOne($id) ?? null;

        if (!$model) {
            return null;
        }

        /** @var \app\models\DirectionFlavorFormulas[] $directions */
        $directions = $model->directionFlavorFormulas ?? null;

        $str = '';

        foreach ($directions as $direction) {
            $str .= $direction->directionFlavor->name . ',' . PHP_EOL;
        }
        return $str;
    }

    /**
     * Определеят рейтинг для формулы по параметру "направление"
     * @param Perfume[] $perfumes Парфюмы, которые нравиться клиенту
     * @return int
     */
    public function getRateByDirectionFlavor($perfumes)
    {
        //Пока у парфюма нет связи с направлением аромата
        return 4;
    }

    /**
     * Определяет рейтинг по слышимым ароматам
     * @param Perfume[] $perfumes Парфюмы, которые нравиться клиенту
     * @return int
     */
    public function getRatingByImportantFlavor($perfumes)
    {
        $sum_rate = 0;

        foreach ($perfumes as $perfume) {
            //Получаем ароматы для парфюма
            $perfume_important_flavor_query = Perfume::find()
                ->joinWith(['importantFlavor'])
                ->andWhere(['perfume.id' => $perfume->id])
                ->orderBy;

            //Получаем формулы для ароматов парфюма
            $formulas = Formulas::find()
                ->joinWith(['importantFlavors'])
                ->andWhere(['IN', 'formulas_important_flover.important_flover_id', $perfume_important_flavor_query])
                ->groupBy('id');
//            TODO::не доделано, наверно
        }

        $rate = (int)($sum_rate / count($perfumes));

        Yii::info('Rate by important flavor: ' . $rate, 'test');

        return $rate;
    }
}
