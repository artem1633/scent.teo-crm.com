<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ingredient".
 *
 * @property int $id
 * @property string $article Артикул
 * @property string $name_rus Наименование
 * @property string $name_eng Наименование (анг.)
 * @property int $developer_id Производитель
 * @property string $description Описание
 * @property int $testable Доступен к дегустации
 * @property int $level_flavor_id Уровень
 * @property int $intensive_flavor_id Интенсивность
 * @property double $substantivity Субстантивность
 * @property double $const_price Себестоимость
 * @property double $cost_price_value объем (за себестоимость)
 * @property string $place_num Место на полке
 * @property int $solvent В чем растворено
 * @property string $cas Номер CAS
 * @property double $input_level Уровень ввода
 * @property string $description_additional Дополнительное описание
 * @property string $created_at
 * @property int $created_by
 * @property integer $has_in_storage
 * @property double $rest Остаток
 * @property double $flavors Ароматы
 * @property string $dev_name Наименование производителя
 * @property string $solvent_percent Процент разбавления
 *
 * @property User $createdBy
 * @property Developer $developer
 * @property IntensiveFlavor $intensiveFlavor
 * @property LevelFlavor $levelFlavor
 * @property IngredientImportantFlover[] $ingredientImportantFlovers
 * @property Remain $remain
 * @property Remain[] $remains
 * @property LevelTriangleFlavor[] $levelTriangleFlavors
 * @property IngredientLevelTriangleFlavor $ingredientLevelTriangleFlavor
 * @property GroupIngredient[] $groups Группы
 */
class Ingredient extends ActiveRecord
{
    const SUBSTANCE_TYPE_ABSOLUTE = 0;
    const SUBSTANCE_TYPE_SYNTHETIC = 1;
    const SUBSTANCE_TYPE_OIL_ESSENTIAL = 2;

    const SOLVENT_ALCOHOL = 0;
    const SOLVENT_DPG = 2;

    public $importantFlovers;
    public $levelTriangleFlavors;
    public $groups;

    /** @var double Остаток */
    public $rest;

    /** @var string Ароматы */
    public $flavors;

    /** @var string Наименование производителя */
    public $dev_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'created_by',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'developer_id',
                    'testable',
                    'level_flavor_id',
                    'intensive_flavor_id',
                    'solvent',
                    'created_by',
                    'has_in_storage'
                ],
                'integer'
            ],
            [['description', 'description_additional'], 'string'],
            [
                [
                    'substantivity',
                    'const_price',
                    'cost_price_value',
                    'input_level',
                    'solvent_percent',
                    'solvent_percent'
                ],
                'number'
            ],
            [['importantFlovers', 'created_at', 'levelTriangleFlavors'], 'safe'],
            [['article', 'name_rus', 'name_eng', 'place_num', 'cas'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['developer_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Developer::className(),
                'targetAttribute' => ['developer_id' => 'id']
            ],
            [
                ['intensive_flavor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => IntensiveFlavor::className(),
                'targetAttribute' => ['intensive_flavor_id' => 'id']
            ],
            [
                ['level_flavor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LevelFlavor::className(),
                'targetAttribute' => ['level_flavor_id' => 'id']
            ],
            [
                ['substance_type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SubstanceType::className(),
                'targetAttribute' => ['substance_type_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Артикул',
            'name_rus' => 'Наименование',
            'name_eng' => 'Наименование (анг.)',
            'substance_type_id' => 'Тип вещества',
            'developer_id' => 'Производитель',
            'description' => 'Описание',
            'testable' => 'Доступен для дегустации',
            'level_flavor_id' => 'Уровень',
            'levelTriangleFlavors' => 'Уровень в пирамиде',
            'intensive_flavor_id' => 'Интенсивность',
            'substantivity' => 'Субстантивность',
            'const_price' => 'Себестоимость',
            'cost_price_value' => 'Объем (за себестоимость)',
            'place_num' => 'Место на полке',
            'solvent' => 'В чем растворено',
            'solvent_percent' => '% разбавления',
            'cas' => 'Номер CAS',
            'has_in_storage' => 'Есть на складе',
            'input_level' => 'Уровень ввода, %',
            'description_additional' => 'Дополнительное описание',
            'importantFlovers' => 'Ароматы',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'rest' => 'Остаток',
            'flavors' => 'Ароматы',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->input_level) {
            $this->input_level = $this->input_level / 100;
        }

        if ($this->solvent_percent) {
            $this->solvent_percent = $this->solvent_percent / 100;
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->importantFlovers != null) {
            IngredientImportantFlover::deleteAll(['ingredient_id' => $this->id]);
            foreach ($this->importantFlovers as $formulasImportantFlover) {
                (new IngredientImportantFlover([
                    'ingredient_id' => $this->id,
                    'important_flover_id' => $formulasImportantFlover
                ]))->save();
            }
        }

        if ($this->levelTriangleFlavors != null) {
            IngredientLevelTriangleFlavor::deleteAll(['ingredient_id' => $this->id]);
            foreach ($this->levelTriangleFlavors as $levelTriangleFlavor) {
                (new IngredientLevelTriangleFlavor([
                    'ingredient_id' => $this->id,
                    'level_triangle_flavor_id' => $levelTriangleFlavor
                ]))->save();
            }
        }

        if ($this->groups != null) {
            IngredientToGroupIngredient::deleteAll(['ingredient_id' => $this->id]);

            foreach ($this->groups as $group) {
                (new IngredientToGroupIngredient([
                    'ingredient_id' => $this->id,
                    'group_ingredient_id' => $group
                ]))->save();
            }
        }

    }

//    /**
//     * @return array
//     */
//    public static function substanceTypeLabels()
//    {
//        return [
//            self::SUBSTANCE_TYPE_ABSOLUTE => 'Абсолют',
//            self::SUBSTANCE_TYPE_SYNTHETIC => 'Синтетика',
//            self::SUBSTANCE_TYPE_OIL_ESSENTIAL => 'Эфирное масло'
//        ];
//    }

    /**
     * @return array
     */
    public static function solventLabels()
    {
        return [
            self::SOLVENT_ALCOHOL => 'Спирт',
            self::SOLVENT_DPG => 'ДПГ'
        ];
    }

    /**
     * @return array
     */
    public function levelTriangleLabels()
    {
        Yii::info('levelTriangleLabels()', 'test');
        $levels = LevelTriangleFlavor::find()
            ->joinWith(['ingredientLevelsTriangleFlavor iltf'])
            ->select('name')
            ->andWhere(['iltf.ingredient_id' => $this->id])
            ->column();

        return implode(',' . PHP_EOL, $levels);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(Developer::className(), ['id' => 'developer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntensiveFlavor()
    {
        return $this->hasOne(IntensiveFlavor::className(), ['id' => 'intensive_flavor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevelFlavor()
    {
        return $this->hasOne(LevelFlavor::className(), ['id' => 'level_flavor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientLevelsTriangleFlavor()
    {
        return $this->hasMany(IngredientLevelTriangleFlavor::class, ['ingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientImportantFlovers()
    {
        return $this->hasMany(IngredientImportantFlover::className(), ['ingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportantFlavors()
    {
        return $this->hasMany(ImportantFlover::className(), ['id' => 'important_flover_id'])
            ->via('ingredientImportantFlovers');
    }

    /**
     * Получает остаток ингридиента
     * @return \yii\db\ActiveQuery|null
     */
    public function getRemain()
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        if ($identity->isSuperAdmin()) {
            return null;

        } else {
            return Remain::find()->andWhere(['ingredient_id' => $this->id, 'market_id' => $identity->market_id]);
        }
    }

    public function getRemains()
    {
        return $this->hasMany(Remain::class, ['ingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getGroups()
    {
        return $this->hasMany(GroupIngredient::class, ['id' => 'group_ingredient_id'])
            ->viaTable('ingredient_to_group_ingredient', ['ingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getFormulas()
    {
        return $this->hasMany(Formulas::class, ['id' => 'formula_id'])
            ->viaTable('formulas_ingredient', ['ingredient_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getList()
    {
        $sql = <<<SQL
SELECT `ingredient`.* FROM `ingredient` INNER JOIN `formulas_ingredient` ON `formulas_ingredient`.`ingredient_id` = `ingredient`.`id`
SQL;

        $ingredients = Ingredient::findBySql($sql)->all();

        return ArrayHelper::map($ingredients, 'id', 'name_rus');
    }
}
