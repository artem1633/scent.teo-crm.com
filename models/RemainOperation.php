<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "remain_operation".
 *
 * @property int $id
 * @property int $remain_id Остаток
 * @property int $method Метод
 * @property int $type Тип
 * @property int $article_id Статья
 * @property double $quantity Кол-во
 * @property double $amount_last Количество до опереции на складе
 * @property string $datetime Дата и время
 * @property int $manager_id Менеджер
 * @property int $inventory_id Инвентаризация
 * @property int $order_id Заказ
 *
 * @property OperationArticle $article
 * @property Inventory $inventory
 * @property User $manager
 * @property Order $order
 * @property Remain $remain
 */
class RemainOperation extends \yii\db\ActiveRecord
{
    const METHOD_USER = 0;
    const METHOD_INVENTORY = 1;
    const METHOD_ORDER = 3;

    const TYPE_WRITE_OFF = 0;
    const TYPE_COMING = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remain_operation';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'datetime',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'manager_id',
                'value' => Yii::$app->user->getId(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['remain_id', 'method', 'type', 'article_id', 'manager_id'], 'integer'],
            [['quantity', 'amount_last'], 'number'],
            [['datetime'], 'safe'],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => OperationArticle::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_id' => 'id']],
            [['remain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Remain::className(), 'targetAttribute' => ['remain_id' => 'id']],
            [['inventory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventory::className(), 'targetAttribute' => ['inventory_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'remain_id' => 'Остаток',
            'method' => 'Метод',
            'type' => 'Тип',
            'article_id' => 'Статья',
            'quantity' => 'Кол-во',
            'amount_last' => 'Количество до опереции на складе',
            'datetime' => 'Дата и время',
            'manager_id' => 'Менеджер',
            'inventory_id' => 'Инвентаризация',
            'order_id' => 'Заказ',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_COMING => 'Приход',
            self::TYPE_WRITE_OFF => 'Списание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(OperationArticle::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemain()
    {
        return $this->hasOne(Remain::className(), ['id' => 'remain_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(Inventory::className(), ['id' => 'inventory_id']);
    }
}
