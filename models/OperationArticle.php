<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operation_article".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $type Тип
 *
 * @property RemainOperation[] $remainOperations
 */
class OperationArticle extends \yii\db\ActiveRecord
{
    const TYPE_WRITE_OFF = 0;
    const TYPE_COMING = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation_article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Тип',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_COMING => 'Приход',
            self::TYPE_WRITE_OFF => 'Списание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemainOperations()
    {
        return $this->hasMany(RemainOperation::className(), ['article_id' => 'id']);
    }
}
