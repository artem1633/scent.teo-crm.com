<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "formulas_important_flover".
 *
 * @property int $id
 * @property int $formulas_id
 * @property int $important_flover_id
 * @property double $level Уровень
 * @property int $formula_ingredient_id Игредиент формулы
 *
 * @property Formulas $formulas
 * @property ImportantFlover $importantFlover
 */
class FormulasImportantFlover extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'formulas_important_flover';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['formulas_id', 'important_flover_id', 'level', 'formula_ingredient_id'], 'integer'],
            [['formulas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Formulas::class, 'targetAttribute' => ['formulas_id' => 'id']],
            [['important_flover_id'], 'exist', 'skipOnError' => true, 'targetClass' => ImportantFlover::class, 'targetAttribute' => ['important_flover_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'formulas_id' => 'Formulas ID',
            'important_flover_id' => 'Аромат',
            'level' => 'Уровень',
            'formula_ingredient_id' => 'Игредиент формулы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulaIngredient()
    {
        return $this->hasOne(FormulasIngredient::class, ['id' => 'formula_ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulas()
    {
        return $this->hasOne(Formulas::class, ['id' => 'formulas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportantFlover()
    {
        return $this->hasOne(ImportantFlover::class, ['id' => 'important_flover_id']);
    }
}
