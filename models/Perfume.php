<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "perfume".
 *
 * @property int $id
 * @property string $name
 * @property int $external_id Внешний ID
 * @property int $sex Пол
 * @property string $thumbnail Изображение
 * @property double $rating Рейтинг
 * @property string $year Рейтинг
 * @property string $url URL
 * @property int $manufacturer_id Производитель
 * @property int $aroma_group_id Группа ароматов
 *
 * @property PerfumeImportantFlover[] $perfumeImportantFlovers
 * @property ImportantFlover[] $importantFlavors
 * @property AromaGroup $aromaGroup
 */
class Perfume extends ActiveRecord
{
    const SEX_MALE = 0;
    const SEX_FEMALE = 1;
    const SEX_UNISEX = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfume';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['external_id', 'sex', 'aroma_group_id'], 'integer'],
            [['rating'], 'number'],
            [['name', 'thumbnail', 'year', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'external_id' => 'Внешний ID',
            'sex' => 'Пол',
            'thumbnail' => 'Изображение',
            'rating' => 'Рейтинг',
            'year' => 'Год',
            'url' => 'URL',
            'aroma_group_id' => 'Группа аромата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfumeImportantFlovers()
    {
        return $this->hasMany(PerfumeImportantFlover::className(), ['perfume_id' => 'id']);
    }

    /**
     * Получает все слышимые ароматы для парфюма
     * @return \yii\db\ActiveQuery
     */
    public function getImportantFlavors()
    {
        return $this->hasMany(ImportantFlover::className(), ['id' => 'important_flover_id'])
            ->via('perfumeImportantFlovers');
    }

    public function getList()
    {
        $list = ArrayHelper::map(self::find()->all(), 'id', 'name');
        return [0 => 'Не выбрано'] + $list;
    }

    public function getAromaGroup()
    {
        return $this->hasOne(AromaGroup::class, ['id' => 'aroma_group_id']);
    }
}
