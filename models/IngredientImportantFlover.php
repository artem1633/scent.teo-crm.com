<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingredient_important_flover".
 *
 * @property int $id
 * @property int $ingredient_id
 * @property int $important_flover_id
 *
 * @property ImportantFlover $importantFlover
 * @property Ingredient $ingredient
 */
class IngredientImportantFlover extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient_important_flover';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ingredient_id', 'important_flover_id'], 'integer'],
            [['important_flover_id'], 'exist', 'skipOnError' => true, 'targetClass' => ImportantFlover::className(), 'targetAttribute' => ['important_flover_id' => 'id']],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ingredient_id' => 'Ingredient ID',
            'important_flover_id' => 'Important Flover ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportantFlover()
    {
        return $this->hasOne(ImportantFlover::className(), ['id' => 'important_flover_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }
}
