<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_perfume".
 *
 * @property int $order_id
 * @property int $perfume_id
 *
 * @property Order $order
 * @property Perfume $perfume
 */
class OrderPerfume extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_perfume';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'perfume_id'], 'required'],
            [['order_id', 'perfume_id'], 'integer'],
            [['order_id', 'perfume_id'], 'unique', 'targetAttribute' => ['order_id', 'perfume_id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['perfume_id'], 'exist', 'skipOnError' => true, 'targetClass' => Perfume::className(), 'targetAttribute' => ['perfume_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'perfume_id' => 'Perfume ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfume()
    {
        return $this->hasOne(Perfume::className(), ['id' => 'perfume_id']);
    }
}
