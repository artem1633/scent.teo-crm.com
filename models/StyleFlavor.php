<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "style_flavor".
 *
 * @property int $id
 * @property string $name
 */
class StyleFlavor extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'style_flavor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @param string $styles Стили аромата. Разделитель "|"
     * @return string
     */
    public function getNames($styles)
    {
        $ids = explode('|', $styles);

        return implode(', ', self::find()->andWhere(['IN', 'id', $ids])->select('name')->column());
    }

    /**
     * Получает наименования стилей аромата из строки формата 2|4|5|6
     * @param $styles
     * @return string
     */
    public function getStyles($styles)
    {
        $styles_ids = explode('|', $styles);

        $arr_styles = [];

        foreach ($styles_ids as $id){
            $arr_styles[] = self::findOne($id)->name;
        }

        return implode(', ', $arr_styles);
    }
}
