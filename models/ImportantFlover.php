<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "important_flover".
 *
 * @property int $id
 * @property string $name
 * @property string $group
 */
class ImportantFlover extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'important_flover';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            [['name', 'group'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'group' => 'Группа'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function groupLabels()
    {
        $arr = [
            'Цветы',
            'Белые цветы',
            'Зеленые, Травянистые, Фужерные',
            'Пряности',
            'Сладкие и гурманские',
            'Деревья и мхи',
            'Смолы и бальзамы',
            'Анималистические ноты',
            'Напитки',
            'Натуральные и синтетические, Популярные и редкие'
        ];

        return array_combine($arr, $arr);
    }

    /**
     * Возвращает идентификаторы ароматов парфюма в порядке убывания рейтинга (самые популярные - первые)
     * Возвращает массив:
     * [
     *   <место> => <Идентификатор аромата>,
     * ]
     * @param Perfume $perfume
     * @return array|null
     */
    public function getRatingForPerfume($perfume)
    {
        $flavors = $perfume->perfumeImportantFlovers ?? null;

        if (!$flavors) {
            return null;
        }

        /** @var array $ids Идентификаторы ароматов, содержащихся в парфюме ключ - ID аромата, значение - рейтинг аромата */
        $ids = [];

        /** @var PerfumeImportantFlover $flavor */
        foreach ($flavors as $flavor) {
            $ids[$flavor->important_flover_id] = $flavor->rating;
        }

        //Сортируем по убыванию рейтинга
        arsort($ids);

        $flavor_ids = array_keys($ids);

        //Ключи массива начинаются от единицы
        $flavor_ids = array_combine(range(1, count($flavor_ids)), $flavor_ids);

        Yii::info($flavor_ids, 'test');

        return $flavor_ids;
    }

    public function getIntensiveRatingForPerfume($perfume)
    {

    }

    public function getList()
    {
       $list = ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name');

       return [0 => 'Не выбрано'] + $list;
    }
}
