<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "inventory".
 *
 * @property int $id
 * @property int $market_id Точка продаж
 * @property string $name Наименование
 * @property string $comment Коменатрий
 * @property string $datetime Дата и время
 * @property int $manager_id Менеджер
 *
 * @property User $manager
 * @property Market $market
 * @property InventoryRemainOperation[] $inventoryRemainOperations
 */
class Inventory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            array(
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'datetime',
                'value' => date('Y-m-d H:i:s'),
            ),
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'manager_id',
                'value' => Yii::$app->user->getId(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['market_id', 'manager_id'], 'integer'],
            [['comment'], 'string'],
            [['datetime'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_id' => 'id']],
            [['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => Market::className(), 'targetAttribute' => ['market_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'market_id' => 'Точка продаж',
            'name' => 'Наименование',
            'comment' => 'Коменатрий',
            'datetime' => 'Дата и время',
            'manager_id' => 'Менеджер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(Market::className(), ['id' => 'market_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryRemainOperations()
    {
        return $this->hasMany(InventoryRemainOperation::className(), ['inventory_id' => 'id']);
    }
}
