<?php

namespace app\models;


/**
 * This is the model class for table "perfume_accord".
 *
 * @property int $id
 * @property string $name Наименование
 *
 * @property PerfumeToPerfumeAccord[] $perfumeToPerfumeAccords
 */
class PerfumeAccord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfume_accord';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfumeToPerfumeAccords()
    {
        return $this->hasMany(PerfumeToPerfumeAccord::className(), ['perfume_accord_id' => 'id']);
    }
}
