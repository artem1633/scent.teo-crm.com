<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "ingredient_to_group_ingredient".
 *
 * @property int $id
 * @property int $ingredient_id Ингредиент
 * @property int $group_ingredient_id Группа ингредиента
 *
 * @property GroupIngredient $groupIngredient
 * @property Ingredient $ingredient
 */
class IngredientToGroupIngredient extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient_to_group_ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ingredient_id', 'group_ingredient_id'], 'integer'],
            [['group_ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => GroupIngredient::className(), 'targetAttribute' => ['group_ingredient_id' => 'id']],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ingredient_id' => 'Ингредиент',
            'group_ingredient_id' => 'Группа ингредиента',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupIngredient()
    {
        return $this->hasOne(GroupIngredient::className(), ['id' => 'group_ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }
}
