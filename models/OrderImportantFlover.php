<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_important_flover".
 *
 * @property int $order_id
 * @property int $important_flover_id
 * @property int $type Тип
 *
 * @property ImportantFlover $importantFlover
 * @property Order $order
 */
class OrderImportantFlover extends \yii\db\ActiveRecord
{
    const TYPE_FLOWER = 0;
    const TYPE_FRUIT = 1;
    const TYPE_SPICE = 2;
    const TYPE_DRINK = 3;
    const TYPE_TREE = 4;
    const TYPE_FOOD = 5;
    const TYPE_NOT_LIKE = 6;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_important_flover';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'important_flover_id', 'type'], 'integer'],
            [['important_flover_id'], 'exist', 'skipOnError' => true, 'targetClass' => ImportantFlover::className(), 'targetAttribute' => ['important_flover_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'important_flover_id' => 'Important Flover ID',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportantFlover()
    {
        return $this->hasOne(ImportantFlover::className(), ['id' => 'important_flover_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
