<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "level_triangle_flavor".
 *
 * @property int $id
 * @property string $name Наименование
 *
 * @property Ingredient[] $ingredients
 */
class LevelTriangleFlavor extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'level_triangle_flavor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    public function getIngredientLevelsTriangleFlavor()
    {
        return $this->hasMany(IngredientLevelTriangleFlavor::class, ['level_triangle_flavor_id' => 'id']);
    }
}
