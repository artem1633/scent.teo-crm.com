<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * FormulasSearch represents the model behind the search form about `app\models\Formulas`.
 */
class FormulasSearch extends Formulas
{
    /**
     * @var FormulasIngredient[]
     */
    public $ingredients;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sex', 'created_by'], 'integer'],
            [['directionFlovers', 'importantFlovers', 'name', 'created_at'], 'safe'],
            [['description_emotion', 'description_use', 'ages', 'style_flavors' , 'seasons', 'author'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Formulas::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $dataProvider->sort->attributes['directionFlovers'] = [
            'asc' => [DirectionFlavor::tableName() . '.name' => SORT_ASC],
            'desc' => [DirectionFlavor::tableName() . '.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['importantFlovers'] = [
            'asc' => [ImportantFlover::tableName() . '.name' => SORT_ASC],
            'desc' => [ImportantFlover::tableName() . '.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sex' => $this->sex,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', Formulas::tableName() . '.name', $this->name]);
        $query->andFilterWhere(['like', 'description_emotion', $this->description_emotion]);
        $query->andFilterWhere(['like', 'description_use', $this->description_use]);
        $query->andFilterWhere(['like', 'seasons', $this->seasons]);
        $query->andFilterWhere(['like', 'ages', $this->ages]);
        $query->andFilterWhere(['like', 'style_flavors', $this->style_flavors]);
        $query->andFilterWhere(['like', 'author', $this->author]);
//        $query->andFilterWhere(['like', 'directionFlavorFormulas.directionFlavor.name', $this->directionFlovers]);

        //Фильтр по Направлению аромата
        $query->joinWith([
            'directionFlavorFormulas' => function (ActiveQuery $query) {
                $query->joinWith([
                    'directionFlavor' => function (ActiveQuery $query) {
                        $query->andFilterWhere(['LIKE', DirectionFlavor::tableName() . '.name', $this->directionFlovers]);
                    }
                ]);
            }
        ]);

        //Фильтр по Слышимые ноты
        $query->joinWith([
            'formulasImportantFlovers' => function (ActiveQuery $query) {
                $query->joinWith([
                    'importantFlover' => function (ActiveQuery $query) {
                        $query->andFilterWhere(['LIKE', ImportantFlover::tableName() . '.name' , $this->importantFlovers]);
                    }
                ]);
            }
        ]);

        return $dataProvider;
    }

    /**
     * @param Order $order
     * @return ArrayDataProvider
     */
    public function searchComplicated($order)
    {

        \Yii::info('searchComplicated', 'test');
        \Yii::info('order ID = ' . $order->id, 'test');

        $query = Formulas::find();

        $dataProvider = new ArrayDataProvider();

        $this->load(Yii::$app->request->queryParams);

        if (!$this->validate()) {
            \Yii::error($this->errors, '_error');
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
//        \Yii::info($this->attributes, 'test');

        $query->andFilterWhere([
            'id' => $this->id,
            'sex' => [$this->sex, Formulas::SEX_UNISEX],
            'seasons' => $this->seasons,
            'ages' => $this->ages,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        \Yii::info('query: ' . $query->createCommand()->getRawSql(), 'test');

//        echo $query->createCommand()->getRawSql();

        /** @var Formulas[] $models */
        $models = $query->asArray()->all();

        $likeFlavors = ArrayHelper::getColumn(
            OrderImportantFlover::find()
                ->where(['order_id' => $order->id])
                ->andFilterWhere([
                    '!=',
                    'type',
                    OrderImportantFlover::TYPE_NOT_LIKE
                ])
                ->all(), 'important_flover_id');

//        \Yii::info('Like flavors: ', 'test');
//        \Yii::info($likeFlavors, 'test');


        $notLikeFlavors = ArrayHelper::getColumn(
            OrderImportantFlover::find()
                ->where(['order_id' => $order->id])
                ->andFilterWhere(['type' => OrderImportantFlover::TYPE_NOT_LIKE])
                ->all(),
            'important_flover_id');

//        \Yii::info('Not Like flavors: ', 'test');
//        \Yii::info($notLikeFlavors, 'test');

        $commonFlavorsCount = count($likeFlavors) + count($notLikeFlavors); // Общее кол-во ароматов
//        $order->current_flavors_count = $commonFlavorsCount;

//        \Yii::info('Total flavors: ' . $commonFlavorsCount, 'test');


        for ($i = 0; $i < count($models); $i++) {
//            \Yii::info('Цикл по всем формулам', 'test');
//            \Yii::info('Текущая формула: ' . $models[$i]['id'] . ' ' . $models[$i]['name'], 'test');

            //            echo "<br>";
            $model = $models[$i];

            $modelFlavors = ArrayHelper::getColumn(
                FormulasImportantFlover::find()
                    ->where(['formulas_id' => $model['id']])
                    ->all(),
                'important_flover_id');

//            \Yii::info('Ароматы для формулы: ' . $model['name'], 'test');
//            \Yii::info($modelFlavors, 'test');

            $models[$i]['rate'] = 0;

            $matches = 0;

            foreach ($modelFlavors as $flavor) {
//                \Yii::info('Цикл по ароматам формулы ' . $model['name'], 'test');
//                \Yii::info('Текуший аромат: ' . $flavor, 'test');

                if (in_array($flavor, $likeFlavors)) {
                    $matches++;
//                    \Yii::info('Совпадения + 1', 'test');
                }
            }
//            \Yii::info('Всего совпадений ароматов: ' . $matches, 'test');

            if (count($likeFlavors) > 0) {
//                \Yii::info('Есть ароматы которые нравятся', 'test');

                if (count($modelFlavors)){
                    $models[$i]['floversMatchPercent'] = round($matches / count($modelFlavors) * 100) ?? 0;
                } else {
                    $models[$i]['floversMatchPercent'] = 0;
                }
                if ($models[$i]['floversMatchPercent'] > 50) {
                    $models[$i]['rate'] += 3;
                } else {
                    if ($models[$i]['floversMatchPercent'] < 50 && $models[$i]['floversMatchPercent'] > 30) {
                        $models[$i]['rate'] += 2;
                    } else {
                        $models[$i]['rate'] += 1;
                    }
                }
//                \Yii::info('Рейтинг ' . $models[$i]['name'] . ' = ' . $models[$i]['rate'], 'test');
            } else {
                $models[$i]['floversMatchPercent'] = 0;
            }

//            \Yii::info('Формулы', 'test');
//            \Yii::info($models, 'test');

            $matches = 0;

            foreach ($modelFlavors as $flavor) {
                if (in_array($flavor, $notLikeFlavors)) {
                    $matches++;
                }
            }

            if (count($notLikeFlavors) > 0) {
//                \Yii::info('Есть ароматы которые НЕ нравятся', 'test');

                if (count($modelFlavors)){
                    $models[$i]['notFloversMatchPercent'] = round($matches / count($modelFlavors) * 100);
                } else {
                    $models[$i]['notFloversMatchPercent'] = 0;
                }

                if ($models[$i]['notFloversMatchPercent'] > 50) {
                    $models[$i]['rate'] -= 3;
                } else {
                    if ($models[$i]['notFloversMatchPercent'] < 50 && $models[$i]['notFloversMatchPercent'] > 30) {
                        $models[$i]['rate'] -= 2;
                    } else {
                        $models[$i]['rate'] -= 1;
                    }
                }
            } else {
                $models[$i]['notFloversMatchPercent'] = 0;
            }

//            \Yii::info('Рейтинг ' . $models[$i]['name'] . ' = ' . $models[$i]['rate'], 'test');


//            \Yii::info($models, 'test');

            $likePerfumes = OrderPerfume::find()->where(['order_id' => $order->id])->all();

            /** @var OrderPerfume $likePerfume */
            foreach ($likePerfumes as $likePerfume) {
                \Yii::info('Збс парфюм: ' . $likePerfume->perfume_id, 'test');
                $perfume = Perfume::findOne($likePerfume->perfume_id);

                if ($perfume) {
                    //Ароматы парфюма
                    $perfumeImportantFlavors = PerfumeImportantFlover::find()
                        ->joinWith(['importantFlover impflav'])
                        ->select('perfume_important_flover.important_flover_id')
                        ->where(['perfume_important_flover.perfume_id' => $perfume->id])
                        ->column();


                    $matches = 0;

                    foreach ($perfumeImportantFlavors as $flavor) {
                        if (in_array($flavor, $likeFlavors)) {
                            $matches++;
                        }
                    }
//                    \Yii::info('Совпадений ароматов парфюма: ' . $matches, 'test');

                    if (count($perfumeImportantFlavors) > 0) {
//                        \Yii::info('Есть парфюмы которые нравятся', 'test');

                        if (count($modelFlavors)) {
                            $models[$i]['PerfumeMatchPercent'] = round($matches / count($modelFlavors) * 100);
                        } else {
                            $models[$i]['PerfumeMatchPercent'] = 0;
                        }
                        if ($models[$i]['PerfumeMatchPercent'] > 50) {
                            $models[$i]['rate'] -= 3;
                        } else {
                            if ($models[$i]['PerfumeMatchPercent'] < 50 && $models[$i]['PerfumeMatchPercent'] > 30) {
                                $models[$i]['rate'] -= 2;
                            } else {
                                $models[$i]['rate'] -= 1;
                            }
                        }
                    } else {
                        $models[$i]['PerfumeMatchPercent'] = 0;
                    }

//                    \Yii::info('Рейтинг парфюма: ' . $perfume->name . ' = ' . $models[$i]['rate'], 'test');
                }
            }

//            echo "{$matches} / ".count($modelFlavors)." * 100";

//            var_dump($modelFlavors);
//            echo "<br>";
        }

//        var_dump($models);
//
//        exit;

        usort($models, function ($a, $b) {
            return $a['rate'] < $b['rate'];
        });


        $topModels = $models;

        if (count($models) > 5) {
            array_splice($topModels, 5);
        }

//        $this->ingredients = [];


        // VarDumper::dump($topModels, 10, true);
        // exit;

//        \Yii::info('Top Formula Models: ', 'test');
//        \Yii::info($topModels, 'test');


        //Цикл по топовым формулам
        /** @var array $model Formula attribute */
        foreach ($topModels as $model) {

            //Получаем все ингредиенты топовой формулы
            $formula_model = Formulas::findOne($model['id']) ?? null;

            if ($formula_model) {
//                \Yii::info('Топ формула', 'test');
//                \Yii::info($formula_model->attributes, 'test');

                $f_ingredients = $formula_model->formulasIngredients;

//                \Yii::info('Ингридиентов в формуле: ' . count($f_ingredients), 'test');

                foreach ($f_ingredients as $f_ingredient) {
                    //Все ингредиенты формулы связываем с заказом (пишем в order_formulas_ingredient)
                    $exist = OrderFormulasIngredient::find()
                        ->andWhere(['order_id' => $order->id, 'formulas_ingredient_id' => $f_ingredient->id])
                        ->exists();
                    if (!$exist) {
//                        \Yii::info('Ингридиент не найден в заказе, сохраняем', 'test');

                        $ofi = new OrderFormulasIngredient();
                        $ofi->order_id = $order->id;
                        $ofi->formulas_ingredient_id = $f_ingredient->id;
                        $ofi->mark = 0;
//                        \Yii::info('Создаем запись OrderFormulasIngredient', 'test');
//                        \Yii::info($ofi->attributes, 'test');
                        if (!$ofi->save()) {
//                            \Yii::error($ofi->errors, '_error');
                        }
                    } else {
//                        \Yii::info('Ингридиент найден в заказе, пропускаем', 'test');
                    }
                }
            }

            /** @var FormulasImportantFlover[] $topModelFlavors */
            $topModelFlavors = FormulasImportantFlover::find()
                ->where(['formulas_id' => $model['id']])
                ->orderBy('level desc')
                ->limit(4)
                ->all();

//            \Yii::info('Кол-во топовых ингредиентов: ' . count($topModelFlavors), 'test');


            /** @var FormulasImportantFlover $flavor */
            foreach ($topModelFlavors as $flavor) {
                if ($flavor->formula_ingredient_id != null) {
                    /** @var FormulasIngredient $ingredient */
                    $ingredient = FormulasIngredient::findOne($flavor->formula_ingredient_id);
//                    \Yii::info('Ингридиент: ' . $ingredient->id, 'test');

                    if ($ingredient->ingredient->testable == 1) {
                        $this->ingredients[] = $ingredient;
//                        \Yii::info('Ингридиент Id' . $ingredient->id . ' добавлен.', 'test');
                    }
                }
            }
        }

        //Убираем дубли
        /** @var array $ids Идентификаторы ингредиентов */
//        $ids = [];
//
//        foreach ($this->ingredients as $ingredient) {
//            if (!array_search($ingredient->id, $ids)) {
//                //Если ингридиент не найден - добавляем (исключаем дубли)
//                $this->ingredients[] = $ingredient;
//                array_push($ids, $ingredient->id);
//            }
//        }
//
//        \Yii::info('Все Ингридиенты:', 'test');
//        \Yii::info(json_decode(json_encode($this->ingredients), true), 'test');
        // var_dump($this->ingredients);

        // exit;

//        VarDumper::dump($this->ingredients, 10, true);
//        exit;

//        \Yii::info($models, 'test');
        $dataProvider->setModels($models);

        return $dataProvider;
    }
}
