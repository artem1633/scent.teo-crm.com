<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_formulas_ingredient".
 *
 * @property int $id
 * @property int $order_id
 * @property int $formulas_ingredient_id
 * @property int $mark Оценка
 *
 * @property FormulasIngredient $formulasIngredient
 * @property Order $order
 */
class OrderFormulasIngredient extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_formulas_ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'formulas_ingredient_id', 'mark'], 'integer'],
            [['formulas_ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormulasIngredient::className(), 'targetAttribute' => ['formulas_ingredient_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'formulas_ingredient_id' => 'Formulas Ingredient ID',
            'mark' => 'Оценка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulasIngredient()
    {
        return $this->hasOne(FormulasIngredient::className(), ['id' => 'formulas_ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

}
