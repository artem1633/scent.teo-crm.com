<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "perfume_important_flover".
 *
 * @property int $id
 * @property int $perfume_id
 * @property int $important_flover_id
 * @property int $rating Рейтиг
 *
 * @property ImportantFlover $importantFlover
 * @property Perfume $perfume
 */
class PerfumeImportantFlover extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfume_important_flover';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perfume_id', 'important_flover_id', 'rating'], 'integer'],
            [['important_flover_id'], 'exist', 'skipOnError' => true, 'targetClass' => ImportantFlover::className(), 'targetAttribute' => ['important_flover_id' => 'id']],
            [['perfume_id'], 'exist', 'skipOnError' => true, 'targetClass' => Perfume::className(), 'targetAttribute' => ['perfume_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perfume_id' => 'Perfume ID',
            'important_flover_id' => 'Important Flover ID',
            'rating' => 'Рейтиг',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportantFlover()
    {
        return $this->hasOne(ImportantFlover::className(), ['id' => 'important_flover_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfume()
    {
        return $this->hasOne(Perfume::className(), ['id' => 'perfume_id']);
    }
}
