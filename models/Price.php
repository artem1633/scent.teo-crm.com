<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "price".
 *
 * @property int $id
 * @property string $name Наименование
 *
 * @property PriceCost[] $priceCosts
 */
class Price extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование услуги',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCosts()
    {
        return $this->hasMany(PriceCost::className(), ['price_id' => 'id']);
    }


    /**
     * Формирует массив для отображения полного прайса
     * Пример возвращаемого массива:
     *  [
     *      <type_name> => [
     *          <price_name> => [
     *              <volume> => <cost>,
     *              <volume> => <cost>,
     *              <volume> => <cost>,
     *          ]
     *      ],
     *      'Новое создание' => [
     *          'Парфюмерная вода' => [
     *              10 => 1500.0,
     *              30 => 200.0,
     *              50 => 4500.0,
     *          ],
     *         'Туалетная вода' => [
     *              10 => 1550.0,
     *              30 => 345.0,
     *              50 => 8000.0,
     *          ]
     *      ]
     *  ]
     * @return array
     */
    public function getFullPrice()
    {
        $data = [];

        /** @var PriceCost $pc */
        foreach (PriceCost::find()->select('DISTINCT(type)')->all() as $pc) {
            /** @var Price $service */
            foreach ($this->getServicesWithType($pc->type) as $service) {
                foreach ($this->getDataForService($service->id) as $service_data) {
                    $data[$pc->getTypeName()][$service->name][$service_data->volume] = $service_data->cost;
                }

            }
        }

        \Yii::info($data, 'test');

        return $data;
    }

    /**
     * @param $type
     * @return array|ActiveRecord[]
     */
    private function getServicesWithType($type)
    {
        return Price::find()
            ->joinWith(['priceCosts pc'])
            ->select(['DISTINCT(price.id)', 'price.name'])
            ->andWhere(['pc.type' => $type])
            ->all();
    }

    /**
     *
     * @param $service_id
     * @return PriceCost[]
     */
    private function getDataForService($service_id)
    {
        return PriceCost::findAll(['price_id' => $service_id]);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
