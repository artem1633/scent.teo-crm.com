<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * IngredientSearch represents the model behind the search form about `app\models\Ingredient`.
 */
class IngredientSearch extends Ingredient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'developer_id', 'level_flavor_id', 'intensive_flavor_id', 'solvent', 'created_by'], 'integer'],
            [
                [
                    'article',
                    'name_rus',
                    'name_eng',
                    'description',
                    'testable',
                    'place_num',
                    'cas',
                    'description_additional',
                    'created_at',
                    'has_in_storage',
                    'flavors',
                    'dev_name',
                    'levelTriangleFlavors'
                ],
                'safe'
            ],
            [['substantivity', 'const_price', 'cost_price_value', 'input_level'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ingredient::find();
        $query->joinWith(['importantFlavors impflav']);
        $query->joinWith(['developer']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'developer_id' => $this->developer_id,
            'level_flavor_id' => $this->level_flavor_id,
            'intensive_flavor_id' => $this->intensive_flavor_id,
            'substantivity' => $this->substantivity,
            'const_price' => $this->const_price,
            'cost_price_value' => $this->cost_price_value,
            'has_in_storage' => $this->has_in_storage,
            'solvent' => $this->solvent,
            'input_level' => $this->input_level,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'name_rus', $this->name_rus])
            ->andFilterWhere(['like', 'name_eng', $this->name_eng])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'testable', $this->testable])
            ->andFilterWhere(['like', 'place_num', $this->place_num])
            ->andFilterWhere(['like', 'cas', $this->cas])
            ->andFilterWhere(['like', 'impflav.name', $this->flavors])
            ->andFilterWhere(['like', 'description_additional', $this->description_additional])
            ->andFilterWhere(['like', 'developer.name', $this->dev_name]);

        return $dataProvider;
    }
}
