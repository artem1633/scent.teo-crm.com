<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "group_ingredient".
 *
 * @property int $id
 * @property string $name Наименование
 *
 * @property IngredientToGroupIngredient[] $ingredientToGroupIngredients
 */
class GroupIngredient extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group_ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientToGroupIngredients()
    {
        return $this->hasMany(IngredientToGroupIngredient::className(), ['group_ingredient_id' => 'id']);
    }
}
