<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Perfume;

/**
 * PerfumeSearch represents the model behind the search form about `app\models\Perfume`.
 */
class PerfumeSearch extends Perfume
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'external_id', 'sex'], 'integer'],
            [['name', 'thumbnail', 'year', 'url'], 'safe'],
            [['rating'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Perfume::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'external_id' => $this->external_id,
            'sex' => $this->sex,
            'rating' => $this->rating,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'thumbnail', $this->thumbnail])
            ->andFilterWhere(['like', 'year', $this->year])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
