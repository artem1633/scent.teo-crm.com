<?php

namespace app\models;

/**
 * This is the model class for table "perfume_manufacturer".
 *
 * @property int $id
 * @property string $name
 *
 * @property Perfume[] $perfumes
 */
class PerfumeManufacturer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfume_manufacturer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfumes()
    {
        return $this->hasMany(Perfume::className(), ['manufacturer_id' => 'id']);
    }
}
