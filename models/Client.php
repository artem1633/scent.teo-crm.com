<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $firstname Имя
 * @property string $lastname Фамилия
 * @property string $phone Телефон
 * @property string $email E-mail
 * @property string $birthday День рождения
 * @property int $male Пол
 * @property string $address Город проживания
 * @property int $allergy Алергия
 * @property string $allergy_name Алергия на что
 * @property string $created_at Дата создания
 *
 * @property Order[] $orders Заказы клиента
 */
class Client extends ActiveRecord
{
    const CLIENT_SEX_MALE = 0;
    const CLIENT_SEX_FEMALE = 1;

    public $client_info;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday', 'created_at'], 'safe'],
            [['male', 'allergy'], 'integer'],
            [['firstname', 'lastname', 'email', 'address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 16],
            [['client_info', 'allergy_name'], 'string'],
            [['firstname', 'lastname', 'phone', 'male', 'address'], 'required'],
            ['phone', 'unique', 'message' => 'Клиент с данным номером телефона уже имеется в базе'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'birthday' => 'День рождения',
            'male' => 'Пол',
            'address' => 'Город проживания',
            'allergy' => 'Алергия',
            'allergy_name' => 'Алергия на что',
            'created_at' => 'Дата добавления',
        ];
    }

    public static function getList()
    {
        $sql = <<<SQL
SELECT `id`, CONCAT(`lastname`, " ", `firstname`, ". тел: ", `phone`) as `client_info` FROM `client`
SQL;

        $query = self::findBySql($sql);

        return ArrayHelper::map($query->all(), 'id', 'client_info');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(),['client_id' => 'id']);
    }

    /**
     * Получает телефоны всех клиентов (для Typehead)
     * @return array
     */
    public function getPhones()
    {
        return  ArrayHelper::getColumn(self::find()->all(), 'phone');
    }

    public function getFullName()
    {
        $f_name = $this->firstname ?? null;
        $l_name = $this->lastname ?? null;

        return $l_name . ' ' . $f_name;

    }

    /**
     * @return array
     */
    public function sexLabels()
    {
        return [
            self::CLIENT_SEX_MALE => 'Мужской',
            self::CLIENT_SEX_FEMALE => 'Женский',
        ];
    }
}
