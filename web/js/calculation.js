function fillClimat() {
    let idCity = $('#w1').val();
    if (idCity) {
        $.get("get-climat", {id: idCity})
            .done(function (data) {
                if (data.data.error) {
                    console.log('Город не найден!!!');
                } else {
                    $('#calculation-temperature_average').val(data.data['temperature_average']);
                    $('#calculation-temperature_calculated').val(data.data['temperature_calculated']);
                    $('#calculation-winder_speed').val(data.data['winder_speed']);
                    $('#calculation-temp_average1').val(data.data['temp_average1']);
                    $('#calculation-temp_average2').val(data.data['temp_average2']);
                    $('#calculation-temp_average3').val(data.data['temp_average3']);
                    $('#calculation-temp_average4').val(data.data['temp_average4']);
                    $('#calculation-temp_average5').val(data.data['temp_average5']);
                    $('#calculation-temp_average6').val(data.data['temp_average6']);
                    $('#calculation-temp_average7').val(data.data['temp_average7']);
                    $('#calculation-temp_average8').val(data.data['temp_average8']);
                    $('#calculation-temp_average9').val(data.data['temp_average9']);
                    $('#calculation-temp_average10').val(data.data['temp_average10']);
                    $('#calculation-temp_average11').val(data.data['temp_average11']);
                    $('#calculation-temp_average12').val(data.data['temp_average12']);
                }
            });
    } else {
        alert('Не выбран город');
    }

}

function buildingTypeChange() {
    let idType = $('#objectsovform-building_type').val();
    if (idType) {
        $.get("get-ov-building-type", {id: idType})
            .done(function (data) {
                if (data.data.error) {
                    console.log('не найден!!!');
                } else {
                    $('#objectsovform-specific_thermal_characteristic_heating').val(data.data['specific_thermal_characteristic_heating']);
                    $('#objectsovform-specific_thermal_characteristic_ventilation').val(data.data['specific_thermal_characteristic_ventilation']);
                }
            });
    } else {
        alert('Не выбран город');
    }
}


function systemGvsTypeChange() {
    let idType = $('#objectsgvsform-purpose_system').val();
    if (idType) {
        $.get("get-gvs-purpose-system", {id: idType})
            .done(function (data) {
                if (data.data.error) {
                    console.log('не найден!!!');
                } else {
                    $('#objectsgvsform-water_consumption_rate').val(data.data['water_consumption_rate']);
                    $('#objectsgvsform-calculation_regarding_quantity').val(data.data['calculation_regarding_quantity']);
                }
            });
    } else {
        alert('Не выбран город');
    }
}

function systemTechTypeChange() {
    let idType = $('#objectstechform-purpose_system').val();
    if (idType) {
        $.get("get-tech-purpose-system", {id: idType})
            .done(function (data) {
                if (data.data.error) {
                    console.log('не найден!!!');
                } else {
                    $('#objectstechform-consumption_thermal_units').val(data.data['consumption_thermal_units']);
                    $('#objectstechform-consumption_equivalent_fuel').val(data.data['consumption_equivalent_fuel']);
                    $('#objectstechform-day_per_week').val(data.data['day_per_week']);
                    $('#objectstechform-norm_document').val(data.data['norm_document']);
                    $('#label-objectstechform-amount').html(data.data['amount_name']);
                    $('#objectstechform-amount_name').val(data.data['amount_name']);

                }
            });
    } else {
        alert('Не выбран город');
    }
}
function ovPowerChange() {
    let powerO = $('#objectsovform-heating_load').val();
    let powerV = $('#objectsovform-ventilation_load').val();
        $('#ov_all').html(parseInt(powerO)+parseInt(powerV))
}

function gvsPowerChange() {
    let powerO = $('#objectsgvsform-heating_load').val();
    let powerV = $('#objectsgvsform-ventilation_load').val();
    $('#gvs_all').html(parseInt(powerO)+parseInt(powerV))
}

function ovLoadsKnowChange() {
    let LoadsKnowValue = $('#objectsovform-heat_loads_known').is(':checked');

    if (LoadsKnowValue) {
        $('#objectsovform-heating_load').prop('disabled', false);
        $('#objectsovform-ventilation_load').prop('disabled', false);


    } else {
        $('#objectsovform-heating_load').prop('disabled', true);
        $('#objectsovform-ventilation_load').prop('disabled', true);
    }
}

function gvsLoadsKnowChange() {
    let LoadsKnowValue = $('#objectsgvsform-heat_loads_known').is(':checked');

    if (LoadsKnowValue) {
        $('#objectsgvsform-heating_load').prop('disabled', false);
        $('#objectsgvsform-ventilation_load').prop('disabled', false);


    } else {
        $('#objectsgvsform-heating_load').prop('disabled', true);
        $('#objectsgvsform-ventilation_load').prop('disabled', true);
    }
}

function group(group) {
    var PosId = $('#crud-datatable-object').yiiGridView('getSelectedRows');
    if (PosId == "") {
        alert("Нет отмеченных записей!");
    } else {
        $.ajax({
            type: 'POST',
            url: 'group-object',
            data: {pks: PosId, num_group: group},
        }).done(function (data) {
            console.log(data);
            $.pjax.reload({container: '#crud-datatable-object-pjax', async: false});
            $('#crud-datatable-object-pjax').removeClass('kv-grid-loading');
            $.pjax.reload({container: '#crud-datatable-equipment-pjax', async: false});
            $('#crud-datatable-equipment-pjax').removeClass('kv-grid-loading');
        });
    }
}