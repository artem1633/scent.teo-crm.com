$('#btn-dropdown_header').click(function () {
    if ($(".dropdown-menu").is(':visible')) {
        $(".dropdown-menu ").hide();
    } else {
        $(".dropdown-menu ").show();
    }
});

$(document).on('click', '#add-params', function (e) {
    e.preventDefault();
    $('.price-cost-clone').clone().appendTo('.form-container').show().removeClass('price-cost-clone').addClass('price-cost');

    console.log($('.price-cost'));
});

$(document).on('click', '.remove-params', function (e) {
    e.preventDefault();
    $(this).parent().parent().remove();

    console.log('removed');
});

$(document).on('change', '.for-calc', function () {
    var volume = $('#value-order').val();
    var concentration = $('#concentration-order').val();

    if (volume > 0 && concentration > 0) {
        send_form('order-form');
    }
});

function send_form(form_id) {
    var form = $('#' + form_id);
    var msg = form.serialize();
    $.ajax({
        type: 'POST',
        url: '/order/calc-price',
        data: msg,
        success: function (response) {
            console.log(response.data);
            if (response.success === 1) {
                $('#total-price').html(response.data);
            } else {
                alert(response.data);
            }

        }
    });
}

$('.step').hover(
    function(){
        var label =  $(this).find('.step-label');
        label.removeClass('hidden');
    },
    function () {
        if ($(this).find('a').hasClass('active') === false) {
            $(this).find('.step-label').addClass('hidden');
        }
    }
);