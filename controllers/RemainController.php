<?php

namespace app\controllers;

use app\models\forms\InventoryForm;
use app\models\forms\RemainOperationForm;
use app\models\RemainOperation;
use Yii;
use app\models\Remain;
use app\models\RemainSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * RemainController implements the CRUD actions for Remain model.
 */
class RemainController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Remain models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RemainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Remain model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "остаток #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Remain model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Remain();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить остаток",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Добавить остаток",
                    'content' => '<span class="text-success">Создание остатка успешно завершено</span>',
                    'footer' => Html::button('ОК', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Добавить остаток",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionComing()
    {
        $request = Yii::$app->request;
        $model = new RemainOperationForm();
        $model->type = RemainOperation::TYPE_COMING;

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $model->marketId = Yii::$app->user->identity->market_id;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Приход",
                    'content' => $this->renderAjax('coming', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->operate()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Приход",
                    'content' => '<span class="text-success">Создание прихода успешно завершено</span>',
                    'footer' => Html::button('ОК', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['coming'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Приход",
                    'content' => $this->renderAjax('coming', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->operate()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('coming', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Списание
     * @param int $order_id Заказ
     * @return array|string|Response
     */
    public function actionWriteOff($order_id = null)
    {
        $request = Yii::$app->request;
        $model = new RemainOperationForm();
        $model->type = RemainOperation::TYPE_WRITE_OFF;
        $model->orderId = $order_id;


        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $model->marketId = Yii::$app->user->identity->market_id;
        }
        Yii::info($model->attributes, 'test');

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::info('Is ajax', 'test');

            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                Yii::info('Is Get', 'test');
                return [
                    'title' => "Списание",
                    'content' => $this->renderAjax('write-off', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->operate()) {
                Yii::info('Is Post and operate', 'test');
                if ($request->get('order_id') ?? null){
                    return [
                        'title' => "Списание",
                        'content' => '<span class="text-success">Создание списания успешно завершено</span>',
                        'footer' => Html::button('ОК', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]),
                    ];
                }
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Списание",
                    'content' => '<span class="text-success">Создание списания успешно завершено</span>',
                    'footer' => Html::button('ОК', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['coming'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                Yii::info('Is Post and not operate', 'test');

                return [
                    'title' => "Списание",
                    'content' => $this->renderAjax('write-off', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            Yii::info('Non Ajax', 'test');

            if ($model->load($request->post()) && $model->operate()) {
                return $this->redirect(['/order/update', 'id' => $model->orderId]);
            } else {
                return $this->render('write-off', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionInventory()
    {
        $request = Yii::$app->request;
        $model = new InventoryForm();

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $model->marketId = Yii::$app->user->identity->market_id;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Инвентаризация",
                    'content' => $this->renderAjax('inventory', [
                        'model' => $model,
                        'step' => 0,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->validate()) {

                $model->scenario = InventoryForm::SCENARIO_INGREDIENTS;

                $model->load($request->post());

                if($model->remains == null){
                    return [
                        'title' => "Инвентаризация",
                        'content' => $this->renderAjax('inventory', [
                            'model' => $model,
                            'step' => 1,
                        ]),
                        'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                } else {

                    $model->invent();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Инвентаризация",
                        'content' => '<span class="text-success">Инвентаризация успешно проведена</span>',
                        'footer' => Html::button('ОК', ['class' => 'btn btn-default btn-block', 'data-dismiss' => "modal"]),
                    ];
                }
            } else {
                return [
                    'title' => "Инвентаризация",
                    'content' => $this->renderAjax('inventory', [
                        'model' => $model,
                        'step' => 0,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->operate()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('inventory', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Remain model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить остаток #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "остаток #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить остаток #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Remain model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Remain model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Remain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Remain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Remain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
