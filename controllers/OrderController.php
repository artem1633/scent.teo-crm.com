<?php

namespace app\controllers;

use app\models\ChangeLog;
use app\models\ChangeLogSearch;
use app\models\Client;
use app\models\FormulasSearch;
use app\models\OrderFormulasIngredient;
use app\models\OrderIngredientSearch;
use app\models\OrderStepSearch;
use app\models\User;
use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "заказ #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Order model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateOld()
    {
        $request = Yii::$app->request;
        $model = new Order();
        $model->scenario = Order::SCENARIO_START;
        $model->price = 0;

        $formulasSearchModel = new FormulasSearch();
        $formulasDataProvider = $formulasSearchModel->searchComplicated($model);

        $orderIngredientSearchModel = new OrderIngredientSearch();
        $orderIngredientDataProvider = $orderIngredientSearchModel->search(Yii::$app->request->queryParams);
        $orderIngredientDataProvider->query->orderBy(['ingredient.testable' => SORT_DESC]);

        $stepSearchModel = new OrderStepSearch();
        $stepDataProvider = $stepSearchModel->search(Yii::$app->request->queryParams);


        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить заказ",
                    'content' => $this->renderAjax('create_old', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->nextStep()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Добавить заказ",
                        'content' => '<span class="text-success">Создание заказа успешно завершено</span>',
                        'footer' => Html::button('ОК',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Добавить заказ",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->nextStep()) {

                Yii::info('is Post', 'test');
                Yii::info($model->toArray(), 'test');

                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                $model->step = 1;
                return $this->render('create_old', [
                    'model' => $model,
                    'formulasSearchModel' => $formulasSearchModel,
                    'formulasDataProvider' => $formulasDataProvider,
                    'orderIngredientSearchModel' => $orderIngredientSearchModel,
                    'orderIngredientDataProvider' => $orderIngredientDataProvider,
                    'stepSearchModel' => $stepSearchModel,
                    'stepDataProvider' => $stepDataProvider
                ]);
            }
        }

    }

    public function actionCreate()
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        $request = Yii::$app->request;
        if ($request->isGet) {
            $model = new Order();
            $model->manager_id = $identity->id;
            $model->market_id = $model->manager->market_id ?? null;
            $model->price = 0;
            $model->step = 1;
            $model->next_step = 2;
            if (!$model->save(false)) {
                Yii::error($model->errors, '_error');
            }
            Yii::info($model->attributes, 'test');

//            $model->setStepInfo(1);
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->goHome();
    }

    /**
     * Updates an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param int $forward Переход вперед или назад
     * @return mixed
     * @throws NotFoundHttpException
     */
//    public function actionUpdate($id)
//    {
//        $request = Yii::$app->request;
//        $model = $this->findModel($id);
//
//        $model->scenario = Order::steps()[$model->step + 1];
//
//        Yii::info($model->scenario, 'test');
//        Yii::info('Client id: ' . $model->client_id, 'test');
//
//        if ($model->client_id != null) {
//            $sex = $model->client->male;
//            $model->old_client_id = $model->client_id;
//        } else {
//            $sex = null;
//        }
//
//        $formulasSearchModel = new FormulasSearch();
//        $formulasSearchModel->sex = $sex;
//        $formulasDataProvider = $formulasSearchModel->searchComplicated($model);
//
//        $orderIngredientSearchModel = new OrderIngredientSearch();
//        $orderIngredientDataProvider = $orderIngredientSearchModel->search(Yii::$app->request->queryParams);
//        $orderIngredientDataProvider->query->andWhere(['order_id' => $id]);
//
//        $stepSearchModel = new OrderStepSearch();
//        $stepDataProvider = $stepSearchModel->search(Yii::$app->request->queryParams);
//        $stepDataProvider->query->andWhere(['order_id' => $id]);
//
//
////        var_dump($formulasDataProvider->models);
//
////        VarDumper::dump($formulasDataProvider->models, 10, true);
//
//        if ($request->isAjax) {
//            /*
//            *   Process for ajax request
//            */
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            if ($request->isGet) {
//                return [
//                    'title' => "Изменить заказ #" . $id,
//                    'content' => $this->renderAjax('update', [
//                        'model' => $model,
//                        'formulasSearchModel' => $formulasSearchModel,
//                        'formulasDataProvider' => $formulasDataProvider,
//                        'orderIngredientSearchModel' => $orderIngredientSearchModel,
//                        'orderIngredientDataProvider' => $orderIngredientDataProvider,
//                        'stepSearchModel' => $stepSearchModel,
//                        'stepDataProvider' => $stepDataProvider
//                    ]),
//                    'footer' => Html::button('Отмена',
//                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
//                ];
//            } else {
//                if ($model->load($request->post()) && $model->nextStep()) {
//                    return [
//                        'forceReload' => '#crud-datatable-pjax',
//                        'title' => "заказ #" . $id,
//                        'content' => $this->renderAjax('view', [
//                            'model' => $model,
//                        ]),
//                        'footer' => Html::button('Отмена',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Изменить', ['update', 'id' => $id],
//                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
//                    ];
//                } else {
//                    return [
//                        'title' => "Изменить заказ #" . $id,
//                        'content' => $this->renderAjax('update', [
//                            'model' => $model,
//                            'formulasSearchModel' => $formulasSearchModel,
//                            'formulasDataProvider' => $formulasDataProvider,
//                            'orderIngredientSearchModel' => $orderIngredientSearchModel,
//                            'orderIngredientDataProvider' => $orderIngredientDataProvider,
//                            'stepSearchModel' => $stepSearchModel,
//                            'stepDataProvider' => $stepDataProvider
//                        ]),
//                        'footer' => Html::button('Отмена',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
//                    ];
//                }
//            }
//        } else {
//            /*
//            *   Process for non-ajax request
//            */
//            if ($model->load($request->post()) && $model->nextStep()) {
//                return $this->redirect(['update', 'id' => $model->id]);
//            } else {
////                var_dump($model->errors);
////                exit;
//                if ($model->errors) {
//                    Yii::error($model->errors, '_error');
//                } else {
//                    Yii::warning('NextStep - false', 'test');
//                }
//
//                if ($model->step == -1) {
//                    $model->scenario = Order::SCENARIO_START;
//                }
//
//                return $this->render('update', [
//                    'model' => $model,
//                    'formulasSearchModel' => $formulasSearchModel,
//                    'formulasDataProvider' => $formulasDataProvider,
//                    'orderIngredientSearchModel' => $orderIngredientSearchModel,
//                    'orderIngredientDataProvider' => $orderIngredientDataProvider,
//                    'stepSearchModel' => $stepSearchModel,
//                    'stepDataProvider' => $stepDataProvider
//                ]);
//            }
//        }
//    }
//    public function actionUpdate($id = null, $forward = 1)
//    {
//        $request = Yii::$app->request;
//
//        if (!$id && $request->isGet) {
//            $model = new Order();
//            $model->manager_id = Yii::$app->user->identity->id;
//            $model->market_id = $model->manager->market_id ?? null;
//            $model->price = 0;
//            $model->step = 1;
//            $model->next_step = 2;
//            if (!$model->save(false)) {
//                Yii::error($model->errors, '_error');
//                throw new ServerErrorHttpException('Внутренняя ошибка');
//            }
//
//            if ($model->client_id != null) {
//                $client = $model->client;
//            } else {
//                $client = new Client();
//            }
//
//            //Сохраняем лог по переходу к шагу заказа
//            $order_step = new OrderStep();
//            $order_step->order_id = $model->id;
//            $order_step->user_id = $model->manager_id;
//            $order_step->datetime = date('Y-m-d H:i:s', time());
//            $order_step->step = $model->step;
//            if (!$order_step->save()) {
//                Yii::error($order_step->errors, '_error');
//            }
//
//            return $this->render('update', [
//                'model' => $model,
//                'client' => $client ?? null,
//            ]);
//
//        }elseif (!$id && $request->isPost){
//            $model = $this->findModel($_POST['Order']['id']);
//        } else {
//            $model = $this->findModel($id);
//        }
//
//        if ($request->isGet){
//            if (!$forward){
//                $forward = 1;
//            }
//            $model->step += $forward;
//            $model->next_step += $model->step + 1;
//        }
//
//
//
////        Yii::info('Формула в модели: ' . (int)$model->formula_id, 'test');
////        Yii::info('$forward = ' . $forward, 'test');
//
//        if ($model->client_id != null) {
//            $client = $model->client;
//            $sex = $model->client->male;
//        } else {
//            $client = new Client();
//            $sex = null;
//        }
//
//        if ($model->step > 1 && $model->step < 5){
//            Yii::info('Prepare flavors. Step = ' . $model->step, 'test');
//            //Получение данных для вывода в анкете ароматов (шаг 3)
//            $model->prepareQuestionnaireFlavors();
//            $model->prepareLikePerfume();
//        } else {
//            Yii::info('Prepare skipped. Step = ' . $model->step, 'test');
//        }
//
//        if ($model->step > 2 && $model->step < 8) {
//            $formulasSearchModel = new FormulasSearch();
//            $formulasSearchModel->sex = $sex;
//            $formulasDataProvider = $formulasSearchModel->searchComplicated($model);
//        }
//
//        $orderIngredientSearchModel = new OrderIngredientSearch();
//        $orderIngredientDataProvider = $orderIngredientSearchModel->search(Yii::$app->request->queryParams);
//        $orderIngredientDataProvider->query->andWhere(['order_id' => $id]);
//
//        $stepSearchModel = new OrderStepSearch();
//        $stepDataProvider = $stepSearchModel->search(Yii::$app->request->queryParams);
//        $stepDataProvider->query->andWhere(['order_id' => $id]);
//
//        if ($request->isPost) {
//            Yii::info('Is POST', 'test');
//            Yii::info($request->post(), 'test');
//            Yii::info('Current Step: ' . $model->step, 'test');
//
//            $model->scenario = Order::steps()[$model->step];
//
//            Yii::info($model->scenario, 'test');
//
//            Yii::info('Order before save: ', 'test');
//            Yii::info($model->toArray(), 'test');
//
//            if ($model->load($request->post()) && $model->validate() && $model->nextStep()) {
//                if ($client->load($request->post())) {
//                    if (!$client->save()){
//                        Yii::error($client->errors, '_error');
//                        $model->step = 2;
//                    }
//                    $model->client_id = $client->id;
//                }
//                Yii::info('Step перед сохранением: ' . $model->step, 'test');
//                if (!$model->save()) {
//                    Yii::error($model->errors, '_error');
//                }
//
//                Yii::info('Order after save: ', 'test');
//                Yii::info($model->toArray(), 'test');
//
//            }
//            if ($model->hasErrors()) {
//                Yii::info($model->scenario, 'test');
//                Yii::error($model->errors, '_error');
//            }
//        } else {
//            Yii::info('Is GET', 'test');
////            $model->save(false);
//            Yii::info('Next Step = ' . $model->next_step, 'test');
//        }
//
//        if ($model->next_step > 10) {
//            return $this->goHome();
//        }
//
//        //Перед рендером сохраняем начало шага заказа
//
//        //Сохраняем лог по переходу к шагу заказа
//        $order_step = new OrderStep();
//        $order_step->order_id = $model->id;
//        $order_step->user_id = $model->manager_id;
//        $order_step->datetime = date('Y-m-d H:i:s', time());
//        $order_step->step = $model->step;
//        if (!$order_step->save()) {
//            Yii::error($order_step->errors, '_error');
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//            'formulasSearchModel' => $formulasSearchModel ?? null,
//            'formulasDataProvider' => $formulasDataProvider ?? null,
//            'orderIngredientSearchModel' => $orderIngredientSearchModel ?? null,
//            'orderIngredientDataProvider' => $orderIngredientDataProvider ?? null,
//            'stepSearchModel' => $stepSearchModel ?? null,
//            'stepDataProvider' => $stepDataProvider ?? null,
//            'client' => $client ?? null,
//        ]);
//    }

    public function actionUpdate_2($id, $forward = 1)
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;

        $order_model = $this->findModel($id);

        $order_model->forward = $forward;

        $client_model = $order_model->client ?? null;
        if (!$client_model) {
            Yii::info('Создаем клиента', 'test');
            $client_model = new Client();
        }

//        Yii::info('Модель клиента:', 'test');
//        Yii::info($client_model->attributes, 'test');

        if ($request->isGet) {
            Yii::info('Is GET request', 'test');
            Yii::info('$forward: ' . $forward, 'test');
            Yii::info('$step: ' . $order_model->step, 'test');
            Yii::info($session, 'test');

            if ($forward == -1 && $session->get('current_step') != 10) {
                $order_model->step = $session->get('prev_step');
            } else {
                $order_model->step = $session->get('current_step') + $order_model->forward;
            }

            Yii::info('$step: ' . $order_model->step, 'test');

            if ($order_model->step == 3) {
                Yii::info('Подготовка модели на шаге 3', 'test');
                //Получение данных для вывода в анкете ароматов (шаг 3)
                $order_model->prepareQuestionnaireFlavors();
                $order_model->prepareLikePerfume();
            } elseif ($order_model->step == 4 || $order_model->step == 5) {
                Yii::info('Формирование датапровайдеров для 4 или 5 шага', 'test');

                //Шаг 4. Дегустация ингредиентов
                $formulasSearchModel = new FormulasSearch();
                $formulasSearchModel->sex = $order_model->client->male ?? null;
                $formulasDataProvider = $formulasSearchModel->searchComplicated($order_model);
            } elseif ($order_model->step == 6 || $order_model->step == 7 || $order_model->step == 8) {
                Yii::info('Формирование датапровайдеров для 6 или 7 или 8 шага', 'test');

                $orderIngredientSearchModel = new OrderIngredientSearch();
                $orderIngredientDataProvider = $orderIngredientSearchModel->search(Yii::$app->request->queryParams);
                $orderIngredientDataProvider->query->andWhere(['order_id' => $id]);
                $orderIngredientDataProvider->pagination = false;
            } elseif ($order_model->step == 10) {
                Yii::info('Формирование датапровайдеров для 10 шага (История заказа)', 'test');

                $stepSearchModel = new OrderStepSearch();
                $stepDataProvider = $stepSearchModel->search(Yii::$app->request->queryParams);
                $stepDataProvider->query->andWhere(['order_id' => $id]);
                $changeLogSearchModel = new ChangeLogSearch();
                $changeLogDataProvider = $changeLogSearchModel->search(Yii::$app->request->queryParams);
                $changeLogDataProvider->query->andWhere(['order_id' => $id]);

            } elseif ($order_model->step == 11) {
                Yii::info('Шаг 11', 'test');
                return $this->goHome();
            } else {
                Yii::info('Формирование датапровайдеров пропущено. Текущий шаг: ' . $order_model->step, 'test');
            }

            if (!$order_model->next_step) {
                $order_model->next_step = $order_model->step + 1;
            }

//            $order_model->setStepInfo($order_model->step);

            return $this->render('update', [
                'model' => $order_model,
                'client' => $client_model,
                'formulasSearchModel' => $formulasSearchModel ?? null,
                'formulasDataProvider' => $formulasDataProvider ?? null,
                'orderIngredientSearchModel' => $orderIngredientSearchModel ?? null,
                'orderIngredientDataProvider' => $orderIngredientDataProvider ?? null,
                'stepSearchModel' => $stepSearchModel ?? null,
                'stepDataProvider' => $stepDataProvider ?? null,
                'changeLogSearchModel' => $changeLogSearchModel ?? null,
                'changeLogDataProvider' => $changeLogDataProvider ?? null,
            ]);
        } else {
            Yii::info('Is POST request', 'test');

            //Пост запрос
            if ($order_model->load($request->post())) {
                $order_model->scenario = $order_model->steps()[$order_model->step];

                $next_step_result = (bool)$order_model->nextStep();
                Yii::info('Результат Next Step: ' . $next_step_result, 'test');

                if ($order_model->scenario == Order::SCENARIO_CLIENT) {
                    //Сохраняем клиента
                    if ($client_model->load($request->post()) && $client_model->save()) {
                        Yii::info('Client saved success', 'test');
                    } else {
                        if ($client_model->hasErrors()) {
                            Yii::error($client_model->errors, '_error');
                        } else {
                            Yii::warning('Данные клиента не получены', 'test');
                        }
                    }
                }

                Yii::info($order_model->attributes, 'test');
                if (!$order_model->save()) {
                    Yii::error($order_model->errors, '_error');
                }

                //Едем дальше
                if (!$order_model->hasErrors()) {
                    $order_model->step = $order_model->next_step;
                    $order_model->next_step += 1;
                    $order_model->forward = 1;
                }

                $client_model = $order_model->client ?? new Client();
                if ($order_model->step == 3) {
                    Yii::info('Подготовка модели на шаге 3', 'test');
                    //Получение данных для вывода в анкете ароматов (шаг 3)
                    $order_model->prepareQuestionnaireFlavors();
                    $order_model->prepareLikePerfume();
                } elseif ($order_model->step == 4 || $order_model->step == 5) {
                    //Шаг 4. Дегустация ингредиентов
                    $formulasSearchModel = new FormulasSearch();
                    $formulasSearchModel->sex = $order_model->client->male ?? null;
                    $formulasDataProvider = $formulasSearchModel->searchComplicated($order_model);
                } elseif ($order_model->step == 6 || $order_model->step == 7 || $order_model->step == 8) {
                    $orderIngredientSearchModel = new OrderIngredientSearch();
                    $orderIngredientDataProvider = $orderIngredientSearchModel->search(Yii::$app->request->queryParams);
                    $orderIngredientDataProvider->query->andWhere(['order_id' => $id]);
                    $orderIngredientDataProvider->pagination = false;
                } elseif ($order_model->step == 10) {
                    $stepSearchModel = new OrderStepSearch();
                    $stepDataProvider = $stepSearchModel->search(Yii::$app->request->queryParams);
                    $stepDataProvider->query->andWhere(['order_id' => $id]);
                    $changeLogSearchModel = new ChangeLogSearch();
                    $changeLogDataProvider = $changeLogSearchModel->search(Yii::$app->request->queryParams);
                    $changeLogDataProvider->query->andWhere(['order_id' => $id]);
                } elseif ($order_model->step == 11) {
                    return $this->goHome();
                }

//                $order_model->setStepInfo($order_model->step);

                return $this->render('update', [
                    'model' => $order_model,
                    'client' => $client_model,
                    'formulasSearchModel' => $formulasSearchModel ?? null,
                    'formulasDataProvider' => $formulasDataProvider ?? null,
                    'orderIngredientSearchModel' => $orderIngredientSearchModel ?? null,
                    'orderIngredientDataProvider' => $orderIngredientDataProvider ?? null,
                    'stepSearchModel' => $stepSearchModel ?? null,
                    'stepDataProvider' => $stepDataProvider ?? null,
                    'changeLogSearchModel' => $changeLogSearchModel ?? null,
                    'changeLogDataProvider' => $changeLogDataProvider ?? null,
                ]);
            }
        }
        return $this->goHome();
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;

        $order_model = $this->findModel($id);
        $client_model = $order_model->client ?? null;
        $errors = [];


//        echo $order_model->step;
//        exit;

        if (!$client_model) {
            Yii::info('Создаем клиента', 'test');
            $client_model = new Client();
        }
        Yii::info($client_model->attributes, 'test');
        Yii::info('Is GET request - ' . (string)$request->isGet, 'test');
        Yii::info('$step: ' . $order_model->step, 'test');
        Yii::info('$next_step: ' . $order_model->next_step, 'test');

        if ($order_model->load($request->post())) {
            Yii::info('Закааз загружен:', 'test');
            Yii::info($order_model->attributes, 'test');

            $next_step_result = (bool)$order_model->nextStep();
            Yii::info('Result nextStep function: ' . (string)$next_step_result, 'test');
            Yii::info('$step: ' . $order_model->step, 'test');
            Yii::info('$next_step: ' . $order_model->next_step, 'test');

            if (!$order_model->save()) {
                Yii::error($order_model->errors, '_error');
//                $order_model->step -=1;
//                $order_model->next_step -=1;
                $errors[] = array_values($order_model->errors)[0];
            } else {
                Yii::info('Заказ сохранен успешно');
            }
        }

        if ($order_model->step == 3) {
            Yii::info('Подготовка модели на шаге 3', 'test');
            //Получение данных для вывода в анкете ароматов (шаг 3)
            $order_model->prepareQuestionnaireFlavors();
            $order_model->prepareLikePerfume();
        } elseif ($order_model->step == 4 || $order_model->step == 5) {
            Yii::info('Формирование датапровайдеров для 4 или 5 шага', 'test');

            //Шаг 4. Дегустация ингредиентов
            $formulasSearchModel = new FormulasSearch();
            $formulasSearchModel->sex = $order_model->client->male ?? null;
            $formulasDataProvider = $formulasSearchModel->searchComplicated($order_model);
        } elseif ($order_model->step == 6 || $order_model->step == 7 || $order_model->step == 8) {
            Yii::info('Формирование датапровайдеров для 6 или 7 или 8 шага', 'test');

            $orderIngredientSearchModel = new OrderIngredientSearch();
            $orderIngredientDataProvider = $orderIngredientSearchModel->search(Yii::$app->request->queryParams);
            $orderIngredientDataProvider->query->andWhere(['order_id' => $id]);
            $orderIngredientDataProvider->pagination = false;
        } elseif ($order_model->step == 10) {
            Yii::info('Формирование датапровайдеров для 10 шага (История заказа)', 'test');

            $stepSearchModel = new OrderStepSearch();
            $stepDataProvider = $stepSearchModel->search(Yii::$app->request->queryParams);
            $stepDataProvider->query->andWhere(['order_id' => $id]);
            $changeLogSearchModel = new ChangeLogSearch();
            $changeLogDataProvider = $changeLogSearchModel->search(Yii::$app->request->queryParams);
            $changeLogDataProvider->query->andWhere(['order_id' => $id]);

        } elseif ($order_model->step == 11) {
            Yii::info('Шаг 11', 'test');
            return $this->goHome();
        } else {
            Yii::info('Формирование датапровайдеров пропущено. Текущий шаг: ' . $order_model->step, 'test');
        }

        if (!$client_model->id && $order_model->client_id){
            //Срабатывает только при переходе ко второму шагу после создания заказа
            $client_model = Client::findOne($order_model->client_id);
        }




//        echo $order_model->step;
//        exit;

        return $this->render('update', [
            'model' => $order_model,
            'client' => $client_model,
            'formulasSearchModel' => $formulasSearchModel ?? null,
            'formulasDataProvider' => $formulasDataProvider ?? null,
            'orderIngredientSearchModel' => $orderIngredientSearchModel ?? null,
            'orderIngredientDataProvider' => $orderIngredientDataProvider ?? null,
            'stepSearchModel' => $stepSearchModel ?? null,
            'stepDataProvider' => $stepDataProvider ?? null,
            'changeLogSearchModel' => $changeLogSearchModel ?? null,
            'changeLogDataProvider' => $changeLogDataProvider ?? null,
            'errors' => $errors,
        ]);
    }

    public function actionSortingIngredients()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = json_decode($_POST['data']);

        Yii::info($_POST, 'test');

        for ($i = 0; $i < count($data); $i++) {
            $question = OrderFormulasIngredient::findOne($data[$i]);
            $question->mark = $i;
            if (!$question->save(false)) {
                Yii::error($question->errors, '_error');
            }
        }

        return $data;
    }

    /**
     * Delete an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    /**
     * Расчитываем цену заказа
     * @return array
     */
    public function actionCalcPrice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;

        $model = new Order();

        Yii::info($_POST, 'test');

        if ($model->load($request->post())) {
//            Yii::info($model->attributes, 'test');
            return $model->calcPrice();
        } else {
            $success = 0;
            $data = 'Форма не загружена';
        }

        return ['success' => $success, 'data' => $data];
    }

    /**
     * Получает список заказов клиента
     * @param int $id Идентификатор клиента
     * @return array
     */
    public function actionClientOrders($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $client = Client::findOne($id) ?? null;

        if (!$client) {
            return [
                'title' => 'Ошибка.',
                'content' => 'Клиент не найден!',
            ];
        }

        $searchModel = new OrderSearch(['client_id' => $client->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'title' => 'Заказы клиента',
            'size' => 'large',
            'content' => $this->renderAjax('_client_orders', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]),
        ];
    }

    public function actionLog()
    {
        $request = Yii::$app->request;

        if ($request->isGet) {
            return ['success' => false, 'data' => 'Некорректный запрос'];
        }

        return (new ChangeLog)->log($request->post());

    }

    /**
     * Показывает форму выбора ингредиента на 4 шаге (Дегустация ингредиентов)
     * @param $order_id
     * @return array
     */
    public function actionViewIngredients($order_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'title' => 'Добавление ингредиента',
            'size' => 'large',
            'content' => $this->renderAjax('_select_ingredient.php'),
            'footer' => Html::button('Добавить', [
                'id' => 'add-ingredient',
                'data-order' => $order_id,
                'class' => 'btn btn-info',
                'data-dismiss' => 'modal',
            ])
        ];
    }

    /**
     * Удаляет ингредиент из заказа
     * @param int $id Идентификатор order_formulas_ingredient
     * @return array
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteIngredient($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $ofi = OrderFormulasIngredient::findOne($id) ?? null;

        if ($ofi) {
            if ($ofi->delete()) {
                return ['success' => 1];
            }
        }
        return ['success' => 0];
    }

    public function actionStepBack($id, $value)
    {
        Yii::info('StepBack function start', 'test');
        $order_model = Order::findOne($id);

        Yii::info('Step: ' . $order_model->step, 'test');

        $order_model->step = $order_model->step - (int)$value;
        $order_model->next_step = $order_model->step + 1;

        Yii::info('Step: ' . $order_model->step, 'test');

        if (!$order_model->save(false)){
            Yii::error($order_model->errors, '_error');
        }

        $this->redirect(['update','id' => $order_model->id]);
    }
}
