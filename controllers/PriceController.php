<?php

namespace app\controllers;

use app\models\PriceCost;
use Yii;
use app\models\Price;
use app\models\PriceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * PriceController implements the CRUD actions for Price model.
 */
class PriceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Price models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Price model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $price_model = $this->findModel($id);
        $cost_models = PriceCost::findAll(['price_id' => $price_model->id]) ?? null;
        $types = PriceCost::find()->select('DISTINCT(type)')->andWhere(['price_id' => $price_model->id])->all();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => $price_model->name,
                'content' => $this->renderAjax('view', [
                    'price_model' => $price_model,
                    'cost_models' => $cost_models,
                    'types' => $types,
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'price_model' => $price_model,
                'cost_models' => $cost_models,
                'types' => $types,
            ]);
        }
    }

    /**
     * Creates a new Price model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $price_model = new Price();
        $cost_model = new PriceCost();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавление услуг в прайс лист",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'price_model' => $price_model,
                        'cost_model' => $cost_model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($price_model->load($request->post()) && $price_model->save()) {
                    if ($cost_model->load($request->post())) {
                        Yii::info($cost_model->types, 'test');
                    } else {
                        Yii::error($cost_model->errors, '_error');
                    }

                    for ($i = 0; $i < count($cost_model->types); $i++) {
                        if ($cost_model->types[$i] && $cost_model->volumes[$i] && $cost_model->costs[$i]) {
                            $new_cost_model = new PriceCost();
                            $new_cost_model->type = $cost_model->types[$i];
                            $new_cost_model->volume = $cost_model->volumes[$i];
                            $new_cost_model->cost = $cost_model->costs[$i];
                            $new_cost_model->price_id = $price_model->id;
                            $new_cost_model->save();
                        }

                    }

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Добавление услуг в прайс лист",
                        'content' => '<span class="text-success">Услуга добавлена успешно</span>',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Добавить ещё', ['create'],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Добавление услуг в прайс лист",
                        'size' => 'large',
                        'content' => $this->renderAjax('create', [
                            'price_model' => $price_model,
                            'cost_model' => $cost_model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($price_model->load($request->post()) && $price_model->save()) {
                return $this->redirect(['view', 'id' => $price_model->id]);
            } else {
                return $this->render('create', [
                    'price_model' => $price_model,
                    'cost_model' => $cost_model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $price_model = $this->findModel($id);
        $cost_models = PriceCost::findAll(['price_id' => $price_model->id]) ?? null;
        if (!$cost_models) {
            $cost_models = new PriceCost(['price_id' => $price_model->id]);
        }
        $cost_model = new PriceCost();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактирование услуги " . $price_model->name,
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'price_model' => $price_model,
                        'cost_models' => $cost_models,
                        'cost_model' => $cost_model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($price_model->load($request->post()) && $price_model->save()) {
                    $cost_model->load($request->post());
                    //Удаляем все цены для услуги
                    PriceCost::deleteAll(['price_id' => $price_model->id]);
                    //Сохраняем
                    for ($i = 0; $i < count($cost_model->types); $i++) {
                           if ($cost_model->types[$i] && $cost_model->volumes[$i] && $cost_model->costs[$i]) {
                               $new_cost_model = new PriceCost();
                               $new_cost_model->type = $cost_model->types[$i];
                               $new_cost_model->volume = $cost_model->volumes[$i];
                               $new_cost_model->cost = $cost_model->costs[$i];
                               $new_cost_model->price_id = $price_model->id;
                               $new_cost_model->save();
                           }

                       }
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Редактирование услуги" . $price_model->name,
                        'size' => 'large',
                        'content' => $this->renderAjax('update', [
                            'price_model' => $price_model,
                            'cost_models' => $cost_models,
                            'cost_model' => $cost_model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($price_model->load($request->post()) && $price_model->save()) {
                return $this->redirect(['view', 'id' => $price_model->id]);
            } else {
                return $this->render('update', [
                    'price_model' => $price_model,
                    'cost_models' => $cost_models,
                    'cost_model' =>$cost_model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Price model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Price model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Price the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Price::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionViewPrice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var array $data Массив с типами и данными по каждой услуге*/
        $data = (new Price())->getFullPrice();

        return [
            'title' => 'Прайс-лист услуг',
            'content' => $this->renderAjax('_full_price', [
                'data' => $data,
            ]),
            'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]),
        ];
    }
}
