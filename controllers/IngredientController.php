<?php

namespace app\controllers;

use app\models\forms\IngredientImportForm;
use app\models\FormulasIngredient;
use app\models\LevelTriangleFlavor;
use app\models\OrderFormulasIngredient;
use app\models\Remain;
use Yii;
use app\models\Ingredient;
use app\models\IngredientSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * IngredientController implements the CRUD actions for Ingredient model.
 */
class IngredientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ingredient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IngredientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ingredient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "ингредиент #" . $id,
                'size' => 'large',
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Ingredient model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Ingredient();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить ингредиент",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Добавить ингредиент",
                        'content' => '<span class="text-success">Создание ингредиента успешно завершено</span>',
                        'footer' => Html::button('ОК',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Добавить ингредиент",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function actionUploadExcelData()
    {
//        /** Load $inputFileName to a Spreadsheet Object  **/
//        $spreadsheet = IOFactory::load($file);
//
//        $sheet = $spreadsheet->getActiveSheet();
//        $skipCount =0;
//        $savedCount = 0;
//        foreach ($sheet->getRowIterator() as $row){
//            $cells = $row->getCellIterator();
//
//            $rowAddress=[];
//
//            foreach ($cells as $cell){
//                if ($cell->getColumn() == "A") {
//                    $rowAddress['town'] = $cell->getValue();
//                }
//                if ($cell->getColumn() == "B") {
//                    $rowAddress['region'] = $cell->getValue();
//                }
//                if ($cell->getColumn() == "C") {
//                    $rowAddress['street'] = $cell->getValue();
//                }
//                if ($cell->getColumn() == "D") {
//                    $rowAddress['house'] = $cell->getValue();
//                }
//                if ($cell->getColumn() == "E") {
//                    $rowAddress['housing'] = $cell->getValue();
//                    yii::info($rowAddress['housing'],'test');
//                }
//                if ($cell->getColumn() == "F") {
//                    $rowAddress['entrance'] = $cell->getValue();
//                }
//                if ($cell->getColumn() == "G") {
//                    $rowAddress['floor'] = $cell->getValue();
//                }
//                if ($cell->getColumn() == "H") {
//                    $rowAddress['apartament'] = $cell->getValue();
//                }
//                if ($cell->getColumn() == "J") {
//                    $rowAddress['porter'] = $cell->getValue();
//                }
//            }
//            if ($rowAddress['town']==null && $rowAddress['street']==null) {
//                break;
//            }
//            if (self::issetAddress($rowAddress['town'],$rowAddress['region'],$rowAddress['street'],$rowAddress['house'],$rowAddress['housing'])) {
//                $skipCount++;
//            } else {
//
//                if(self::addToAddressList($rowAddress)){
//                    $savedCount++;
//                }
//            }
//
//        }

        $request = Yii::$app->request;
        $model = new IngredientImportForm();
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load($request->post()) && $model->import()) {
            return [
                'title' => "Импорт",
                'content' => "<p>Импорт завершен</p><p>Всего: {$model->totalCount}, Загружено: {$model->savedCount}, Ошибки: {$model->skipCount}</p>",
                'footer' => Html::button('Ок', ['class' => 'btn btn-primary', 'data-dismiss' => "modal"])

            ];
        } else {
            return [
                'title' => "Импорт",
                'content' => $this->renderAjax('import', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отмена',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Импортировать', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        }
    }

    /**
     * Updates an existing Ingredient model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
//                $model->input_level *= 100;
                $model->solvent_percent *= 100;
                return [
                    'title' => "Изменить ингредиент #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "ингредиент #" . $id,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Изменить', ['update', 'id' => $id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Изменить ингредиент #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Ingredient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Ingredient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Ingredient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ingredient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ingredient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    public function actionDelAll($pass)
    {
        if ($pass != 'epvewrmorn') {
            return 'false';
        }

        Ingredient::deleteAll();
        LevelTriangleFlavor::deleteAll();

        return 'true';
    }

    /**
     * Есть или нет ингредиент на складе
     * @param int $id Идентификатор ингредиента
     * @return int
     */
    public function actionGetInStorage($id)
    {
        $ingredient = Ingredient::findOne($id);

        return (bool)$ingredient->has_in_storage;
    }

    /**
     * @param int $id Идентификатор ингредиента
     * @param int $order_id Идентификатор заказа
     * @return string
     */
    public function actionGetIngredient($id, $order_id)
    {
        $ingredient = Ingredient::findOne($id);

        //В списке на шаге 4 (дегустация ингредиентов) выводятся ингредиенты подобраных формул
        //поэтому сначала находим первую попавшуюся формулу с выбранным ингредиентом и добавляем формулу и ингредиент формулы

        //Ищем первую попавшуюся формулу с нужным ингредиентом
        /** @var FormulasIngredient $formulas_ingredients */
        $formulas_ingredients = FormulasIngredient::find()
                ->andWhere(['ingredient_id' => $id])->one() ?? null;

        if (!$formulas_ingredients) {
            Yii::error('Ингредиент не принадлежит ни одной формуле');
            return false;
        }

        //Добавляем в order_formulas_ingredient
        $ofi = new OrderFormulasIngredient([
            'order_id' => $order_id,
            'formulas_ingredient_id' => $formulas_ingredients->id,
            'mark' => 0,
        ]);

        if (!$ofi->save()) {
            Yii::error($ofi->errors, '_error');
        }

        /** @var Remain $remain */
        $remain = Remain::find()->where([
            'market_id' => Yii::$app->user->identity->market_id,
            'ingredient_id' => $id
        ])->one() ?? 0;

        $remain ? $amount = $remain->amount : $amount = 0;
        $del_btn = Html::a('x', ['delete-ingredient', 'id' => $ofi->id], [
            'role' => 'modal-remote',
            'title' => 'Удалить',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы действительно хотите удалить ' . $ingredient->name_rus . '?',
        ]);
        $otherInfo = "<span class='text-success pull-right'>" . Yii::$app->formatter->asDecimal($amount) . "<span class='del-ing'>$del_btn</span></span>";

        return '<li draggable="true" role="option" aria-grabbed="false">
                    <div class="ingredient-id" data-id="' . $ofi->id . '">
                        <span class="counter">Новый.</span> ' . $ingredient->name_rus . $otherInfo . '</i>
                    </div>
                </li>';
    }
}
