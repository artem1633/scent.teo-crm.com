<?php

namespace app\controllers;

use app\models\Order;
use Yii;
use app\models\OrderIngredient;
use app\models\OrderIngredientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * OrderIngredientController implements the CRUD actions for OrderIngredient model.
 */
class OrderIngredientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderIngredient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderIngredientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderIngredient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "ингредиент #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new OrderIngredient model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @param $order_id
     * @return mixed
     */
    public function actionCreate($order_id)
    {
        $request = Yii::$app->request;
        $model = new OrderIngredient();
        $model->order_id = $order_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить ингредиент",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit", 'data-pjax' => 1])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    //Костыль
                    $session = Yii::$app->session;
                    Yii::info('current_step: ' . $session->get('current_step'), 'test');

                    if ($session->get('current_step') == 7) {
                        $session->set('current_step', 5);
                        $order_model = Order::findOne($model->order_id);
                        $order_model->step = 7;
                        $order_model->save(false);
                        Yii::info($order_model->toArray(), 'test');
                    }
                    return [
                        'forceReload' => '#crud-order-ingredient-datatable-container',
                        'forceClose' => true,
                    ];

//                    return [
//                        'forceReload' => '#crud-order-ingredient-datatable-pjax',
//                        'title' => "Добавить ингредиент",
//                        'content' => '<span class="text-success">Ингредиент успешно добавлен</span>',
//                        'footer' => Html::button('ОК',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
//                    ];
                } else {
                    Yii::error($model->errors, '_error');
                    return [
                        'title' => "Добавить ингредиент",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing OrderIngredient model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить ингредиент #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-order-datatable-container',
                        'forceClose' => true,
//                        'title' => "ингредиент #" . $id,
//                        'content' => $this->renderAjax('view', [
//                            'model' => $model,
//                        ]),
//                        'footer' => Html::button('Отмена',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Изменить', ['update', 'id' => $id],
//                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Изменить ингредиент #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing OrderIngredient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;

        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            Yii::$app->session->set('current_step', 5);
            Yii::$app->session->set('ingredient_deleted', 1);

            return [
                'forceReload' => '#crud-order-ingredient-datatable-container',
                'forceClose' => true,
            ];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing OrderIngredient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the OrderIngredient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderIngredient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderIngredient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    /**
     * Увеличивает кол-во ингредиента
     * @param int $id Идентификатор OrderIngredient
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionIncrease($id)
    {
        $request = Yii::$app->request;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $oi_model = $this->findModel($id) ?? null;

        if ($request->isGet) {

            return [
                'title' => 'Увеличение кол-ва ингредиента',
                'size' => 'small',
                'content' => $this->renderAjax('_change_value_form', [
                    'model' => $oi_model,
                    'increase' => true,
                ]),
                'footer' => Html::submitButton('Принять изменения', [
                    'class' => 'btn btn-success btn-block',
                ]),
            ];
        } else {
            if ($oi_model->load(Yii::$app->request->post())) {
                $oi_model->count += $oi_model->increase_value;
                if (!$oi_model->save()) {
                    Yii::error($oi_model->errors, '_error');
                    return [
                        'title' => 'Ошибка.',
                        'content' => 'При увеличении кол-ва ингредиента произошла ошибка: ' . json_encode($oi_model->errors),
                    ];
                }
                Yii::$app->session->set('current_step', 5);
                return [
                    'forceReload' => '#crud-order-ingredient-datatable-container',
                    'forceClose' => true,
                ];
            }
        }
        return ['success' => 0];
    }

    /**
     * Уменьшает кол-во ингредиента
     * @param int $id Идентификатор OrderIngredient
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDecrease($id)
    {
        $request = Yii::$app->request;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $oi_model = $this->findModel($id) ?? null;

        if ($request->isGet) {
            return [
                'title' => 'Уменьшение кол-ва ингредиента',
                'size' => 'small',
                'content' => $this->renderAjax('_change_value_form', [
                    'model' => $oi_model,
                    'increase' => false,
                ]),
                'footer' => Html::submitButton('Принять изменения', [
                    'class' => 'btn btn-success btn-block',
                ]),
            ];
        } else {
            if ($oi_model->load(Yii::$app->request->post())) {
                //Добавляем остальным ингредиентам укзанное значение
                /** @var OrderIngredient $o_ingredient */
                foreach (OrderIngredient::find()->andWhere(['<>', 'id', $oi_model->id])->each() as $o_ingredient){
                    $o_ingredient->count += $oi_model->decrease_value;
                    if (!$o_ingredient->save()){
                        Yii::error($o_ingredient->errors, '_error');
                    }
                }
               $oi_model->decrease_value;
                if (!$oi_model->save()) {
                    Yii::error($oi_model->errors, '_error');
                    return [
                        'title' => 'Ошибка.',
                        'content' => 'При увеличении кол-ва ингредиента произошла ошибка: ' . json_encode($oi_model->errors),
                    ];
                }
                Yii::$app->session->set('current_step', 5);
                return [
                    'forceReload' => '#crud-order-ingredient-datatable-container',
                    'forceClose' => true,
                ];
            }
        }
        return ['success' => 0];
    }
}
