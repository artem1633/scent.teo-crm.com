<?php

namespace app\controllers;

use app\helpers\ImportCity;
use app\models\forms\ResetPasswordForm;
use app\models\ImportantFlover;
use app\models\Order;
use app\models\OrderIngredient;
use app\models\Perfume;
use app\models\Remain;
use app\models\User;
use PHPHtmlParser\Dom;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        ImportCity::import();
    }

    public function actionTest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
//
//        $output = [];
//
//        for ($i = 0; $i < 1915; $i++){
//            $ch = curl_init();
//
//            $page = $i + 1;
//
//            curl_setopt($ch, CURLOPT_URL,"https://fgvi612dfz-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20instantsearch.js%20(3.7.0)%3B%20Vue%20(2.6.10)%3B%20Vue%20InstantSearch%20(2.6.0)%3B%20JS%20Helper%20(2.28.0)&x-algolia-application-id=FGVI612DFZ&x-algolia-api-key=NTFjYjQ5MmExN2ZlNWU2M2YwN2NjYTc3YmE2ZDQzODQxZjMwYjA2MTY1MjE0YmUzNDM3MjVjZjliN2E3MjVhN3ZhbGlkVW50aWw9MTU3Nzg2Mzc5MA%3D%3D");
////        curl_setopt($ch, CURLOPT_URL,"https://fgvi612dfz-dsn.algolia.net/1/indexes/fragrantica_perfumes/611?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20instantsearch.js%20(3.7.0)%3B%20Vue%20(2.6.10)%3B%20Vue%20InstantSearch%20(2.6.0)%3B%20JS%20Helper%20(2.28.0)&x-algolia-application-id=FGVI612DFZ&x-algolia-api-key=NTFjYjQ5MmExN2ZlNWU2M2YwN2NjYTc3YmE2ZDQzODQxZjMwYjA2MTY1MjE0YmUzNDM3MjVjZjliN2E3MjVhN3ZhbGlkVW50aWw9MTU3Nzg2Mzc5MA%3D%3D");
////        curl_setopt($ch, CURLOPT_URL,"https://fgvi612dfz-dsn.algolia.net/1/indexes/fragrantica_perfumes/facets/ingredients.RU/query?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20instantsearch.js%20(3.7.0)%3B%20Vue%20(2.6.10)%3B%20Vue%20InstantSearch%20(2.6.0)%3B%20JS%20Helper%20(2.28.0)&x-algolia-application-id=FGVI612DFZ&x-algolia-api-key=NTFjYjQ5MmExN2ZlNWU2M2YwN2NjYTc3YmE2ZDQzODQxZjMwYjA2MTY1MjE0YmUzNDM3MjVjZjliN2E3MjVhN3ZhbGlkVW50aWw9MTU3Nzg2Mzc5MA%3D%3D");
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS,
//                '{"requests":[{"indexName":"fragrantica_perfumes","params": "query=&hitsPerPage=80&page='.$page.'&attributesToRetrieve=*"}]}');
//
////        curl_setopt($ch, CURLOPT_POSTFIELDS, '{"params": ""}');
//
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//            $server_output = json_decode(curl_exec($ch), true);
//
////        curl_close ($ch);
//
////            $names = [];
//
//            foreach ($server_output['results'][0]['hits'] as $item)
//            {
//                $sex = null;
//
//                if($item['spol'] == 'male'){
//                    $sex = Perfume::SEX_MALE;
//                } else if($item['spol'] == 'female'){
//                    $sex = Perfume::SEX_FEMALE;
//                } else if($item['spol'] == 'unisex'){
//                    $sex = Perfume::SEX_UNISEX;
//                }
//
//                $url = null;
//
//                if(isset($item['url']['RU'][0])){
//                    $url = $item['url']['RU'][0];
//                }
//
//                $perfume = new Perfume([
//                    'external_id' => $item['id'],
//                    'name' => $item['naslov'],
//                    'sex' => $sex,
//                    'year' => $item['godina'],
//                    'rating' => $item['rating'],
//                    'thumbnail' => $item['thumbnail'],
//                    'url' => $url,
//                ]);
//
//                $perfume->save(false);
//
//                $names[] = $item['naslov'];
//            }
//
//
////            $output[] = $names;
//        }

        $model = Order::findOne(13);

        $ingredients = OrderIngredient::find()->where(['order_id' => $model->id])->all();

        var_dump($ingredients);

        foreach ($ingredients as $ingredient) {
            /** @var Remain $remain */
            $remain = Remain::find()->where(['market_id' => $model->manager->market_id, 'ingredient_id' => $ingredient->ingredient_id])->one();

//            echo Remain::find()->where(['market_id' => $model->manager->market_id, 'ingredient_id' => $ingredient->id])->createCommand()->getRawSql();
//
//            var_dump($model->manager->market_id);
//
//            var_dump($remain);

//            if($remain){
//                $remain->amount = $remain->amount - $ingredient->count;
//                $remain->save(false);
//
//                $operation = new RemainOperation([
//                    'remain_id' => $remain->id,
//                    'method' => RemainOperation::METHOD_ORDER,
//                    'type' => RemainOperation::TYPE_WRITE_OFF,
//                    'quantity' => $ingredient->count,
//                    'amount_last' => $remain->amount,
//                    'order_id' => $this->id,
//                ]);
//
//                $operation->save(false);
//            }
//        }
        }
//        return $output;
    }

    public function actionParseFlovers()
    {
//        file_get_contents('https://www.fragrantica.ru/perfume/Chanel/Coco-Eau-de-Parfum-609.html', [
//            ''
//        ]);

//        file_get_contents('https://www.fragrantica.ru/perfume/Chanel/Coco-Eau-de-Parfum-609.html', 0, $ctx);

        $result = file_get_contents('https://www.fragrantica.ru/perfume/Chanel/Coco-Eau-de-Parfum-609.html', false, stream_context_create(array(
            'https' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
//                'content' => ''
            )
        )));
        $result = json_decode($result, true);

        exit;
        /** @var Perfume[] $perfumes */
        $perfumes = Perfume::find()->limit(1)->all();

        foreach ($perfumes as $perfume)
        {
            echo $perfume->url.'<br><br>';
            $dom = new Dom();

            $useragent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2';
            $cookieFile = 'cookie.txt';

            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, 'https://www.fragrantica.ru/perfume/Chanel/Coco-Eau-de-Parfum-609.html');

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
//            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $cookieFile);
//            curl_setopt($ch, CURLOPT_COOKIESESSION, true);
//            curl_setopt($ch, CURLOPT_STDERR,  fopen('php://stdout', 'w'));
//
//            curl_setopt($ch, CURLOPT_PROXY, '173.249.42.83:3128');

            // $output contains the output string
            $output = curl_exec($ch);

            // close curl resource to free up system resources
            curl_close($ch);

            $dom->load($output);

            $div = $dom->find('.effect6 div', 1);
//            $content = $div->find('h3')[0];

            echo $output;

            exit;
        }
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

//    /**
//     * Displays contact page.
//     *
//     * @return string
//     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }

    /**
     * Get Rate Flavor For Perfume
     */
    public function actionGRFFP()
    {
        $perfume = Perfume::findOne(1);

        VarDumper::dump((new ImportantFlover())->getRatingForPerfume($perfume), 20, true);
    }

    /**
     * Скачивание файлов логов в зависимости от типа лога
     * @param string $name Тип логов
     * Типы логов:
     * test, app, _error
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionGetLog($name)
    {
        $file_path = '';

        switch ($name) {
            case 'test':
                $file_path = Url::to('@app/runtime/logs/test.log');
                break;
            case 'app':
                $file_path = Url::to('@app/runtime/logs/app.log');
                break;
            case '_error':
                $file_path = Url::to('@app/runtime/logs/_error.log');
                break;

        }

        if (is_file($file_path)) {
            return Yii::$app->response->sendFile($file_path);
        }

        throw new NotFoundHttpException('Файл не найден');
    }
}
