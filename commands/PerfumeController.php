<?php

namespace app\commands;

use app\models\ImportantFlover;
use app\models\Perfume;
use app\models\PerfumeImportantFlover;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Class PerfumeController
 * @package app\commands
 */
class PerfumeController extends Controller
{
    /**
     *
     */
    public function actionFakeFlovers()
    {
        $perfumes = Perfume::find()->all();
        $floversPks = ArrayHelper::getColumn(ImportantFlover::find()->all(), 'id');

        foreach ($perfumes as $perfume) {
            $countFlovers = rand(2, 8);

            for ($i = 0; $i < $countFlovers; $i++)
            {
                $rate = rand(500, 2000);

                $floverId = $floversPks[rand(0, count($floversPks)-1)];

                $perfumeFlover = new PerfumeImportantFlover([
                    'perfume_id' => $perfume->id,
                    'important_flover_id' => $floverId,
                    'rating' => $rate,
                ]);
                $perfumeFlover->save(false);
            }
        }
    }
}