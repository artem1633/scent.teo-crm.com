<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Ingredient;

/**
 * Class IngredientController
 * @package app\commands
 */
class IngredientController extends Controller
{
    /**
     *
     */
    public function actionDeleteDuplicates()
    {
        $ignorePks = [];
        foreach (Ingredient::find()->all() as $model)
        {
            if(in_array($model->id, $ignorePks)){
                continue;
            }

            $repeats = Ingredient::find()->where(['name_rus' => $model->name_rus])->andWhere(['!=', 'id', $model->id])->all();

            foreach ($repeats as $repeat){
                $repeat->delete();
                $ignorePks[] = $repeat->id;
            }
        }
    }
}