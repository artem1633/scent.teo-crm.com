<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%perfume}}`.
 */
class m200318_085238_add_aroma_group_id_column_to_perfume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%perfume}}', 'aroma_group_id', $this->integer()->comment('Идентификатор группы ароматов'));

        $this->addForeignKey(
            'fk-perfume-aroma_group_id',
            '{{%perfume}}',
            'aroma_group_id',
            '{{%aroma_group}}',
            'id',
            'SET NULL',
             'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%perfume}}', 'fk-perfume-aroma_group_id');
        $this->dropColumn('{{%perfume}}', 'aroma_group_id');
    }
}
