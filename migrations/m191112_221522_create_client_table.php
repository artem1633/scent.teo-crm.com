<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m191112_221522_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'firstname'=>$this->string()->comment('Имя'),
            'lastname'=>$this->string()->comment('Фамилия'),
            'phone'=>$this->string(16)->comment('Телефон'),
            'email'=>$this->string()->comment('E-mail'),
            'birthday'=>$this->date()->comment('День рождения'),
            'male'=>$this->smallInteger()->comment('Пол'),
            'address'=>$this->string()->comment('Город проживания'),
            'allergy'=>$this->smallInteger()->comment('Алергия'),
            'allergy_name'=>$this->smallInteger()->comment('Алергия на что')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client}}');
    }
}
