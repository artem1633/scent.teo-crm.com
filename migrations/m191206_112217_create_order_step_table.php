<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_step}}`.
 */
class m191206_112217_create_order_step_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_step}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'user_id' => $this->integer()->comment('Менеджер'),
            'step' => $this->integer()->comment('Этап'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-order_step-order_id',
            'order_step',
            'order_id'
        );

        $this->addForeignKey(
            'fk-order_step-order_id',
            'order_step',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-order_step-user_id',
            'order_step',
            'user_id'
        );

        $this->addForeignKey(
            'fk-order_step-user_id',
            'order_step',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order_step-user_id',
            'order_step'
        );

        $this->dropIndex(
            'idx-order_step-user_id',
            'order_step'
        );

        $this->dropForeignKey(
            'fk-order_step-order_id',
            'order_step'
        );

        $this->dropIndex(
            'idx-order_step-order_id',
            'order_step'
        );

        $this->dropTable('{{%order_step}}');
    }
}
