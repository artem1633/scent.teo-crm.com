<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%remain_operation}}`.
 */
class m200104_223615_add_order_id_column_to_remain_operation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('remain_operation', 'order_id', $this->integer()->comment('Заказ'));

        $this->createIndex(
            'idx-remain_operation-order_id',
            'remain_operation',
            'order_id'
        );

        $this->addForeignKey(
            'fk-remain_operation-order_id',
            'remain_operation',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-remain_operation-order_id',
            'remain_operation'
        );

        $this->dropIndex(
            'idx-remain_operation-order_id',
            'remain_operation'
        );

        $this->dropColumn('remain_operation', 'order_id');
    }
}
