<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%change_log}}`.
 */
class m200206_060757_create_change_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%change_log}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'step' => $this->integer()->comment('Раздел (номер)'),
            'field' => $this->string()->comment('Поле'),
            'action' => $this->integer()->comment('Действие'),
            'old_value' => $this->text()->comment('Старое значение'),
            'new_value' => $this->text()->comment('Новое значение'),
            'event_time' => $this->timestamp()->defaultExpression('NOW()')->comment('Время события')
        ]);

        $this->addForeignKey(
            'fk-change_log-order_id',
            '{{%change_log}}',
            'order_id',
            'order',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%change_log}}');
    }
}
