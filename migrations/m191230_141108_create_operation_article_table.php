<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%operation_article}}`.
 */
class m191230_141108_create_operation_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%operation_article}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'type' => $this->integer()->comment('Тип'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%operation_article}}');
    }
}
