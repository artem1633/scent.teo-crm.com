<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredient_to_group_ingredient}}`.
 */
class m200219_090439_create_ingredient_to_group_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ingredient_to_group_ingredient}}', [
            'id' => $this->primaryKey(),
            'ingredient_id' => $this->integer()->comment('Ингредиент'),
            'group_ingredient_id' => $this->integer()->comment('Группа ингредиента')
        ]);

        $this->addForeignKey(
            'fk-ingredient_to_group_ingredient-ingredient_id',
            '{{%ingredient_to_group_ingredient}}',
            'ingredient_id',
            '{{%ingredient}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-ingredient_to_group_ingredient-group_ingredient_id',
            '{{%ingredient_to_group_ingredient}}',
            'group_ingredient_id',
            '{{%group_ingredient}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ingredient_to_group_ingredient}}');
    }
}
