<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredient_substance_type}}`.
 */
class m191123_102812_create_ingredient_substance_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%substance_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%substance_type}}');
    }
}
