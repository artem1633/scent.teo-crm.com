<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ingredient}}`.
 */
class m191119_132237_add_substance_type_column_to_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ingredient', 'substance_type', $this->integer()->comment('Тип вещества'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn( 'ingredient', 'substance_type');
    }
}
