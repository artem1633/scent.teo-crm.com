<?php

use yii\db\Migration;

/**
 * Class m200213_081919_change_fields_type_in_formulas_table
 */
class m200213_081919_change_fields_type_in_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Задача от клиента сделать множественный выбор для полей формулы: возраст, сезон, стиль
        $this->dropForeignKey('fk-formulas-age_id', '{{%formulas}}');
        $this->dropForeignKey('fk-formulas-season_id', '{{%formulas}}');
        $this->dropForeignKey('fk-formulas-style_flavor_id', '{{%formulas}}');

        $this->alterColumn('{{%formulas}}', 'age_id', $this->string()->comment('Идентификаторы ворастов'));
        $this->renameColumn('{{%formulas}}', 'age_id', 'ages');
        $this->alterColumn('{{%formulas}}', 'season_id', $this->string()->comment('Идентификаторы сезонов'));
        $this->renameColumn('{{%formulas}}', 'season_id', 'seasons');
        $this->alterColumn('{{%formulas}}', 'style_flavor_id', $this->string()->comment('Идентификаторы стилей'));
        $this->renameColumn('{{%formulas}}', 'style_flavor_id', 'style_flavors');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200213_081919_change_fields_type_in_formulas_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200213_081919_change_fields_type_in_formulas_table cannot be reverted.\n";

        return false;
    }
    */
}
