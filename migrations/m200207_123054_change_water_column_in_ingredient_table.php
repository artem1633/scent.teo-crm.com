<?php

use yii\db\Migration;

/**
 * Class m200207_123054_change_water_column_in_ingridient_table
 */
class m200207_123054_change_water_column_in_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%ingredient}}', 'water', $this->float(2)->comment('Уровень ввода'));
        $this->renameColumn('{{%ingredient}}', 'water', 'input_level');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200207_123054_change_water_column_in_ingridient_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200207_123054_change_water_column_in_ingridient_table cannot be reverted.\n";

        return false;
    }
    */
}
