<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%group_ingredient}}`.
 */
class m200219_090346_create_group_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%group_ingredient}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%group_ingredient}}');
    }
}
