<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%perfume_accord}}`.
 */
class m200318_120015_create_perfume_accord_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%perfume_accord}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique()->comment('Наименование'),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%perfume_accord}}');
    }
}
