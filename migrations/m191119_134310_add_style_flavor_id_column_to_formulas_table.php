<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas}}`.
 */
class m191119_134310_add_style_flavor_id_column_to_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas', 'style_flavor_id', $this->integer()->comment('Стиль аромата'));

        $this->createIndex(
            'idx-formulas-style_flavor_id',
            'formulas',
            'style_flavor_id'
        );

        $this->addForeignKey(
            'fk-formulas-style_flavor_id',
            'formulas',
            'style_flavor_id',
            'style_flavor',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-formulas-style_flavor_id',
            'formulas'
        );

        $this->dropIndex(
            'idx-formulas-style_flavor_id',
            'formulas'
        );

        $this->dropColumn('formulas', 'style_flavor_id');
    }
}
