<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%important_flover}}`.
 */
class m191128_081036_add_group_column_to_important_flover_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('important_flover', 'group', $this->string()->comment('Группа'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('important_flover', 'group');
    }
}
