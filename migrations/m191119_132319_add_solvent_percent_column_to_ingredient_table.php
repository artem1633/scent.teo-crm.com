<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ingredient}}`.
 */
class m191119_132319_add_solvent_percent_column_to_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ingredient', 'solvent_percent', $this->float()->comment('% разбавления'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ingredient', 'solvent_percent');
    }
}
