<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredient_important_flover}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ingredient}}`
 * - `{{%important_flover}}`
 */
class m191115_063605_create_junction_table_for_ingredient_and_important_flover_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ingredient_important_flover}}', [
            'id' => $this->primaryKey(),
            'ingredient_id' => $this->integer(),
            'important_flover_id' => $this->integer(),
        ]);

        // creates index for column `ingredient_id`
        $this->createIndex(
            '{{%idx-ingredient_important_flover-ingredient_id}}',
            '{{%ingredient_important_flover}}',
            'ingredient_id'
        );

        // add foreign key for table `{{%ingredient}}`
        $this->addForeignKey(
            '{{%fk-ingredient_important_flover-ingredient_id}}',
            '{{%ingredient_important_flover}}',
            'ingredient_id',
            '{{%ingredient}}',
            'id',
            'CASCADE'
        );

        // creates index for column `important_flover_id`
        $this->createIndex(
            '{{%idx-ingredient_important_flover-important_flover_id}}',
            '{{%ingredient_important_flover}}',
            'important_flover_id'
        );

        // add foreign key for table `{{%important_flover}}`
        $this->addForeignKey(
            '{{%fk-ingredient_important_flover-important_flover_id}}',
            '{{%ingredient_important_flover}}',
            'important_flover_id',
            '{{%important_flover}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ingredient}}`
        $this->dropForeignKey(
            '{{%fk-ingredient_important_flover-ingredient_id}}',
            '{{%ingredient_important_flover}}'
        );

        // drops index for column `ingredient_id`
        $this->dropIndex(
            '{{%idx-ingredient_important_flover-ingredient_id}}',
            '{{%ingredient_important_flover}}'
        );

        // drops foreign key for table `{{%important_flover}}`
        $this->dropForeignKey(
            '{{%fk-ingredient_important_flover-important_flover_id}}',
            '{{%ingredient_important_flover}}'
        );

        // drops index for column `important_flover_id`
        $this->dropIndex(
            '{{%idx-ingredient_important_flover-important_flover_id}}',
            '{{%ingredient_important_flover}}'
        );

        $this->dropTable('{{%ingredient_important_flover}}');
    }
}
