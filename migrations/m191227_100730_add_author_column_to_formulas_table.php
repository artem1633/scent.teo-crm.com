<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas}}`.
 */
class m191227_100730_add_author_column_to_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas', 'author', $this->string()->comment('Автор'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('formulas', 'author');
    }
}
