<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m200218_070803_add_columns_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'gender', $this->smallInteger()->comment('Пол'));
        $this->addColumn('{{%user}}', 'birthday_date', $this->date()->comment('Дата рождения'));
        $this->addColumn('{{%user}}', 'email', $this->string()->comment('Адрес электронной почты'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'gender');
        $this->dropColumn('{{%user}}', 'birthday_date');
        $this->dropColumn('{{%user}}', 'email');
    }
}
