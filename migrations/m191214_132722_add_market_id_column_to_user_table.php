<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m191214_132722_add_market_id_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'market_id', $this->integer()->comment('Точка продажи'));

        $this->createIndex(
            'idx-user-market_id',
            'user',
            'market_id'
        );

        $this->addForeignKey(
            'fk-user-market_id',
            'user',
            'market_id',
            'market',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user-market_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-market_id',
            'user'
        );

        $this->dropColumn('user', 'market_id');
    }
}
