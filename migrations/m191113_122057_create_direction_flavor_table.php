<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%direction_flavor}}`.
 */
class m191113_122057_create_direction_flavor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%direction_flavor}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);
        $this->insert('{{%direction_flavor}}',['name'=>'Восточный']);
        $this->insert('{{%direction_flavor}}',['name'=>'Фужерный']);
        $this->insert('{{%direction_flavor}}',['name'=>'Шипровый']);
        $this->insert('{{%direction_flavor}}',['name'=>'Цветочный']);
        $this->insert('{{%direction_flavor}}',['name'=>'Цитрусовый']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%direction_flavor}}');
    }
}
