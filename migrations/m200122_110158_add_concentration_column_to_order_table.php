<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m200122_110158_add_concentration_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'price_id', $this->integer()->comment('Прайс'));

        $this->addForeignKey(
            'fk-order-price_id',
            'order',
            'price_id',
            'price',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'price_id');
    }
}
