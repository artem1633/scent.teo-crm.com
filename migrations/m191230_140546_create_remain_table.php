<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%remain}}`.
 */
class m191230_140546_create_remain_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%remain}}', [
            'id' => $this->primaryKey(),
            'market_id' => $this->integer()->comment('Точка продаж'),
            'ingredient_id' => $this->integer()->comment('Ингридиент'),
            'amount' => $this->float()->comment('Количество'),
        ]);

        $this->createIndex(
            'idx-remain-market_id',
            'remain',
            'market_id'
        );

        $this->addForeignKey(
            'fk-remain-market_id',
            'remain',
            'market_id',
            'market',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-remain-ingredient_id',
            'remain',
            'ingredient_id'
        );

        $this->addForeignKey(
            'fk-remain-ingredient_id',
            'remain',
            'ingredient_id',
            'ingredient',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-remain-ingredient_id',
            'remain'
        );

        $this->dropIndex(
            'idx-remain-ingredient_id',
            'remain'
        );

        $this->dropForeignKey(
            'fk-remain-market_id',
            'remain'
        );

        $this->dropIndex(
            'idx-remain-market_id',
            'remain'
        );

        $this->dropTable('{{%remain}}');
    }
}
