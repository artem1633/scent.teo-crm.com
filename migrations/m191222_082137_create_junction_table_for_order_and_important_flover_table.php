<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_important_flover}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%order}}`
 * - `{{%important_flover}}`
 */
class m191222_082137_create_junction_table_for_order_and_important_flover_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_important_flover}}', [
            'order_id' => $this->integer(),
            'important_flover_id' => $this->integer(),
            'type' => $this->integer()->comment('Тип')
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-order_important_flover-order_id}}',
            '{{%order_important_flover}}',
            'order_id'
        );

        // add foreign key for table `{{%order}}`
        $this->addForeignKey(
            '{{%fk-order_important_flover-order_id}}',
            '{{%order_important_flover}}',
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE'
        );

        // creates index for column `important_flover_id`
        $this->createIndex(
            '{{%idx-order_important_flover-important_flover_id}}',
            '{{%order_important_flover}}',
            'important_flover_id'
        );

        // add foreign key for table `{{%important_flover}}`
        $this->addForeignKey(
            '{{%fk-order_important_flover-important_flover_id}}',
            '{{%order_important_flover}}',
            'important_flover_id',
            '{{%important_flover}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%order}}`
        $this->dropForeignKey(
            '{{%fk-order_important_flover-order_id}}',
            '{{%order_important_flover}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-order_important_flover-order_id}}',
            '{{%order_important_flover}}'
        );

        // drops foreign key for table `{{%important_flover}}`
        $this->dropForeignKey(
            '{{%fk-order_important_flover-important_flover_id}}',
            '{{%order_important_flover}}'
        );

        // drops index for column `important_flover_id`
        $this->dropIndex(
            '{{%idx-order_important_flover-important_flover_id}}',
            '{{%order_important_flover}}'
        );

        $this->dropTable('{{%order_important_flover}}');
    }
}
