<?php

use yii\db\Migration;

/**
 * Class m191123_103006_alter_substance_type_column_from_ingredient_table
 */
class m191123_103006_alter_substance_type_column_from_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('ingredient', 'substance_type', 'substance_type_id');

        $this->createIndex(
            'idx-ingredient-substance_type_id',
            'ingredient',
            'substance_type_id'
        );

        $this->addForeignKey(
            'fk-ingredient-substance_type_id',
            'ingredient',
            'substance_type_id',
            'substance_type',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-ingredient-substance_type_id',
            'ingredient'
        );

        $this->dropIndex(
            'idx-ingredient-substance_type_id',
            'ingredient'
        );

        $this->renameColumn('ingredient', 'substance_type_id', 'substance_type');
    }
}
