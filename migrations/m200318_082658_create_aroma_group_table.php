<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%aroma_group}}`.
 */
class m200318_082658_create_aroma_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%aroma_group}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'parent_id' => $this->integer()->comment('Идентификатор родительской группы')
        ]);

        $this->addForeignKey(
            'fk-aroma_group-parent_id',
            '{{%aroma_group}}',
            'parent_id',
            '{{%aroma_group}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->batchInsert('{{%aroma_group}}', ['name', 'parent_id'], [
           ['фужерные', null],
           ['фужерные водяные', null],
           ['фужерные фруктовые', null],
           ['фужерные зеленые', null],
           ['фужерные пряные', null],
           ['шипровые', null],
           ['шипровые цветочные', null],
           ['шипровые фруктовые', null],
           ['цитрусовые', null],
           ['цитрусовые фужерные', null],
           ['цитрусовые сладкие', null],
           ['цветочные', null],
           ['цветочные альдегидные', null],
           ['цветочные водяные', null],
           ['цветочные фруктовые', null],
           ['цветочные фруктовые сладкие', null],
           ['цветочные зеленые', null],
           ['цветочные древесно-мускусные', null],
           ['кожаные', null],
           ['восточные', null],
           ['восточные цветочные', null],
           ['восточные фужерные', null],
           ['восточные пряные', null],
           ['восточные гурманские', null],
           ['восточные древесные', null],
           ['древесные', null],
           ['древесные водяные', null],
           ['древесные фужерные', null],
           ['древесные шипровые', null],
           ['древесные цветочные мускусные', null],
           ['древесные пряные', null],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%aroma_group}}');
    }
}
