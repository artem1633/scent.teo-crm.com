<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%perfume}}`.
 */
class m191224_213948_add_new_columns_to_perfume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perfume', 'external_id', $this->integer()->comment('Внешний ID'));
        $this->addColumn('perfume', 'sex', $this->integer()->comment('Пол'));
        $this->addColumn('perfume', 'thumbnail', $this->string()->comment('Изображение'));
        $this->addColumn('perfume', 'rating', $this->double()->comment('Рейтинг'));
        $this->addColumn('perfume', 'year', $this->string()->comment('Рейтинг'));
        $this->addColumn('perfume', 'url', $this->string()->comment('URL'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('perfume', 'external_id');
        $this->dropColumn('perfume', 'sex');
        $this->dropColumn('perfume', 'thumbnail');
        $this->dropColumn('perfume', 'rating');
        $this->dropColumn('perfume', 'year');
        $this->dropColumn('perfume', 'url');
    }
}
