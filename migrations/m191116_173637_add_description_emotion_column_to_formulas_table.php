<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas}}`.
 */
class m191116_173637_add_description_emotion_column_to_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas', 'description_emotion', $this->text()->comment('Эмоциональное описание'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('formulas', 'description_emotion');
    }
}
