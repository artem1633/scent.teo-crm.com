<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%finance}}`.
 */
class m200105_102805_create_finance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%finance}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'amount' => $this->float()->comment('Сумма'),
            'type' => $this->integer()->comment('Тип'),
            'payment_type' => $this->integer()->comment('Способ оплаты'),
            'client_id' => $this->integer()->comment('Клиент'),
            'manager_id' => $this->integer()->comment('Менеджер'),
            'market_id' => $this->integer()->comment('Точка продаж'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-finance-order_id',
            'finance',
            'order_id'
        );

        $this->addForeignKey(
            'fk-finance-order_id',
            'finance',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-finance-client_id',
            'finance',
            'client_id'
        );

        $this->addForeignKey(
            'fk-finance-client_id',
            'finance',
            'client_id',
            'client',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-finance-manager_id',
            'finance',
            'manager_id'
        );

        $this->addForeignKey(
            'fk-finance-manager_id',
            'finance',
            'manager_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-finance-market_id',
            'finance',
            'market_id'
        );

        $this->addForeignKey(
            'fk-finance-market_id',
            'finance',
            'market_id',
            'market',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-finance-market_id',
            'finance'
        );

        $this->dropIndex(
            'idx-finance-market_id',
            'finance'
        );

        $this->dropForeignKey(
            'fk-finance-manager_id',
            'finance'
        );

        $this->dropIndex(
            'idx-finance-manager_id',
            'finance'
        );

        $this->dropForeignKey(
            'fk-finance-client_id',
            'finance'
        );

        $this->dropIndex(
            'idx-finance-client_id',
            'finance'
        );

        $this->dropForeignKey(
            'fk-finance-order_id',
            'finance'
        );

        $this->dropIndex(
            'idx-finance-order_id',
            'finance'
        );

        $this->dropTable('{{%finance}}');
    }
}
