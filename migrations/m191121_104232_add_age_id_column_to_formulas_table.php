<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas}}`.
 */
class m191121_104232_add_age_id_column_to_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas', 'age_id', $this->integer()->comment('Возраст'));

        $this->createIndex(
            'idx-formulas-age_id',
            'formulas',
            'age_id'
        );

        $this->addForeignKey(
            'fk-formulas-age_id',
            'formulas',
            'age_id',
            'age',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-formulas-age_id',
            'formulas'
        );

        $this->dropIndex(
            'idx-formulas-age_id',
            'formulas'
        );

        $this->dropColumn('formulas', 'age_id');
    }
}
