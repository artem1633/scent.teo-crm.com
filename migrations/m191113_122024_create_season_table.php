<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%season}}`.
 */
class m191113_122024_create_season_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%season}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);
        $this->insert('{{%season}}',['name'=>'Лето']);
        $this->insert('{{%season}}',['name'=>'Осень']);
        $this->insert('{{%season}}',['name'=>'Зима']);
        $this->insert('{{%season}}',['name'=>'Весна']);
        $this->insert('{{%season}}',['name'=>'Не знаю']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%season}}');
    }
}
