<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%formulas}}`.
 */
class m191119_143001_drop_age_column_from_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('formulas', 'age');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('formulas', 'age', $this->string()->after('sex')->comment('Возраст'));
    }
}
