<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%remain_operation}}`.
 */
class m200101_153758_add_inventory_id_column_to_remain_operation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('remain_operation', 'inventory_id', $this->integer()->comment('Инвентаризация'));

        $this->createIndex(
            'idx-remain_operation-inventory_id',
            'remain_operation',
            'inventory_id'
        );

        $this->addForeignKey(
            'fk-remain_operation-inventory_id',
            'remain_operation',
            'inventory_id',
            'inventory',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-remain_operation-inventory_id',
            'remain_operation'
        );

        $this->dropIndex(
            'idx-remain_operation-inventory_id',
            'remain_operation'
        );

        $this->dropColumn('remain_operation', 'inventory_id');
    }
}
