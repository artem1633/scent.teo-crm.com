<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredient_level_triangle_flavor}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ingredient}}`
 * - `{{%level_triangle_flavor}}`
 */
class m191117_091753_create_junction_table_for_ingredient_and_level_triangle_flavor_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ingredient_level_triangle_flavor}}', [
            'id' => $this->primaryKey(),
            'ingredient_id' => $this->integer(),
            'level_triangle_flavor_id' => $this->integer(),
        ]);

        // creates index for column `ingredient_id`
        $this->createIndex(
            '{{%idx-ingredient_level_triangle_flavor-ingredient_id}}',
            '{{%ingredient_level_triangle_flavor}}',
            'ingredient_id'
        );

        // add foreign key for table `{{%ingredient}}`
        $this->addForeignKey(
            '{{%fk-ingredient_level_triangle_flavor-ingredient_id}}',
            '{{%ingredient_level_triangle_flavor}}',
            'ingredient_id',
            '{{%ingredient}}',
            'id',
            'CASCADE'
        );

        // creates index for column `level_triangle_flavor_id`
        $this->createIndex(
            '{{%idx-ingredient_level_triangle_flavor-level_triangle_flavor_id}}',
            '{{%ingredient_level_triangle_flavor}}',
            'level_triangle_flavor_id'
        );

        // add foreign key for table `{{%level_triangle_flavor}}`
        $this->addForeignKey(
            '{{%fk-ingredient_level_triangle_flavor-level_triangle_flavor_id}}',
            '{{%ingredient_level_triangle_flavor}}',
            'level_triangle_flavor_id',
            '{{%level_triangle_flavor}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ingredient}}`
        $this->dropForeignKey(
            '{{%fk-ingredient_level_triangle_flavor-ingredient_id}}',
            '{{%ingredient_level_triangle_flavor}}'
        );

        // drops index for column `ingredient_id`
        $this->dropIndex(
            '{{%idx-ingredient_level_triangle_flavor-ingredient_id}}',
            '{{%ingredient_level_triangle_flavor}}'
        );

        // drops foreign key for table `{{%level_triangle_flavor}}`
        $this->dropForeignKey(
            '{{%fk-ingredient_level_triangle_flavor-level_triangle_flavor_id}}',
            '{{%ingredient_level_triangle_flavor}}'
        );

        // drops index for column `level_triangle_flavor_id`
        $this->dropIndex(
            '{{%idx-ingredient_level_triangle_flavor-level_triangle_flavor_id}}',
            '{{%ingredient_level_triangle_flavor}}'
        );

        $this->dropTable('{{%ingredient_level_triangle_flavor}}');
    }
}
