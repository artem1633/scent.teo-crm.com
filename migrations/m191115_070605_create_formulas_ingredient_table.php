<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%formulas_ingredient}}`.
 */
class m191115_070605_create_formulas_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%formulas_ingredient}}', [
            'id' => $this->primaryKey(),
            'formula_id' => $this->integer()->comment('Формула'),
            'ingredient_id' => $this->integer()->comment('Ингредиент'),
            'dilute' => $this->float()->comment('Процент разбавления'),
            'count' => $this->float()->comment('Кол-во'),
            'is_main' => $this->boolean()->defaultValue(true)->comment('Основной ингредиент'),
            'comment' => $this->text()->comment('Комментарий'),
            'can_delete' => $this->boolean()->defaultValue(false)->comment('Можно удалить из формулы')
        ]);

        $this->createIndex(
            'idx-formulas_ingredient-formula_id',
            'formulas_ingredient',
            'formula_id'
        );

        $this->addForeignKey(
            'fk-formulas_ingredient-formula_id',
            'formulas_ingredient',
            'formula_id',
            'formulas',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-formulas_ingredient-ingredient_id',
            'formulas_ingredient',
            'ingredient_id'
        );

        $this->addForeignKey(
            'fk-formulas_ingredient-ingredient_id',
            'formulas_ingredient',
            'ingredient_id',
            'ingredient',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-formulas_ingredient-ingredient_id',
            'formulas_ingredient'
        );

        $this->dropIndex(
            'idx-formulas_ingredient-ingredient_id',
            'formulas_ingredient'
        );

        $this->dropForeignKey(
            'fk-formulas_ingredient-formula_id',
            'formulas_ingredient'
        );

        $this->dropIndex(
            'idx-formulas_ingredient-formula_id',
            'formulas_ingredient'
        );

        $this->dropTable('{{%formulas_ingredient}}');
    }
}
