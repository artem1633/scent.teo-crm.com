<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredient}}`.
 */
class m191114_201521_create_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ingredient}}', [
            'id' => $this->primaryKey(),
            'article' => $this->string()->comment('Артикул'),
            'name_rus' => $this->string()->comment('Наименование'),
            'name_eng' => $this->string()->comment('Наименование (анг.)'),
            'developer_id' => $this->integer()->comment('Производитель'),
            'description' => $this->text()->comment('Описание'),
            'testable' => $this->boolean()->defaultValue(false)->comment('Доступен к дегустации'),
            'level_flavor_id' => $this->integer()->comment('Уровень'),
            'intensive_flavor_id' => $this->integer()->comment('Интенсивность'),
            'substantivity' => $this->float()->comment('Субстантивность'),
            'const_price' => $this->float()->comment('Себестоимость'),
            'cost_price_value' => $this->float()->comment('объем (за себестоимость)'),
            'place_num' => $this->string()->comment('Место на полке'),
            'solvent' => $this->integer()->comment('В чем растворено'),
            'cas' => $this->string()->comment('Номер CAS'),
            'water' => $this->float()->comment('Уровень ввода'),
            'description_additional' => $this->text()->comment('Дополнительное описание'),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer(),
        ]);


        $this->createIndex(
            'idx-ingredient-developer_id',
            'ingredient',
            'developer_id'
        );

        $this->addForeignKey(
            'fk-ingredient-developer_id',
            'ingredient',
            'developer_id',
            'developer',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-ingredient-level_flavor_id',
            'ingredient',
            'level_flavor_id'
        );

        $this->addForeignKey(
            'fk-ingredient-level_flavor_id',
            'ingredient',
            'level_flavor_id',
            'level_flavor',
            'id',
            'SET NULL'
        );


        $this->createIndex(
            'idx-ingredient-intensive_flavor_id',
            'ingredient',
            'intensive_flavor_id'
        );

        $this->addForeignKey(
            'fk-ingredient-intensive_flavor_id',
            'ingredient',
            'intensive_flavor_id',
            'intensive_flavor',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-ingredient-created_by',
            'ingredient',
            'created_by'
        );

        $this->addForeignKey(
            'fk-ingredient-created_by',
            'ingredient',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-ingredient-created_by',
            'ingredient'
        );

        $this->dropIndex(
            'idx-ingredient-created_by',
            'ingredient'
        );

        $this->dropForeignKey(
            'fk-ingredient-intensive_flavor_id',
            'ingredient'
        );

        $this->dropIndex(
            'idx-ingredient-intensive_flavor_id',
            'ingredient'
        );

        $this->dropForeignKey(
            'fk-ingredient-level_flavor_id',
            'ingredient'
        );

        $this->dropIndex(
            'idx-ingredient-level_flavor_id',
            'ingredient'
        );

        $this->dropForeignKey(
            'fk-ingredient-developer_id',
            'ingredient'
        );

        $this->dropIndex(
            'idx-ingredient-developer_id',
            'ingredient'
        );

        $this->dropTable('{{%ingredient}}');
    }
}
