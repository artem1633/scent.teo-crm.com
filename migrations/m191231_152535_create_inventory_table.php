<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%inventory}}`.
 */
class m191231_152535_create_inventory_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%inventory}}', [
            'id' => $this->primaryKey(),
            'market_id' => $this->integer()->comment('Точка продаж'),
            'name' => $this->string()->comment('Наименование'),
            'comment' => $this->text()->comment('Коменатрий'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
            'manager_id' => $this->integer()->comment('Менеджер'),
        ]);

        $this->createIndex(
            'idx-inventory-market_id',
            'inventory',
            'market_id'
        );

        $this->addForeignKey(
            'fk-inventory-market_id',
            'inventory',
            'market_id',
            'market',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-inventory-manager_id',
            'inventory',
            'manager_id'
        );

        $this->addForeignKey(
            'fk-inventory-manager_id',
            'inventory',
            'manager_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-inventory-manager_id',
            'inventory'
        );

        $this->dropIndex(
            'idx-inventory-manager_id',
            'inventory'
        );

        $this->dropForeignKey(
            'fk-inventory-market_id',
            'inventory'
        );

        $this->dropIndex(
            'idx-inventory-market_id',
            'inventory'
        );

        $this->dropTable('{{%inventory}}');
    }
}
