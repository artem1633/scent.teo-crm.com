<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%level_flavor}}`.
 */
class m191114_195655_create_level_flavor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%level_flavor}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%level_flavor}}');
    }
}
