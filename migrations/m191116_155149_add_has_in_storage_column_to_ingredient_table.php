<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ingredient}}`.
 */
class m191116_155149_add_has_in_storage_column_to_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ingredient', 'has_in_storage', $this->boolean()->defaultValue(false)->comment('Есть на складе'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ingredient', 'has_in_storage');
    }
}
