<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m191130_091854_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'step' => $this->integer()->comment('Этап'),
            'type' => $this->integer()->comment('Тип заказа'),
            'solvent' => $this->integer()->comment('Растворитель'),
            'value' => $this->float()->comment('Объем'),
            'price' => $this->float()->comment('Цена заказа'),
            'client_id' => $this->integer()->comment('Клиент'),

            'current_flavors_count' => $this->integer()->comment('Текущие кол-во ароматов'),
            'style_flavor_id' => $this->integer()->comment('Стиль аромата'),
            'intensive_flavor_id' => $this->integer()->comment('Интенсивность аромата'),
            'season_id' => $this->integer()->comment('Сезон'),
            'direction_id' => $this->integer()->comment('Направление'),

            'formula_id' => $this->integer()->comment('Формула'),

            'client_mark' => $this->integer()->comment('Оценка клиента'),
            'mark_client_comment' => $this->text()->comment('Комментарий клиента'),

            'created_at' => $this->dateTime(),
            'ended_at' => $this->dateTime()->comment('Дата и время закрытия заказа'),
        ]);

        $this->createIndex(
            'idx-order-client_id',
            'order',
            'client_id'
        );

        $this->addForeignKey(
            'fk-order-client_id',
            'order',
            'client_id',
            'client',
            'id',
            'SET NULL'
        );


        $this->createIndex(
            'idx-order-style_flavor_id',
            'order',
            'style_flavor_id'
        );

        $this->addForeignKey(
            'fk-order-style_flavor_id',
            'order',
            'style_flavor_id',
            'style_flavor',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order-intensive_flavor_id',
            'order',
            'intensive_flavor_id'
        );

        $this->addForeignKey(
            'fk-order-intensive_flavor_id',
            'order',
            'intensive_flavor_id',
            'intensive_flavor',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-order-season_id',
            'order',
            'season_id'
        );

        $this->addForeignKey(
            'fk-order-season_id',
            'order',
            'season_id',
            'season',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order-direction_id',
            'order',
            'direction_id'
        );

        $this->addForeignKey(
            'fk-order-direction_id',
            'order',
            'direction_id',
            'direction_flavor',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order-formula_id',
            'order',
            'formula_id'
        );

        $this->addForeignKey(
            'fk-order-formula_id',
            'order',
            'formula_id',
            'formulas',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-direction_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-direction_id',
            'order'
        );

        $this->dropForeignKey(
            'fk-order-season_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-season_id',
            'order'
        );

        $this->dropForeignKey(
            'fk-order-intensive_flavor_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-intensive_flavor_id',
            'order'
        );

        $this->dropForeignKey(
            'fk-order-style_flavor_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-style_flavor_id',
            'order'
        );


        $this->dropForeignKey(
            'fk-order-client_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-client_id',
            'order'
        );

        $this->dropForeignKey(
            'fk-order-formula_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-formula_id',
            'order'
        );

        $this->dropTable('{{%order}}');
    }
}
