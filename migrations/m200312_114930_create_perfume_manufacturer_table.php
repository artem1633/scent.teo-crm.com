<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%perfume_manufacturer}}`.
 */
class m200312_114930_create_perfume_manufacturer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%perfume_manufacturer}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%perfume_manufacturer}}');
    }
}
