<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas_important_flover}}`.
 */
class m191222_140450_add_level_column_to_formulas_important_flover_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas_important_flover', 'level', $this->float()->defaultValue(0)->comment('Уровень'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('formulas_important_flover', 'level');
    }
}
