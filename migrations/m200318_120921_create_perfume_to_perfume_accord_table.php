<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%perfume_to_perfume_accord}}`.
 */
class m200318_120921_create_perfume_to_perfume_accord_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%perfume_to_perfume_accord}}', [
            'id' => $this->primaryKey(),
            'perfume_id' => $this->integer()->comment('Парфюм'),
            'perfume_accord_id' => $this->integer()->comment('Аккорд'),
            'rate' => $this->integer()->comment('Позиция (относительно остальных аккордов)')
        ]);

        $this->addForeignKey(
            'fk-perfume_to_perfume_accord-perfume_id',
            '{{%perfume_to_perfume_accord}}',
            'perfume_id',
            '{{%perfume}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-perfume_to_perfume_accord-perfume_accord_id',
            '{{%perfume_to_perfume_accord}}',
            'perfume_accord_id',
            '{{%perfume_accord}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-perfume_to_perfume_accord-perfume_id', '{{%perfume_to_perfume_accord}}');
        $this->dropForeignKey('fk-perfume_to_perfume_accord-perfume_accord_id', '{{%perfume_to_perfume_accord}}');
        $this->dropTable('{{%perfume_to_perfume_accord}}');
    }
}
