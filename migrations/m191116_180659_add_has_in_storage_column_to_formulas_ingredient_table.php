<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas_ingredient}}`.
 */
class m191116_180659_add_has_in_storage_column_to_formulas_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas_ingredient', 'has_in_storage', $this->boolean()->defaultValue(false)->comment('Есть на складе'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('formulas_ingredient', 'has_in_storage');
    }
}
