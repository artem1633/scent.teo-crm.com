<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%perfume}}`.
 */
class m200312_114954_add_manufacturer_id_column_to_perfume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%perfume}}', 'manufacturer_id', $this->integer()->comment('Производитель'));
        $this->addForeignKey(
            'fk-perfume-manufacturer_id',
            '{{%perfume}}',
            'manufacturer_id',
            'perfume_manufacturer',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-perfume-manufacturer_id', '{{%perfume}}');
        $this->dropTable('{{%perfume}}');
    }
}
