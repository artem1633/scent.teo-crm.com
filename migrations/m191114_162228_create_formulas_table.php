<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%formulas}}`.
 */
class m191114_162228_create_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%formulas}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'sex' => $this->integer()->comment('Пол'),
            'age' => $this->string()->comment('Возраст'),
            'season_id' => $this->integer()->comment('Сезон'),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-formulas-season_id',
            'formulas',
            'season_id'
        );

        $this->addForeignKey(
            'fk-formulas-season_id',
            'formulas',
            'season_id',
            'season',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-formulas-created_by',
            'formulas',
            'created_by'
        );

        $this->addForeignKey(
            'fk-formulas-created_by',
            'formulas',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-formulas-created_by',
            'formulas'
        );

        $this->dropIndex(
            'idx-formulas-created_by',
            'formulas'
        );

        $this->dropForeignKey(
            'fk-formulas-season_id',
            'formulas'
        );

        $this->dropIndex(
            'idx-formulas-season_id',
            'formulas'
        );

        $this->dropTable('{{%formulas}}');
    }
}
