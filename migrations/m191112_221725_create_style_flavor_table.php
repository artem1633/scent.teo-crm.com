<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%style_flavor}}`.
 */
class m191112_221725_create_style_flavor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%style_flavor}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);
        $this->insert('{{%style_flavor}}',['name'=>'Деловой-класика']);
        $this->insert('{{%style_flavor}}',['name'=>'Спорт']);
        $this->insert('{{%style_flavor}}',['name'=>'Вечерний']);
        $this->insert('{{%style_flavor}}',['name'=>'Кежуал']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%style_flavor}}');
    }
}
