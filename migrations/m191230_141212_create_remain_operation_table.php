<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%remain_operation}}`.
 */
class m191230_141212_create_remain_operation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%remain_operation}}', [
            'id' => $this->primaryKey(),
            'remain_id' => $this->integer()->comment('Остаток'),
            'method' => $this->integer()->comment('Метод'),
            'type' => $this->integer()->comment('Тип'),
            'article_id' => $this->integer()->comment('Статья'),
            'quantity' => $this->float()->comment('Кол-во'),
            'amount_last' => $this->float()->comment('Количество до опереции на складе'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
            'manager_id' => $this->integer()->comment('Менеджер'),
        ]);

        $this->createIndex(
            'idx-remain_operation-remain_id',
            'remain_operation',
            'remain_id'
        );

        $this->addForeignKey(
            'fk-remain_operation-remain_id',
            'remain_operation',
            'remain_id',
            'remain',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-remain_operation-article_id',
            'remain_operation',
            'article_id'
        );

        $this->addForeignKey(
            'fk-remain_operation-article_id',
            'remain_operation',
            'article_id',
            'operation_article',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-remain_operation-manager_id',
            'remain_operation',
            'manager_id'
        );

        $this->addForeignKey(
            'fk-remain_operation-manager_id',
            'remain_operation',
            'manager_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-remain_operation-manager_id',
            'remain_operation'
        );

        $this->dropIndex(
            'idx-remain_operation-manager_id',
            'remain_operation'
        );

        $this->dropForeignKey(
            'fk-remain_operation-article_id',
            'remain_operation'
        );

        $this->dropIndex(
            'idx-remain_operation-article_id',
            'remain_operation'
        );

        $this->dropForeignKey(
            'fk-remain_operation-remain_id',
            'remain_operation'
        );

        $this->dropIndex(
            'idx-remain_operation-remain_id',
            'remain_operation'
        );

        $this->dropTable('{{%remain_operation}}');
    }
}
