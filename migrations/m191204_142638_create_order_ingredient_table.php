<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_ingredient}}`.
 */
class m191204_142638_create_order_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_ingredient}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'ingredient_id' => $this->integer()->comment('Ингредиент'),
            'dilute' => $this->float()->comment('Процент разбавления'),
            'count' => $this->float()->comment('Кол-во'),
            'is_main' => $this->boolean()->defaultValue(true)->comment('Основной ингредиент'),
            'comment' => $this->text()->comment('Комментарий'),
            'can_delete' => $this->boolean()->defaultValue(false)->comment('Можно удалить из формулы')
        ]);

        $this->createIndex(
            'idx-order_ingredient-order_id',
            'order_ingredient',
            'order_id'
        );

        $this->addForeignKey(
            'fk-order_ingredient-order_id',
            'order_ingredient',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order_ingredient-ingredient_id',
            'order_ingredient',
            'ingredient_id'
        );

        $this->addForeignKey(
            'fk-order_ingredient-ingredient_id',
            'order_ingredient',
            'ingredient_id',
            'ingredient',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order_ingredient-ingredient_id',
            'order_ingredient'
        );

        $this->dropIndex(
            'idx-order_ingredient-ingredient_id',
            'order_ingredient'
        );

        $this->dropForeignKey(
            'fk-order_ingredient-order_id',
            'order_ingredient'
        );

        $this->dropIndex(
            'idx-order_ingredient-order_id',
            'order_ingredient'
        );

        $this->dropTable('{{%order_ingredient}}');
    }
}
