<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas_important_flover}}`.
 */
class m191224_070433_add_formula_ingredient_id_column_to_formulas_important_flover_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas_important_flover', 'formula_ingredient_id', $this->integer()->comment('Игредиент формулы'));

        $this->createIndex(
            'idx-formulas_important_flover-formula_ingredient_id',
            'formulas_important_flover',
            'formula_ingredient_id'
        );

        $this->addForeignKey(
            'fk-formulas_important_flover-formula_ingredient_id',
            'formulas_important_flover',
            'formula_ingredient_id',
            'formulas_ingredient',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-formulas_important_flover-formula_ingredient_id',
            'formulas_important_flover'
        );

        $this->dropIndex(
            'idx-formulas_important_flover-formula_ingredient_id',
            'formulas_important_flover'
        );

        $this->dropColumn('formulas_important_flover', 'formula_ingredient_id');
    }
}
