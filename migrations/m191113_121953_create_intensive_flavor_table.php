<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%intensive_flavor}}`.
 */
class m191113_121953_create_intensive_flavor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%intensive_flavor}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);
        $this->insert('{{%intensive_flavor}}',['name'=>'Сильный']);
        $this->insert('{{%intensive_flavor}}',['name'=>'Умеренный']);
        $this->insert('{{%intensive_flavor}}',['name'=>'Еле уловимый']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%intensive_flavor}}');
    }
}
