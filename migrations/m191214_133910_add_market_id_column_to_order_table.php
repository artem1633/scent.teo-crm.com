<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m191214_133910_add_market_id_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'market_id', $this->integer()->comment('Точка продажи'));

        $this->createIndex(
            'idx-order-market_id',
            'order',
            'market_id'
        );

        $this->addForeignKey(
            'fk-order-market_id',
            'order',
            'market_id',
            'market',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-market_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-market_id',
            'order'
        );

        $this->dropColumn('order', 'market_id');
    }
}
