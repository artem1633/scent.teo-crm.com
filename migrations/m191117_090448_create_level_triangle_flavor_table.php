<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%level_triangle_flavor}}`.
 */
class m191117_090448_create_level_triangle_flavor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%level_triangle_flavor}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%level_triangle_flavor}}');
    }
}
