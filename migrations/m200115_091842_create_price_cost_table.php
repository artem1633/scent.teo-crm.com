<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%price_cost}}`.
 */
class m200115_091842_create_price_cost_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%price_cost}}', [
            'id' => $this->primaryKey(),
            'price_id' => $this->integer()->comment('Прайс'),
            'type' => $this->integer()->comment('Тип (новое создание/повторное создание)'),
            'volume' => $this->integer()->comment('Объем, (мл)'),
            'cost' => $this->double(2)->comment('Цена'),
            'currency' => $this->integer()->comment('Валюта')
        ]);

        $this->addForeignKey(
            'fk-price_cost-price_id',
            'price_cost',
            'price_id',
            'price',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%price_cost}}');
    }
}
