<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_perfume}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%order}}`
 * - `{{%perfume}}`
 */
class m191227_130923_create_junction_table_for_order_and_perfume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_perfume}}', [
            'order_id' => $this->integer(),
            'perfume_id' => $this->integer(),
            'PRIMARY KEY(order_id, perfume_id)',
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-order_perfume-order_id}}',
            '{{%order_perfume}}',
            'order_id'
        );

        // add foreign key for table `{{%order}}`
        $this->addForeignKey(
            '{{%fk-order_perfume-order_id}}',
            '{{%order_perfume}}',
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE'
        );

        // creates index for column `perfume_id`
        $this->createIndex(
            '{{%idx-order_perfume-perfume_id}}',
            '{{%order_perfume}}',
            'perfume_id'
        );

        // add foreign key for table `{{%perfume}}`
        $this->addForeignKey(
            '{{%fk-order_perfume-perfume_id}}',
            '{{%order_perfume}}',
            'perfume_id',
            '{{%perfume}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%order}}`
        $this->dropForeignKey(
            '{{%fk-order_perfume-order_id}}',
            '{{%order_perfume}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-order_perfume-order_id}}',
            '{{%order_perfume}}'
        );

        // drops foreign key for table `{{%perfume}}`
        $this->dropForeignKey(
            '{{%fk-order_perfume-perfume_id}}',
            '{{%order_perfume}}'
        );

        // drops index for column `perfume_id`
        $this->dropIndex(
            '{{%idx-order_perfume-perfume_id}}',
            '{{%order_perfume}}'
        );

        $this->dropTable('{{%order_perfume}}');
    }
}
