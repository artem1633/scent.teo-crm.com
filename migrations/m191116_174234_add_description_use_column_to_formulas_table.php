<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%formulas}}`.
 */
class m191116_174234_add_description_use_column_to_formulas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('formulas', 'description_use', $this->text()->comment('Применение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('formulas', 'description_use');
    }
}
