<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%perfume_important_flover}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%perfume}}`
 * - `{{%important_flover}}`
 */
class m191224_214619_create_junction_table_for_perfume_and_important_flover_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%perfume_important_flover}}', [
            'id' => $this->primaryKey(),
            'perfume_id' => $this->integer(),
            'important_flover_id' => $this->integer(),
            'rating' => $this->integer()->comment('Рейтиг'),
        ]);

        // creates index for column `perfume_id`
        $this->createIndex(
            '{{%idx-perfume_important_flover-perfume_id}}',
            '{{%perfume_important_flover}}',
            'perfume_id'
        );

        // add foreign key for table `{{%perfume}}`
        $this->addForeignKey(
            '{{%fk-perfume_important_flover-perfume_id}}',
            '{{%perfume_important_flover}}',
            'perfume_id',
            '{{%perfume}}',
            'id',
            'CASCADE'
        );

        // creates index for column `important_flover_id`
        $this->createIndex(
            '{{%idx-perfume_important_flover-important_flover_id}}',
            '{{%perfume_important_flover}}',
            'important_flover_id'
        );

        // add foreign key for table `{{%important_flover}}`
        $this->addForeignKey(
            '{{%fk-perfume_important_flover-important_flover_id}}',
            '{{%perfume_important_flover}}',
            'important_flover_id',
            '{{%important_flover}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%perfume}}`
        $this->dropForeignKey(
            '{{%fk-perfume_important_flover-perfume_id}}',
            '{{%perfume_important_flover}}'
        );

        // drops index for column `perfume_id`
        $this->dropIndex(
            '{{%idx-perfume_important_flover-perfume_id}}',
            '{{%perfume_important_flover}}'
        );

        // drops foreign key for table `{{%important_flover}}`
        $this->dropForeignKey(
            '{{%fk-perfume_important_flover-important_flover_id}}',
            '{{%perfume_important_flover}}'
        );

        // drops index for column `important_flover_id`
        $this->dropIndex(
            '{{%idx-perfume_important_flover-important_flover_id}}',
            '{{%perfume_important_flover}}'
        );

        $this->dropTable('{{%perfume_important_flover}}');
    }
}
