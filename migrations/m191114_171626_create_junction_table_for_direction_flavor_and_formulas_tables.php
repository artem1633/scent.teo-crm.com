<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%direction_flavor_formulas}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%direction_flavor}}`
 * - `{{%formulas}}`
 */
class m191114_171626_create_junction_table_for_direction_flavor_and_formulas_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%direction_flavor_formulas}}', [
            'id' => $this->primaryKey(),
            'direction_flavor_id' => $this->integer(),
            'formulas_id' => $this->integer(),
        ]);

        // creates index for column `direction_flavor_id`
        $this->createIndex(
            '{{%idx-direction_flavor_formulas-direction_flavor_id}}',
            '{{%direction_flavor_formulas}}',
            'direction_flavor_id'
        );

        // add foreign key for table `{{%direction_flavor}}`
        $this->addForeignKey(
            '{{%fk-direction_flavor_formulas-direction_flavor_id}}',
            '{{%direction_flavor_formulas}}',
            'direction_flavor_id',
            '{{%direction_flavor}}',
            'id',
            'CASCADE'
        );

        // creates index for column `formulas_id`
        $this->createIndex(
            '{{%idx-direction_flavor_formulas-formulas_id}}',
            '{{%direction_flavor_formulas}}',
            'formulas_id'
        );

        // add foreign key for table `{{%formulas}}`
        $this->addForeignKey(
            '{{%fk-direction_flavor_formulas-formulas_id}}',
            '{{%direction_flavor_formulas}}',
            'formulas_id',
            '{{%formulas}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%direction_flavor}}`
        $this->dropForeignKey(
            '{{%fk-direction_flavor_formulas-direction_flavor_id}}',
            '{{%direction_flavor_formulas}}'
        );

        // drops index for column `direction_flavor_id`
        $this->dropIndex(
            '{{%idx-direction_flavor_formulas-direction_flavor_id}}',
            '{{%direction_flavor_formulas}}'
        );

        // drops foreign key for table `{{%formulas}}`
        $this->dropForeignKey(
            '{{%fk-direction_flavor_formulas-formulas_id}}',
            '{{%direction_flavor_formulas}}'
        );

        // drops index for column `formulas_id`
        $this->dropIndex(
            '{{%idx-direction_flavor_formulas-formulas_id}}',
            '{{%direction_flavor_formulas}}'
        );

        $this->dropTable('{{%direction_flavor_formulas}}');
    }
}
