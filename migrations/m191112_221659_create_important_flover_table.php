<?php

use yii\db\Migration;

/**
 * Class m191113_121659_create_important
 */
class m191112_221659_create_important_flover_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%important_flover}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);
        $this->insert('{{%important_flover}}',['name'=>'Бренд']);
        $this->insert('{{%important_flover}}',['name'=>'Шлейф']);
        $this->insert('{{%important_flover}}',['name'=>'Стойкость']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191113_121659_create_important cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191113_121659_create_important cannot be reverted.\n";

        return false;
    }
    */
}
