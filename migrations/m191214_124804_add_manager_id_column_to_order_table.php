<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m191214_124804_add_manager_id_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'manager_id', $this->integer()->comment('Менеджер'));

        $this->createIndex(
            'idx-order-manager_id',
            'order',
            'manager_id'
        );

        $this->addForeignKey(
            'fk-order-manager_id',
            'order',
            'manager_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-manager_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-manager_id',
            'order'
        );

        $this->dropColumn('order', 'manager_id');
    }
}
