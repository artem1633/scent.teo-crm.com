<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_formulas_ingredient}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%order}}`
 * - `{{%formulas_ingredient}}`
 */
class m191224_110015_create_junction_table_for_order_and_formulas_ingredient_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_formulas_ingredient}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'formulas_ingredient_id' => $this->integer(),
            'mark' => $this->integer()->comment('Оценка'),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-order_formulas_ingredient-order_id}}',
            '{{%order_formulas_ingredient}}',
            'order_id'
        );

        // add foreign key for table `{{%order}}`
        $this->addForeignKey(
            '{{%fk-order_formulas_ingredient-order_id}}',
            '{{%order_formulas_ingredient}}',
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE'
        );

        // creates index for column `formulas_ingredient_id`
        $this->createIndex(
            '{{%idx-order_formulas_ingredient-formulas_ingredient_id}}',
            '{{%order_formulas_ingredient}}',
            'formulas_ingredient_id'
        );

        // add foreign key for table `{{%formulas_ingredient}}`
        $this->addForeignKey(
            '{{%fk-order_formulas_ingredient-formulas_ingredient_id}}',
            '{{%order_formulas_ingredient}}',
            'formulas_ingredient_id',
            '{{%formulas_ingredient}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%order}}`
        $this->dropForeignKey(
            '{{%fk-order_formulas_ingredient-order_id}}',
            '{{%order_formulas_ingredient}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-order_formulas_ingredient-order_id}}',
            '{{%order_formulas_ingredient}}'
        );

        // drops foreign key for table `{{%formulas_ingredient}}`
        $this->dropForeignKey(
            '{{%fk-order_formulas_ingredient-formulas_ingredient_id}}',
            '{{%order_formulas_ingredient}}'
        );

        // drops index for column `formulas_ingredient_id`
        $this->dropIndex(
            '{{%idx-order_formulas_ingredient-formulas_ingredient_id}}',
            '{{%order_formulas_ingredient}}'
        );

        $this->dropTable('{{%order_formulas_ingredient}}');
    }
}
