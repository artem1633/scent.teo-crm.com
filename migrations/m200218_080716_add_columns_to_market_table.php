<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%market}}`.
 */
class m200218_080716_add_columns_to_market_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%market}}', 'open_date', $this->date()->comment('Дата открытия'));
        $this->addColumn('{{%market}}', 'city', $this->string()->comment('Город'));
        $this->addColumn('{{%market}}', 'supervisor', $this->string()->comment('Супервайзер'));
        $this->addColumn('{{%market}}', 'address', $this->string()->comment('Адрес'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%market}}', 'open_date');
        $this->dropColumn('{{%market}}', 'city');
        $this->dropColumn('{{%market}}', 'supervisor');
        $this->dropColumn('{{%market}}', 'address');
    }
}
