<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%formulas_important_flover}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%formulas}}`
 * - `{{%important_flover}}`
 */
class m191114_164707_create_junction_table_for_formulas_and_important_flover_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%formulas_important_flover}}', [
            'id' => $this->primaryKey(),
            'formulas_id' => $this->integer(),
            'important_flover_id' => $this->integer(),
        ]);

        // creates index for column `formulas_id`
        $this->createIndex(
            '{{%idx-formulas_important_flover-formulas_id}}',
            '{{%formulas_important_flover}}',
            'formulas_id'
        );

        // add foreign key for table `{{%formulas}}`
        $this->addForeignKey(
            '{{%fk-formulas_important_flover-formulas_id}}',
            '{{%formulas_important_flover}}',
            'formulas_id',
            '{{%formulas}}',
            'id',
            'CASCADE'
        );

        // creates index for column `important_flover_id`
        $this->createIndex(
            '{{%idx-formulas_important_flover-important_flover_id}}',
            '{{%formulas_important_flover}}',
            'important_flover_id'
        );

        // add foreign key for table `{{%important_flover}}`
        $this->addForeignKey(
            '{{%fk-formulas_important_flover-important_flover_id}}',
            '{{%formulas_important_flover}}',
            'important_flover_id',
            '{{%important_flover}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%formulas}}`
        $this->dropForeignKey(
            '{{%fk-formulas_important_flover-formulas_id}}',
            '{{%formulas_important_flover}}'
        );

        // drops index for column `formulas_id`
        $this->dropIndex(
            '{{%idx-formulas_important_flover-formulas_id}}',
            '{{%formulas_important_flover}}'
        );

        // drops foreign key for table `{{%important_flover}}`
        $this->dropForeignKey(
            '{{%fk-formulas_important_flover-important_flover_id}}',
            '{{%formulas_important_flover}}'
        );

        // drops index for column `important_flover_id`
        $this->dropIndex(
            '{{%idx-formulas_important_flover-important_flover_id}}',
            '{{%formulas_important_flover}}'
        );

        $this->dropTable('{{%formulas_important_flover}}');
    }
}
