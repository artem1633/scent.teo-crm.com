<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%perfume}}`.
 */
class m191112_221611_create_perfume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%perfume}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%perfume}}');
    }
}
