<?php

namespace app\helpers;


use app\models\AromaGroup;
use app\models\ImportantFlover;
use app\models\Perfume;
use app\models\PerfumeAccord;
use app\models\PerfumeImportantFlover;
use app\models\PerfumeManufacturer;
use app\models\PerfumeToPerfumeAccord;
use app\models\User;
use Yii;
use yii\helpers\Url;

/**
 * @property array $json_files Массив с именами файлов для импорта
 * @property string $path_dir Путь к папке с json файлами
 * @property array $errors Ошибки
 * @property int $count_files Кол-во файлов
 * @property int $count_items Кол-во парфюмов
 * @property int $skip_items Пропущено файлов
 * @property int $start Метка времени начала скрипта
 *
 * @property Perfume $perfume_in_base Модель парфюма из базы
 */
class ImportPerfume
{
    private $path_dir;
    private $json_files;
    private $errors = [];
    private $count_files = 0;
    private $count_items = 0;
    private $skip_items = 0;
    private $perfume_in_base;
    private $start;


    public function __construct()
    {
        $this->path_dir = Url::to('@app/import_data');
    }

    /**
     * Импорт данных из JSON файла
     * @param string $file_name Имя файла
     * @return array|bool
     * @throws \yii\db\Exception
     */
    public function import($file_name = '')
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        if (!$identity->isSuperAdmin()) {
            Yii::warning('Пользователь не является админом. Отмена импорта', 'test');
            return ['success' => 0, 'errors' => ['Пользователь не является админом. Отмена импорта']];
        }

        set_time_limit(60 * 180);
//        ini_set('max_execution_time', 60 * 15);
        ini_set('memory_limit', '2000M');
        $this->start = time();

        if (!is_dir($this->path_dir)) {
            Yii::error('Путь к директории (' . $this->path_dir . ') не корректен. Отмена ипорта', 'error');
            return [
                'success' => 0,
                'errors' => ['Путь к директории (' . $this->path_dir . ') не корректен. Отмена ипорта']
            ];
        }

        //Получаем все файлы для импорта
        $this->json_files = array_diff(scandir($this->path_dir), ['.', '..']);

        //Удаляем из массива не файлы и не json-файлы
        $this->cleanListFiles();

        if ($file_name){
            $this->json_files = [$file_name];
        }

        echo "<b>Файлов для импорта: " . count($this->json_files) . '</b><br>';

        foreach ($this->json_files as $file) {
            $this->count_files++;
            $file_content = file_get_contents($this->path_dir . '/' . $file);
            echo '<b>Обработка файла: ' . $file . '</b><br>';
            $this->fromJSON($file_content);
            echo '<b>Обработка файла: ' . $file . ' завершена</b><br>';
            unset($file_content);
        }

        return [
            'success' => 1,
            'errors' => $this->errors,
            'count_files' => $this->count_files,
            'count_items' => $this->count_items,
            'skip_items' => $this->skip_items,
        ];

    }

    /**
     * Удаляет из массива файлов не файлы и не JSON файлы
     * @return bool
     */
    private function cleanListFiles()
    {
        $error_files = [];
        foreach ($this->json_files as $file) {
            if (pathinfo($file, PATHINFO_EXTENSION) != 'json' || !is_file($this->path_dir . '/' . $file)) {
                array_push($error_files, $file);
            }
        }

        array_diff($this->json_files, $error_files);

        Yii::info($this->json_files, 'test');

        return true;
    }

    /**
     * Импорт в базу из набора данных
     * @param string $data JSON данные
     * @return bool
     * @throws \yii\db\Exception
     */
    private function fromJSON($data)
    {
        $data = json_decode($data, true);
//        Yii::info($data, 'test');
        $perfume_counter = 1;
        /** @var array $perfume json данные */
        foreach ($data as &$perfume) {
            echo $perfume_counter . '. ';
            $this->perfume_in_base = Perfume::find()
                    ->andWhere(['external_id' => $perfume['id']])
                    ->one() ?? null;
            if ($this->perfume_in_base) {
                echo $this->perfume_in_base->name . ' уже есть в базе, проверяем связи<br>';

                $accords = $this->addAccords($perfume['accords']);
                $this->addAccordRelation($accords);
                $this->addImportantFlavors2($perfume);
                $this->skip_items++;
                unset($perfume);
                $perfume_counter ++;
                continue;
            }

            $this->perfume_in_base = new Perfume([
                'name' => $perfume['naslov'],
                'external_id' => $perfume['id'],
                'sex' => $this->getGender($perfume['name']),
                'thumbnail' => $perfume['thumbnail'],
                'rating' => (double)$perfume['rating'],
                'year' => $perfume['godina'],
                'manufacturer_id' => $this->addManufacturer($perfume['dizajner']),
                'aroma_group_id' => $this->getAromaGroupId($perfume['group']),
            ]);

            if (!$this->perfume_in_base->save()) {
                Yii::error($this->perfume_in_base->errors, '_error');
                $this->errors[$this->perfume_in_base->name] = $this->perfume_in_base->errors;
            } else {
                echo $this->perfume_in_base->name . ' добавлен в базу. Создаем связи<br>';
                $accords = $this->addAccords($perfume['accords']);
                $this->addAccordRelation($accords);
                $this->addImportantFlavors2($perfume);
                $this->count_items++;
            }

            unset($perfume);
            $perfume_counter += 1;
        }

        unset($data);
        echo '<br>Прошло ' . round((time() - $this->start) / 60, 1) . ' мин.<br>';
//        die;
        return true;
    }

    /**
     * @param string $name Имя производителя
     * @return int|null
     */
    private function addManufacturer($name)
    {
        //Проверяем на наличие в базе
        /** @var PerfumeManufacturer $p_manufacturer */
        $p_manufacturer = PerfumeManufacturer::find()
                ->andWhere(['name' => trim($name)])->one() ?? null;


        if ($p_manufacturer) {
            return $p_manufacturer->id;
        }

        //Добавляем производителя
        $manufacturer = new PerfumeManufacturer([
            'name' => trim($name),
        ]);

        if (!$manufacturer->save()) {
            Yii::error($manufacturer->errors, '_error');
            $this->errors[$manufacturer->name] = $manufacturer->errors;
        } else {
            return $manufacturer->id;
        }

        return null;
    }

    /**
     * Импорт ароматов
     * @param $perfume
     * @return bool
     * @throws \yii\db\Exception
     */
    private function addImportantFlavors2($perfume)
    {
        $added_flavors = $this->addFlavors($perfume);

        $this->addRelations($perfume, $added_flavors);

        unset($added_flavors);
        return true;

    }

    /**
     * Добавление ароматов
     * @param $perfume
     * @return array Массив имен добавленых ароматов
     * @throws \yii\db\Exception
     */
    private function addFlavors($perfume)
    {
        $flavors_name_list = [];

        /** @var array $flavor_rows Строки для добавления ароматов */
        $flavor_rows = [];

        /** @var array $rel_rate_flavor массив рейтинг => Нименование аромата */
        $rel_rate_flavor = [];


        //Получаем массив наименований ароматов
        foreach ($perfume['flovers'] as &$flavor) {
            array_push($flavors_name_list, $flavor['flover']);
            $rel_rate_flavor[$flavor['rate']] = $flavor['flover'];
            unset($flavor);
        }

        //Находим в базе все ароматы из списка
        $flavor_names_in_base = ImportantFlover::find()->andWhere([
            'IN',
            'name',
            $flavors_name_list
        ])->select('name')->column();

        //Получаем не найденные ароматы
        $flavor_add_names = array_diff($flavors_name_list, $flavor_names_in_base);

        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Не найдено ароматов: ' . count($flavor_add_names) . ' шт.<br>';

        foreach ($flavor_add_names as $name) {
            //Добавляем в массив полей для запроса на добавление
            $flavor_rows[] = [
                'name' => $name,
                'group' => null,
            ];
            unset($name);
        }

        //Добавляем ароматы в базу
        Yii::$app->db->createCommand()->batchInsert(ImportantFlover::tableName(), ['name', 'group'],
            $flavor_rows)->execute();

        //Получаем массив рейтинг => наименование аромата
        $result = array_diff($rel_rate_flavor, $flavor_add_names);

        unset($flavors_name_list, $rel_rate_flavor, $flavor_add_names, $flavor_names_in_base, $flavor_rows);

        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Проверка ароматов завершена<br>';

        return $result;
    }

    /**
     * Добавляет связь парфюма с ароматом (perfume_important_flover)
     * @param array $perfume Инфа о парфюме из json файла
     * @param array $added_flavors Добавленные ароматы ([рейтинг => Наименование аромата])
     * @return bool
     * @throws \yii\db\Exception
     */
    private function addRelations($perfume, $added_flavors)
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Обработка связей парфюм-аромат...<br>';

        if (count($added_flavors) == 0) {
            return false;
        }

        /** @var array $relations_row Строки для добавления связи аромата с парфюмом */
        $relations_row = [];

        /** @var Perfume $perfume_model */
        $perfume_model = Perfume::find()->andWhere(['external_id' => $perfume['id']])->one() ?? null;

        if (!$perfume_model) {
            return false;
        }
        //Получаем вновь добавленные ароматы

        $flavor_models = ImportantFlover::find()->andWhere([
            'IN',
            'name',
            array_values($added_flavors)
        ]);

        $perfume_id = $perfume_model->id;

        unset($perfume_model);

        /** @var ImportantFlover $flavor_model */
        $counter = 0;
        foreach ($flavor_models->each() as $flavor_model) {
            if (PerfumeImportantFlover::find()->andWhere([
                'perfume_id' => $perfume_id,
                'important_flover_id' => $flavor_model->id
            ])->exists()) {
                unset($flavor_model);
                continue;
            }
            $relations_row[] = [
                'perfume_id' => $perfume_id,
                'important_flover_id' => $flavor_model->id,
                'rating' => array_search($flavor_model->name, $added_flavors),
            ];
            unset($flavor_model);
            $counter++;
        }

        Yii::$app->db->createCommand()->batchInsert(PerfumeImportantFlover::tableName(),
            ['perfume_id', 'important_flover_id', 'rating'],
            $relations_row)->execute();

        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Добавлено связей: ' . count($relations_row) . ' шт.<br>';

        unset($relations_row);

        return true;
    }

    /**
     * Парсит строку и возвразает значение константы пола
     * @param string $string Строка наименования парфюма с указанием пола
     * @return int
     */
    private function getGender($string)
    {
        if (stripos($string, 'для мужчин')) {
            return Perfume::SEX_MALE;
        } elseif (stripos($string, 'для женщин')) {
            return Perfume::SEX_FEMALE;
        }

        return Perfume::SEX_UNISEX;
    }

    public function getAromaGroupId($group_name)
    {
        if (!$group_name) {
            return null;
        }

        $group_name = trim($group_name);

        /** @var AromaGroup $aroma_group */
        $aroma_group = AromaGroup::find()->andWhere(['name' => strtolower($group_name)])->one() ?? null;

        if (!$aroma_group) {
            return null;
        }

        return $aroma_group->id;
    }

    /**
     * Добавляет аккорды для парфюма
     * @param string $str_accords Строка с аккордами, разделенными через запятую
     * @return array Массив имен аккордов (из json файла)
     * @throws \yii\db\Exception
     */
    public function addAccords($str_accords)
    {
        Yii::info($str_accords, 'test');
        $str_accords = str_replace(' ', '', $str_accords);
        $accords = explode(',', $str_accords);
        Yii::info($accords, 'test');

        //Получаем аккорды из базы
        $accords_base = PerfumeAccord::find()->andWhere(['IN', 'name', $accords])->select('name')->column();
        Yii::info($accords_base, 'test');

        //Получаем имена аккордов, отсутствующих в базе
        $missing_accords = array_diff($accords, $accords_base);
        Yii::info($missing_accords, 'test');

        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;аккордов, не найденых в базе: ' . count($missing_accords) . ' шт. <br>';

        $rows = [];
        foreach ($missing_accords as &$missing_accord) {
            $rows[] = [$missing_accord];
            unset($missing_accord);
        }
        Yii::info($rows, 'test');

        unset($str_accords, $accords_base, $missing_accords);

        //Добавляем аккорды
        Yii::$app->db->createCommand()->batchInsert(PerfumeAccord::tableName(), ['name'], $rows)->execute();

        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;проверка аккордов завершена';

        unset($rows, $missing_accords);
        return $accords;
    }

    /**
     * Добавляет данные в perfume_to_perfume_accord
     * @param array $accords Наименования аккородов
     * @return bool
     * @throws \yii\db\Exception
     */
    public function addAccordRelation($accords)
    {
        if (count($accords)) {
            //Получаем идентификаторы аккордов
            $accord_ids = PerfumeAccord::find()->andWhere(['IN', 'name', $accords])->select('id')->column();
            $rate = 1;
            $rows = [];

            //Получаем связи аккордов с парфюмом уже внесенные в базу
            $relation_accords = PerfumeToPerfumeAccord::find()
                ->andWhere(['perfume_id' => $this->perfume_in_base])
                ->select('perfume_accord_id')->column();

            //Получаем id аккордов, для которых нет связи с парфюмом
            $missing_relations_accord = array_diff($accord_ids, $relation_accords);

            echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;не найдено связей парфюм-аккорд: ' . count($missing_relations_accord) . ' шт.<br>';

            //Готовим данные для запроса
            foreach ($missing_relations_accord as &$accord_id) {
                $rows[] = [$this->perfume_in_base->id, $accord_id, $rate];
                $rate++;
                unset($accord_id);
            }

            unset($accord_ids, $relation_accords, $missing_relations_accord);

            if (count($rows) > 0) {
                Yii::$app->db->createCommand()->batchInsert(PerfumeToPerfumeAccord::tableName(),
                    ['perfume_id', 'perfume_accord_id', 'rate'], $rows)->execute();
                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cвязи парфюм-аккорд добавлены.<br>';

                unset($rows, $accords);
                return true;
            }
        }
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cвязи парфюм-аккорд проверены.<br>';

        return false;
    }
}