<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PerfumeAccord */
?>
<div class="perfume-accord-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
