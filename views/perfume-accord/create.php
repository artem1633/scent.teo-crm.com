<?php

/* @var $this yii\web\View */
/* @var $model app\models\PerfumeAccord */

?>
<div class="perfume-accord-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
