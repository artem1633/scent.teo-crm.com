<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FormulasImportantFlover */
?>
<div class="formulas-important-flover-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'formulas_id',
            'important_flover_id',
            'level',
        ],
    ]) ?>

</div>
