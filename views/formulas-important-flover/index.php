<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\FormulasImportantFloverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Ароматы";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse formulas-important-flover-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Ароматы</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    //            'filterModel' => $searchModel,
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                            [
                                'role' => 'modal-remote',
                                'title' => 'Добавить аромат',
                                'class' => 'btn btn-success'
                            ]) . '&nbsp;' .
                        Html::a('<i class="fa fa-repeat"></i>', [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => ''
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
