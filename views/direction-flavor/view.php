<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DirectionFlavor */
?>
<div class="direction-flavor-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
