<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirectionFlavor */
?>
<div class="direction-flavor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
