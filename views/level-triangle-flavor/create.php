<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LevelTriangleFlavor */

?>
<div class="level-triangle-flavor-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
