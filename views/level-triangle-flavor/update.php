<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LevelTriangleFlavor */
?>
<div class="level-triangle-flavor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
