<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LevelTriangleFlavor */
?>
<div class="level-triangle-flavor-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
