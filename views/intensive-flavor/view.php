<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\IntensiveFlavor */
?>
<div class="intensive-flavor-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
