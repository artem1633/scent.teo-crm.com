<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IntensiveFlavor */
?>
<div class="intensive-flavor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
