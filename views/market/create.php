<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Market */

?>
<div class="market-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
