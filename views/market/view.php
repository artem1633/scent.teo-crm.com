<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Market */
?>
<div class="market-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'open_date',
                'name',
                'city',
                'supervisor',
                'address',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
