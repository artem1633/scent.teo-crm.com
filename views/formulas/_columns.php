<?php

use app\models\Age;
use app\models\Formulas;
use app\models\Season;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'sex',
        'filter' => Formulas::sexLabels(),
        'value' => function ($model) {
            return ArrayHelper::getValue(Formulas::sexLabels(), $model->sex);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'seasons',
        'filter' => (new Season())->getList(),
        'content' => function (Formulas $model){
            return (new Season)->getSeasons($model->seasons);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ages',
        'filter' => (new Age())->getList(),
        'content' => function (Formulas $model){
            return (new Age)->getAges($model->seasons);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'directionFlovers',
        'value' => function (Formulas $model){
//            'directionFlavorFormulas.directionFlavor.name'
            $direct_flavors = [];
            /** @var \app\models\DirectionFlavorFormulas $df_formula */
            foreach ($model->directionFlavorFormulas as $df_formula) {
                $direct_flavor = $df_formula->directionFlavor->name ?? null;

               if ($direct_flavor){
                   array_push($direct_flavors, $direct_flavor);
               }
            }
            return implode(', ' , $direct_flavors);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'importantFlovers',
        'value' => function (Formulas $model){
            /** @var \app\models\ImportantFlover $importantFlover */
            $flowers = '';
            foreach ($model->formulasImportantFlovers as $formulaImportantFlower) {
                $flowers .= $formulaImportantFlower->importantFlover->name . ',' . PHP_EOL;
            }
           return $flowers;
        }
    ],


    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'description_emotion',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'author',
        'label' => 'Автор',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'description_use',
    ],

//     [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'created_at',
//         'format' => ['date', 'php:d M Y H:i:s'],
//     ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'label'=>'Добавить ингредиент',
//        'content' => function($model){
//            return Html::a('<i class="fa fa-plus"></i>', ['formulas-ingredient/create', 'formula_id' => $model->id], ['class' => 'btn btn-sm btn-success', 'role' => 'modal-remote']);
//        },
//        'width' => '10px',
//        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{view} {update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Удалить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Изменить',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                    ]) . "&nbsp;";
            }
        ],
    ],

];   