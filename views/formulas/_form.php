<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\DirectionFlavor;
use app\models\ImportantFlover;
use app\models\Season;
use app\models\Formulas;
use app\models\Age;

/* @var $this yii\web\View */
/* @var $model app\models\Formulas */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord == false) {
    $model->directionFlovers = ArrayHelper::getColumn(\app\models\DirectionFlavorFormulas::find()->where(['formulas_id' => $model->id])->all(),
        'direction_flavor_id');
    $model->importantFlovers = ArrayHelper::getColumn(\app\models\FormulasImportantFlover::find()->where(['formulas_id' => $model->id])->all(),
        'important_flover_id');
}

?>

<div class="formulas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex')->dropDownList(Formulas::sexLabels(), ['prompt' => 'Выберите пол']) ?>

    <?= $form->field($model, 'ages')->widget(Select2::class, [
        'data' => (new Age)->getList(),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
            'multiple' => true,
            'placeholder' => 'Выберите одно или несколько значений...',
        ],
        'pluginOptions' => [
            'placeholder' => 'Выберите возраст'
        ],
    ]) ?>

    <?= $form->field($model, 'seasons')->widget(Select2::class, [
        'data' => (new Season)->getList(),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
            'multiple' => true,
            'placeholder' => 'Выберите одно или несколько значений...',
        ],
        'pluginOptions' => [
            'placeholder' => 'Выберите сезон'
        ],
    ]) ?>

    <?= $form->field($model, 'directionFlovers')->widget(Select2::class, [
        'data' => (new DirectionFlavor)->getList(),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
            'multiple' => true,
            'placeholder' => 'Выберите одно или несколько значений...',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?php
//    echo $form->field($model, 'importantFlovers')->widget(Select2::class, [
//        'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
//        'theme' => Select2::THEME_BOOTSTRAP,
//        'options' => [
//            'multiple' => true,
//            'placeholder' => 'Выберите одно или несколько значений...',
//
//        ],
//        'pluginOptions' => [
//            'allowClear' => true
//        ],
//    ])
    ?>

    <?= $form->field($model, 'description_emotion')->textarea() ?>

    <?= $form->field($model, 'description_use')->textarea() ?>

    <?= $form->field($model, 'style_flavors')->widget(Select2::class, [
        'data' => ArrayHelper::map(\app\models\StyleFlavor::find()->all(), 'id', 'name'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
            'multiple' => true,
            'placeholder' => 'Выберите одно или несколько значений...',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
