<?php

use app\models\Age;
use app\models\DirectionFlavor;
use app\models\DirectionFlavorFormulas;
use app\models\FormulasImportantFlover;
use app\models\ImportantFlover;
use app\models\Season;
use app\models\StyleFlavor;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Formulas;

/* @var $this yii\web\View */
/* @var $model app\models\Formulas */
/* @var $importantFloverSearch \app\models\ImportantFloverSearch */
/* @var $importantFloverDataProvider \yii\data\ActiveDataProvider */
/* @var $ingredientsSearchModel \app\models\IngredientSearch */
/* @var $ingredientsDataProvider \yii\data\ActiveDataProvider*/

$this->title = "Формула «{$model->name}»";

?>
<div class="formulas-view">

    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Основная информация</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" style="margin-top: -40px;"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <?php
                    try {
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'name',
                                'author',
                                [
                                    'attribute' => 'sex',
                                    'value' => function ($model) {
                                        return ArrayHelper::getValue(Formulas::sexLabels(), $model->sex);
                                    }
                                ],
                                [
                                    'attribute' => 'ages',
                                    'value' => function (Formulas $model) {
                                        if ($model->ages != null) {
                                            return (new Age)->getAges($model->ages);
                                        }
                                        return null;
                                    }
                                ],
                                [
                                    'attribute' => 'season_id',
                                    'value' => function (Formulas $model) {
                                        if ($model->seasons != null) {
                                            return (new Season)->getSeasons($model->seasons);
                                        }
                                        return null;
                                    },
                                ],
                                [
                                    'attribute' => 'style_flavors',
                                    'value' => function (Formulas $model) {
                                        if ($model->style_flavors != null) {
                                            return (new StyleFlavor)->getStyles($model->style_flavors);
                                        }
                                    },
                                ],
                                'description_emotion',
                                'description_use',
                                [
                                    'attribute' => 'directionFlovers',
                                    'value' => function (Formulas $model) {
                                        $pks = ArrayHelper::getColumn(DirectionFlavorFormulas::find()->where(['formulas_id' => $model->id])->all(),
                                            'id');
                                        return implode(', ',
                                            ArrayHelper::getColumn(DirectionFlavor::findAll($pks), 'name'));
                                    }
                                ],
                                [
                                    'attribute' => 'importantFlovers',
                                    'value' => function (Formulas $model) {
                                        $pks = ArrayHelper::getColumn(FormulasImportantFlover::find()->where(['formulas_id' => $model->id])->all(),
                                            'important_flover_id');

                                        Yii::info($pks, 'test');

                                        return implode(', ',
                                            ArrayHelper::getColumn(ImportantFlover::findAll($pks), 'name'));
                                    }
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'format' => ['date', 'php:d M Y H:i:s'],
                                ],
                                [
                                    'attribute' => 'created_by',
                                    'value' => function ($model) {
                                        if ($model->createdBy != null) {
                                            return $model->createdBy->login;
                                        }
                                    },
                                ],
                            ],
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                        echo $e->getMessage();
                    } ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?= $this->render('@app/views/formulas-important-flover/index', [
                'searchModel' => $importantFloverSearch,
                'dataProvider' => $importantFloverDataProvider,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Ингредиенты</h4>
                </div>
                <div class="panel-body">
                    <?= $this->render('@app/views/formulas-ingredient/index', [
                        'searchModel' => $ingredientsSearchModel,
                        'dataProvider' => $ingredientsDataProvider,
                        'model' => $model,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
