<?php


use app\admintheme\widgets\Menu;

$is_admin = Yii::$app->user->identity->isSuperAdmin();
?>

<div id="sidebar" class="sidebar" style="overflow: auto;">
    <?php if (Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo Menu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        ['label' => 'Заказы', 'icon' => 'fa  fa-rub', 'url' => ['/order'], 'visible' => $is_admin],
                        ['label' => 'Клиенты', 'icon' => 'fa  fa-users', 'url' => ['/client']],
                        ['label' => 'Формулы', 'icon' => 'fa fa-tasks', 'url' => ['/formulas'], 'visible' => $is_admin],
                        [
                            'label' => 'Ингредиенты',
                            'icon' => 'fa fa-th',
                            'url' => ['/ingredient'],
                            'visible' => $is_admin
                        ],
                        ['label' => 'Финансы', 'icon' => 'fa fa-rub', 'url' => ['/finance'], 'visible' => $is_admin],
                        ['label' => 'Склад', 'icon' => 'fa fa-inbox', 'url' => ['/remain']],
                        [
                            'label' => 'Настройки',
                            'icon' => 'fa fa-gear',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'visible' => $is_admin,
                            'items' => [
                                [
                                    'label' => 'Справочники',
                                    'icon' => 'fa fa-tasks',
                                    'url' => '#',
                                    'options' => ['class' => 'has-sub'],
                                    'visible' => $is_admin,
                                    'items' => [
                                        ['label' => 'Прайс', 'url' => ['/price'], 'visible' => $is_admin],
                                        [
                                            'label' => 'Статьи прихода/списания',
                                            'url' => ['/operation-article'],
                                            'visible' => $is_admin
                                        ],
                                        ['label' => 'Возраста', 'url' => ['/age'], 'visible' => $is_admin],
                                        [
                                            'label' => 'Парфюмы',
                                            'url' => ['/perfume'],
                                            'visible' => $is_admin
                                        ],
                                        [
                                            'label' => 'Ароматы и запахи',
                                            'url' => ['/important-flover'],
                                            'visible' => $is_admin
                                        ],
                                        [
                                            'label' => 'Группы ароматов',
                                            'url' => ['/aroma-group'],
                                            'visible' => $is_admin
                                        ],
                                        [
                                            'label' => 'Стиль аромата',
                                            'url' => ['/style-flavor'],
                                            'visible' => $is_admin
                                        ],
                                        [
                                            'label' => 'Типы вещества',
                                            'url' => ['/substance-type'],
                                            'visible' => $is_admin
                                        ],

                                        [
                                            'label' => 'Интенсивность аромата',
                                            'url' => ['/intensive-flavor'],
                                            'visible' => $is_admin
                                        ],
                                        [
                                            'label' => 'Уровни аромата',
                                            'url' => ['/level-flavor'],
                                            'visible' => $is_admin
                                        ],
//                        ['label' => 'Уровень в пирамиде', 'url' => ['/level-triangle-flavor'],'visible'=>$is_admin],
                                        [
                                            'label' => 'Сезон использования',
                                            'url' => ['/season'],
                                            'visible' => $is_admin
                                        ],
                                        ['label' => 'Производители', 'url' => ['/developer'], 'visible' => $is_admin],
                                        [
                                            'label' => 'Направление',
                                            'url' => ['/direction-flavor'],
                                            'visible' => $is_admin
                                        ]
                                    ]
                                ],
                                [
                                    'label' => 'Пользователи',
                                    'icon' => 'fa  fa-user-o',
                                    'url' => ['/user'],
                                    'visible' => $is_admin
                                ],
                                [
                                    'label' => 'Точки продаж',
                                    'icon' => 'fa fa-balance-scale',
                                    'url' => ['/market'],
                                    'visible' => $is_admin
                                ],

                            ]
                        ],


                        [
                            'label' => 'Журнал изменений',
                            'icon' => 'fa fa-inbox',
                            'url' => ['/remain-operation'],
                            'visible' => false
                        ],
                        [
                            'label' => 'Инвентаризация',
                            'icon' => 'fa fa-inbox',
                            'url' => ['/inventory'],
                            'visible' => $is_admin
                        ],
                    ],
                ]
            );
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
