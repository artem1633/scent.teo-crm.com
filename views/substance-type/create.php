<?php

/* @var $this yii\web\View */
/* @var $model app\models\SubstanceType */

?>
<div class="substance-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
