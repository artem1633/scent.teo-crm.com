<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubstanceTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тип вещества';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse substance-type-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Типы вещества</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php

            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',

                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'toolbar' => [
                        [
                            'content' =>
                                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                                    [
                                        'role' => 'modal-remote',
                                        'title' => 'Добавить тип вещества',
                                        'class' => 'btn btn-default'
                                    ]) .
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']) .
                                '{toggleData}' .
                                '{export}'
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                            [
                                'role' => 'modal-remote',
                                'title' => 'Добавить тип',
                                'class' => 'btn btn-success'
                            ]) . '&nbsp;' .
                        Html::a('<i class="fa fa-repeat"></i>', [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after' => BulkButtonWidget::widget([
                                'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                    ["bulk-delete"],
                                    [
                                        "class" => "btn btn-danger btn-xs",
                                        'role' => 'modal-remote-bulk',
                                        'data-confirm' => false,
                                        'data-method' => false,// for overide yii data api
                                        'data-request-method' => 'post',
                                        'data-confirm-title' => 'Are you sure?',
                                        'data-confirm-message' => 'Are you sure want to delete this item'
                                    ]),
                            ]) .
                            '<div class="clearfix"></div>',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
