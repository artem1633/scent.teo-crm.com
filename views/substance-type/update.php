<?php

/* @var $this yii\web\View */
/* @var $model app\models\SubstanceType */
?>
<div class="substance-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
