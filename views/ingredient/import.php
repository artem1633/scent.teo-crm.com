<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Developer;
use app\models\LevelTriangleFlavor;
use app\models\IntensiveFlavor;
use app\models\Ingredient;
use app\models\IngredientImportantFlover;
use app\models\IngredientLevelTriangleFlavor;
use app\models\ImportantFlover;

/* @var $this yii\web\View */
/* @var $model app\models\forms\IngredientImportForm */


?>

<div class="ingredient-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput() ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
