<?php

use app\models\Ingredient;
use app\models\LevelFlavor;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\IngredientImportantFlover;
use app\models\ImportantFlover;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name_rus',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Ароматы',
        'attribute' => 'flavors',
        'content' => function ($model) {
            $flovers = ArrayHelper::getColumn(IngredientImportantFlover::find()->where(['ingredient_id' => $model->id])->all(),
                'important_flover_id');
            $flovers = ArrayHelper::getColumn(ImportantFlover::findAll($flovers), 'name');

            return implode(', ', $flovers);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'description',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'dev_name',
        'value' => 'developer.name',
        'label' => 'Производитель',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'testable',
        'content' => function ($model) {
            if ($model->testable == 1) {
                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
            }

            return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
        },
        'filter' => [
            1 => 'Да',
            0 => 'Нет',
        ],
        'width' => '10px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'levelTriangleFlavors',
        'filter' => (new LevelFlavor())->getList(),
        'content' => function(Ingredient $model){
            return $model->levelTriangleLabels();
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'const_price',
        'width' => '10px',
    ],
[
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'place_num',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'rest',
        'content' => function(Ingredient $model){
            if (Yii::$app->user->identity->isSuperAdmin()){
                $info_remains = [];
                foreach ($model->remains as $remain) {
                    $market_name = $remain->market->name ?? null;
                    array_push($info_remains, $remain->amount . ' (' . $market_name . ')');
                }

                return implode(',' . PHP_EOL, $info_remains);
            }
            return $model->remain->amount ?? null;
        }
    ],


//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'has_in_storage',
//        'filter' => [
//            1 => 'Да',
//            0 => 'Нет',
//        ],
//        'content' => function ($model) {
//            if ($model->has_in_storage == 1) {
//                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
//            }
//
//            return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
//        },
//        'width' => '10px',
//        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{view}&nbsp;{update}&nbsp;{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Удалить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Изменить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
//                    'data-request-method' => 'post',
                ]);
            },
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-succes" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Просмотр',
                ]);
            }
        ],
    ],

];   