<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ingredient */

?>
<div class="ingredient-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
