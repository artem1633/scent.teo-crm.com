<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ingredient */
?>
<div class="ingredient-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'article',
                'name_rus',
                'name_eng',
                [
                    'attribute' => 'developer_id',
                    'value' => $model->developer->name ?? null,
                ],
                'description:ntext',
                [
                    'attribute' => 'testable',
                    'value' => $model->testable ? 'Да' : 'Нет',
                ],
                'intensive_flavor_id',
                'substantivity',
                'const_price',
                'cost_price_value',
                'place_num',
                'solvent',
                [
                    'attribute' => 'solvent_percent',
                    'value' => Yii::$app->formatter->format($model->solvent_percent, ['percent', 2])

                ],

                'cas',
                [
                    'attribute' => 'input_level',
                    'value' => Yii::$app->formatter->format($model->input_level, ['percent', 2])

                ],
                'description_additional:ntext',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
