<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Developer;
use app\models\LevelTriangleFlavor;
use app\models\IntensiveFlavor;
use app\models\Ingredient;
use app\models\IngredientImportantFlover;
use app\models\IngredientLevelTriangleFlavor;
use app\models\ImportantFlover;
use app\models\SubstanceType;

/* @var $this yii\web\View */
/* @var $model app\models\Ingredient */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->importantFlovers = ArrayHelper::getColumn(IngredientImportantFlover::find()->where(['ingredient_id' => $model->id])->all(), 'important_flover_id');
    $model->levelTriangleFlavors = ArrayHelper::getColumn(IngredientLevelTriangleFlavor::find()->where(['ingredient_id' => $model->id])->all(), 'level_triangle_flavor_id');
}

?>

<div class="ingredient-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name_rus')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name_eng')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'substance_type_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(SubstanceType::find()->all(), 'id', 'name'),
                'pluginOptions' => [
                    'placeholder' => 'Выберите тип'
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'developer_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(Developer::find()->all(), 'id', 'name'),
                'pluginOptions' => [
                    'placeholder' => 'Выберите производителя'
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'levelTriangleFlavors')->widget(Select2::class, [
                'data' => ArrayHelper::map(LevelTriangleFlavor::find()->all(), 'id', 'name'),
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'tokenSeparators' => [','],
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'intensive_flavor_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(IntensiveFlavor::find()->all(), 'id', 'name'),
                'pluginOptions' => [
                    'placeholder' => 'Выберите'
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'substantivity')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'const_price')->textInput() ?>
        </div>
        <div class="col-md-6">
            <div style="margin-top: 22px;">
                <?= $form->field($model, 'cost_price_value')->textInput(['placeholder' => 'за мл'])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'importantFlovers')->widget(Select2::class, [
                'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'placeholder' => 'Выберите',
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'place_num')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'solvent')->dropDownList(Ingredient::solventLabels(), ['prompt' => 'Выберите']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'solvent_percent')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'cas')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'input_level')->textInput() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description_additional')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'testable')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'has_in_storage')->checkbox() ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
