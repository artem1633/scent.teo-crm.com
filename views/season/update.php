<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Season */
?>
<div class="season-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
