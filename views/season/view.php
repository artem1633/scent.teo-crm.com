<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Season */
?>
<div class="season-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
