<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $ordersDataProvider \yii\data\ActiveDataProvider */
?>
<div class="client-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'firstname',
                'lastname',
                'phone',
                'email:email',
                'birthday:date',
                [
                    'attribute' => 'male',
                    'value' => $model->sexLabels()[$model->male] ?? null,
                ],
                'address',
                [
                    'attribute' => 'allergy',
                    'value' => $model->allergy ? 'Да': 'Нет',
                ],
                [
                    'attribute' => 'allergy_name',
                   'visible' => $model->allergy,
                ],
                'created_at',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

    <div class="panel panel-inverse order-index">
        <div class="panel-heading">
            <!--        <div class="panel-heading-btn">-->
            <!--        </div>-->
            <h4 class="panel-title">Заказы</h4>
        </div>
        <div class="panel-body">
            <div id="ajaxCrudDatatable">
                <?php
                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $ordersDataProvider,
                        'pjax' => true,
                        'columns' => require(Url::to('@app/views/order/_columns.php')),
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                } ?>
            </div>
        </div>
    </div>


</div>
