<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Developer */
?>
<div class="developer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
