<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Developer */

?>
<div class="developer-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
