<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $price_model app\models\Price */
/* @var $cost_models app\models\PriceCost[] */
/* @var $cost_model app\models\PriceCost */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="price-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-container">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($price_model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row price-cost-clone" style="display: none">
            <div class="col-xs-3">
                <?= $form->field($cost_model, 'types[]')->dropDownList($cost_model->getTypes(), [
                    'prompt' => 'Выберите тип...'
                ]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($cost_model, 'volumes[]')->textInput() ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($cost_model, 'costs[]')->textInput() ?>
            </div>
            <div class="col-xs-1" style="margin-top: 20px;">
                <?= Html::a('<i class="fa fa-trash-o"></i>', ['#'],
                    [
                        'title' => 'Удалить параметры',
                        'class' => 'text-danger remove-params',
                        'style' => 'font-size: 2rem',
                    ]) ?>
            </div>
        </div>

            <?php foreach ($cost_models as $cost_model): ?>
                <div class="row price-cost">
                    <div class="col-xs-3">
                        <?= $form->field($cost_model, 'types[]')->dropDownList($cost_model->getTypes(),[
                                'value' => $cost_model->type,
                        ]) ?>
                    </div>
                    <div class="col-xs-4">
                        <?= $form->field($cost_model, 'volumes[]')->textInput([
                            'value' => $cost_model->volume,
                        ]) ?>
                    </div>
                    <div class="col-xs-4">
                        <?= $form->field($cost_model, 'costs[]')->textInput([
                            'value' => $cost_model->cost,
                        ]) ?>
                    </div>
                    <div class="col-xs-1" style="margin-top: 20px;">
                        <?= Html::a('<i class="fa fa-trash-o"></i>', ['#'],
                            [
                                'title' => 'Удалить параметры',
                                'class' => 'text-danger remove-params',
                                'style' => 'font-size: 2rem',
                            ]) ?>
                    </div>
                </div>
            <?php endforeach; ?>
    </div>

    <hr>

    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить параметры к услуге', ['#'],
                [
                    'id' => 'add-params',
                    'title' => 'Доабвить услугу',
                    'class' => 'btn btn-info'
                ]) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
