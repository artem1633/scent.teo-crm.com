<?php

use app\models\PriceCost;

/* @var $this yii\web\View */
/* @var $price_model app\models\Price */
/* @var $cost_models PriceCost */
/* @var $types PriceCost модели с разными типами */

?>
<div class="price-view">
    <?php /** @var PriceCost $type */
    foreach ($data as $name => $items):?>
        <table class="table table-bordered">
            <caption>
                <h3> <?= $name ?></h3>
            </caption>
            <thead>
            <tr>
                <th> ...</th>
                <?php foreach ($items as $service_name => $prices): ?>
                    <?php foreach ($prices as $volume => $cost): ?>
                        <th>
                            <?= $volume ?> мл.
                        </th>
                    <?php endforeach; ?>
                    <?php break; ?>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($items as $service_name => $prices): ?>
                <tr>

                    <td><?= $service_name ?> </td>
                    <?php foreach ($prices as $volume => $cost): ?>
                        <td>
                            <?= $cost ?> руб.
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endforeach; ?>

</div>
