<?php

/* @var $this yii\web\View */
/* @var $price_model app\models\Price */
/* @var $cost_models app\models\PriceCost */
/* @var $cost_model app\models\PriceCost */

?>
<div class="price-update">

    <?= $this->render('_update_form', [
        'price_model' => $price_model,
        'cost_models' => $cost_models,
        'cost_model' => $cost_model,
    ]) ?>

</div>
