<?php

use app\models\PriceCost;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $price_model app\models\Price */
/* @var $cost_model app\models\PriceCost */
/* @var $cost_models app\models\PriceCost[] */
/* @var $form yii\widgets\ActiveForm */

if (isset($cost_model)){
    $c_model = $cost_model;
} else {
    $c_model = $cost_models;
}

?>

<div class="price-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-container">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($price_model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row price-cost-clone" style="display: none">
            <div class="col-xs-3">
                <?= $form->field($c_model, 'types[]')->dropDownList((new PriceCost())->getTypes(), [
                    'prompt' => 'Выберите тип...'
                ]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($c_model, 'volumes[]')->textInput() ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($c_model, 'costs[]')->textInput() ?>
            </div>
            <div class="col-xs-1" style="margin-top: 20px;">
                <?= Html::a('<i class="fa fa-trash-o"></i>', ['#'],
                    [
                        'title' => 'Удалить параметры',
                        'class' => 'text-danger remove-params',
                        'style' => 'font-size: 2rem',
                    ]) ?>
            </div>
        </div>
    </div>



    <hr>

    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить параметры к услуге', ['#'],
                [
                    'id' => 'add-params',
                    'title' => 'Доабвить услугу',
                    'class' => 'btn btn-info'
                ]) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
