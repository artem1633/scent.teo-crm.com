<?php

use app\models\PriceCost;

/* @var $this yii\web\View */
/* @var $price_model app\models\Price */
/* @var $cost_models PriceCost */
/* @var $types PriceCost модели с разными типами*/

?>
<div class="price-view">
    <?php /** @var PriceCost $type */
    foreach($types as $type):?>
        <?php
            Yii::info($type->toArray(), 'test');
        ?>
        <table class="table table-bordered">
            <caption>
                <?= $type->getTypes()[$type->type] ?>
            </caption>
            <thead>
            <tr>
                <?php /** @var PriceCost $cost_model */
                foreach ($type->getByType($price_model->id) as $cost_model): ?>
                    <td>
                        <?= $cost_model->volume ?> мл
                    </td>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php /** @var PriceCost $cost_model */
                foreach ($type->getByType($price_model->id) as $cost_model): ?>
                    <td>
                        <?= $cost_model->cost ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            </tbody>
        </table>
    <?php endforeach; ?>

</div>
