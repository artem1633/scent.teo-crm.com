<?php

/* @var $this yii\web\View */
/* @var $price_model app\models\Price */
/* @var $cost_model app\models\PriceCost */

?>
<div class="price-create">
    <?= $this->render('_create_form', [
        'price_model' => $price_model,
        'cost_model' => $cost_model,
    ]) ?>
</div>
