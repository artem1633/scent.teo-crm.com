<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\OperationArticle;

/* @var $this yii\web\View */
/* @var $model app\models\OperationArticle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operation-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(OperationArticle::typeLabels()) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
