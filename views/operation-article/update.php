<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OperationArticle */
?>
<div class="operation-article-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
