<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OperationArticle */
?>
<div class="operation-article-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'type',
        ],
    ]) ?>

</div>
