<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\OperationArticle;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Точка продаж',
        'value'=>'remain.market.name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Ингредиент',
        'value'=>'remain.ingredient.name_rus',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'value' => function($model){
            return ArrayHelper::getValue(OperationArticle::typeLabels(), $model->type);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'article_id',
        'value' => 'article.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'quantity',
        'format' => 'decimal'
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'amount_last',
         'format' => 'decimal'
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'datetime',
         'format' => ['date', 'php:d M Y H:i:s'],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'manager_id',
         'value' => 'manager.login'
     ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'template' => '{update}{delete}',
//        'buttons' => [
//            'delete' => function ($url, $model) {
//                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Удалить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                    'data-confirm-title'=>'Вы уверены?',
//                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
//                ]);
//            },
//            'update' => function ($url, $model) {
//                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            }
//        ],
//    ],

];   