<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RemainOperation */

?>
<div class="remain-operation-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
