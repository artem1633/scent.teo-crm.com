<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RemainOperation */
?>
<div class="remain-operation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'remain_id',
            'method',
            'type',
            'article_id',
            'quantity',
            'amount_last',
            'datetime',
            'manager_id',
        ],
    ]) ?>

</div>
