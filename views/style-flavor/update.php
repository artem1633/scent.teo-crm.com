<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StyleFlavor */
?>
<div class="style-flavor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
