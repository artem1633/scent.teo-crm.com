<?php

use app\models\Client;
use app\models\Finance;
use app\models\Market;
use app\models\Order;
use app\models\User;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Finance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'order_id')->widget(Select2::className(), [
            'data' => (new Order())->getList(),
        'options' => [
                'prompt' => 'Выберите заказ',
        ]
    ]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(Finance::typeLabels()) ?>

    <?= $form->field($model, 'payment_type')->textInput() ?>

    <?= $form->field($model, 'client_id')->widget(Select2::className(), [
        'data' => (new Client())->getList(),
        'options' => [
            'prompt' => 'Выберите клиента',
        ]
    ]) ?>

    <?= $form->field($model, 'manager_id')->widget(Select2::className(), [
        'data' => (new User())->getManagersList(),
        'options' => [
            'prompt' => 'Выберите менеджера',
        ]
    ]) ?>

    <?= $form->field($model, 'market_id')->widget(Select2::className(), [
        'data' => (new Market())->getList(),
        'options' => [
            'prompt' => 'Выберите точку продаж',
        ]
    ]) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
