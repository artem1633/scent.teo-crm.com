<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Finance;
use yii\helpers\ArrayHelper;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_id',
        'value' => function($model){
            return '№'.$model->id;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'amount',
        'format' => ['currency', 'rub']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'value' => function($model){
            return ArrayHelper::getValue(Finance::typeLabels(), $model->type);
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'payment_type',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'value' => function($model){
            $client = $model->client ?? null;
            if ($client){
                \Yii::info($model->client, 'test');
                return $model->client->getFullName() ?? null;
            }
            return null;
        }
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'manager_id',
         'value' => 'manager.login',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'market_id',
         'value' => 'market.name',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'datetime',
         'format' => ['date', 'php:d M Y H:i:s'],
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   