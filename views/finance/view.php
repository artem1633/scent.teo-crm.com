<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Finance */
?>
<div class="finance-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'amount',
            'type',
            'payment_type',
            'client_id',
            'manager_id',
            'market_id',
            'datetime',
        ],
    ]) ?>

</div>
