<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderIngredient */
?>
<div class="order-ingredient-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'ingredient_id',
            'dilute',
            'count',
            'is_main',
            'comment:ntext',
            'can_delete',
        ],
    ]) ?>

</div>
