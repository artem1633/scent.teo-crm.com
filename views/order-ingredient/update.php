<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderIngredient */
?>
<div class="order-ingredient-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
