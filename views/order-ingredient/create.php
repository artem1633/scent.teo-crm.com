<?php

/* @var $this yii\web\View */
/* @var $model app\models\OrderIngredient */

?>
<div class="order-ingredient-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
