<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderIngredient */
/* @var $form yii\widgets\ActiveForm */
/* @var bool $increase Увеличение ли */
?>

<div class="order-ingredient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($increase == true): ?>

        <?= $form->field($model, 'increase_value')->textInput() ?>

    <?php else: ?>

        <?= $form->field($model, 'decrease_value')->textInput() ?>

    <?php endif; ?>


    <?php ActiveForm::end(); ?>

</div>
