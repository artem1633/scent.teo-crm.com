<?php

use app\models\Ingredient;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderIngredient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-ingredient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ingredient_id')->widget(Select2::class, [
        'data' => (new Ingredient())->getList(),
    ]) ?>

    <?= $form->field($model, 'dilute')->textInput() ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'is_main')->checkbox() ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'can_delete')->checkbox() ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
