<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inventory */
?>
<div class="inventory-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
