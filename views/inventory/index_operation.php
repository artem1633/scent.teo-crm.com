<?php
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\OperationArticle;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RemainOperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'pjax'=>true,
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    // [
                    // 'class'=>'\kartik\grid\DataColumn',
                    // 'attribute'=>'id',
                    // ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label' => 'Ингредиент',
                        'value'=>'remain.ingredient.name_rus',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'type',
                        'value' => function($model){
                            return ArrayHelper::getValue(OperationArticle::typeLabels(), $model->type);
                        }
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'quantity',
                        'format' => 'decimal'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'amount_last',
                        'format' => 'decimal'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'datetime',
                        'format' => ['date', 'php:d M Y H:i:s'],
                    ],
                ],
                'panelBeforeTemplate' => '',
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>'',
                ]
            ])?>
