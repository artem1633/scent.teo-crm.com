<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Inventory */
?>
<div class="inventory-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'market_id',
            'name',
            'comment:ntext',
            'datetime',
            'manager_id',
        ],
    ]) ?>

</div>
