<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Remain */

?>
<div class="remain-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
