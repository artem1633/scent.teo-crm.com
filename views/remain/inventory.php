<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Market;
use app\models\forms\InventoryForm;
use app\models\Remain;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\forms\InventoryForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $step int */

?>

<div class="remain-form">

    <?php $form = ActiveForm::begin(); ?>

    <div style="<?= $model->scenario == InventoryForm::SCENARIO_DEFAULT ? '' : 'display: none;'?>">
        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>

            <?= $form->field($model, 'marketId')->widget(Select2::class, [
                'data' => ArrayHelper::map(Market::find()->all(), 'id', 'name'),
            ]) ?>

        <?php endif; ?>

        <?= $form->field($model, 'name')->textInput() ?>

        <?= $form->field($model, 'comment')->textarea() ?>
    </div>

    <div style="<?= $model->scenario == InventoryForm::SCENARIO_INGREDIENTS ? '' : 'display: none;'?>">
        <?php if($model->marketId != null && $step == 1): ?>
            <?php
//            $remains = Remain::find()->where(['market_id' => $model->marketId])->all();
            $dataProvider = new ActiveDataProvider();
            $dataProvider->query = Remain::find()->where(['market_id' => $model->marketId])->orderBy('id desc');
            $dataProvider->pagination = false;
//            $dataProvider->setModels($remains);
            ?>

            <?= $this->render('@app/views/remain/index-mini', [
                'searchModel' => new \app\models\RemainSearch(),
                'dataProvider' => $dataProvider,
            ]) ?>

        <?php endif; ?>



    </div>

    <?php ActiveForm::end(); ?>

</div>

