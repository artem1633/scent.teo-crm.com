<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Remain */
?>
<div class="remain-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'market_id',
                'ingredient_id',
                'amount',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
