<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\RemainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Остатки";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse remain-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Остатки</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'panelBeforeTemplate' => Html::a('Приход <i class="fa fa-plus"></i>', ['coming'],
                            ['role' => 'modal-remote', 'title' => 'Приход', 'class' => 'btn btn-success']) . '&nbsp;' .
                        Html::a('Списание <i class="fa fa-minus"></i>', ['write-off'],
                            ['role' => 'modal-remote', 'class' => 'btn btn-danger', 'title' => 'Списание']) .
                        Html::a('Инвентаризация <i class="fa fa-pencil"></i>', ['inventory'], [
                            'role' => 'modal-remote',
                            'title' => 'Провести инвентаризацию',
                            'class' => 'btn btn-warning pull-right'
                        ]),
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after' => '',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
