<?php

use app\models\Remain;
use yii\helpers\Html;


return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'market_id',
        'value' => 'market.name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ingredient_id',
        'value' => 'ingredient.name_rus',
        'content' => function($data){
            if($data->ingredient != null){
                return Html::a($data->ingredient->name_rus, ['ingredient/view', 'id' => $data->ingredient_id], ['role' => 'modal-remote']);
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'amount',
        'format' => 'decimal',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'has_in_storage',
        'content' => function(Remain $model){
            $has_in_storage = $model->ingredient->has_in_storage ?? 0;
            $style = 'style="font-size: 2rem;"';
            if ($has_in_storage){
                return '<i class="fa fa-calendar-check-o alert-success" '. $style. '></i>';
            }
            return '<i class="fa fa-calendar-times-o alert-danger" '. $style. '></i>';

        },
        'hAlign' => 'center',
        'filter' => [0 => 'Отсутствует', 1 => 'В наличии']
    ],

//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'template' => '{update}{delete}',
//        'buttons' => [
//            'delete' => function ($url, $model) {
//                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Удалить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                    'data-confirm-title'=>'Вы уверены?',
//                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
//                ]);
//            },
//            'update' => function ($url, $model) {
//                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            }
//        ],
//    ],

];   