<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Market;
use app\models\OperationArticle;
use app\models\Ingredient;

/* @var $this yii\web\View */
/* @var $model app\models\forms\RemainOperationForm */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="remain-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (Yii::$app->user->identity->isSuperAdmin()): ?>

        <?= $form->field($model, 'marketId')->widget(Select2::class, [
            'data' => ArrayHelper::map(Market::find()->all(), 'id', 'name'),
        ]) ?>

    <?php endif; ?>

    <?= $form->field($model, 'ingredientId')->widget(Select2::class, [
        'data' => ArrayHelper::map(Ingredient::find()->all(), 'id', 'name_rus'),
    ]) ?>

    <?= $form->field($model, 'articleId')->widget(Select2::class, [
        'data' => ArrayHelper::map(OperationArticle::find()->where(['type' => OperationArticle::TYPE_WRITE_OFF])->all(),
            'id', 'name'),
        'options' => [
                'placeholder' => 'Выберите значение...'
        ]
    ]) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'orderId')->hiddenInput()->label(false) ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group pull-right">
            <?= Html::submitButton('Списать', ['class' => 'btn btn-warning']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

