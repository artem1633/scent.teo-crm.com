<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Age */
?>
<div class="age-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
