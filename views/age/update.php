<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Age */
?>
<div class="age-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
