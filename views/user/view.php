<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */
?>
<div class="user-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'created_at',
                'login',
                'name',
                [
                    'attribute' => 'birthday_date',
                    'value' => Yii::$app->formatter->asDate($model->birthday_date),
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'gender',
                    'value' => $model->gender ? $model->getGenderLabels()[$model->gender] : '',
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'market_id',
                    'value' => $model->market->name ?? null,
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'role',
                    'value' => $model->getRoleLabels()[$model->role] ?? null,
                    'format' => 'raw',
                ],
                'phone',
                'email',
                [
                    'attribute' => 'is_deletable',
                    'value' => $model->is_deletable ? 'Да' : 'Нет',
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
