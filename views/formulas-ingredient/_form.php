<?php

use app\models\Ingredient;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FormulasIngredient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="formulas-ingredient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ingredient_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(Ingredient::find()->all(), 'id', 'name_rus'),
        'options' => [
            'placeholder' => 'Выберите ингредиент',
        ],
        'pluginEvents' => [
            "select2:select" => "function() {
                var select_id = $('#formulasingredient-ingredient_id').val();
                console.log('Select: ' + select_id);
                $.get(
                    '/ingredient/get-in-storage?id=' + select_id,
                    function(response){
                        $('#in-storage-chk').prop('checked', response);
                    }
                )
            }",
        ]
    ]) ?>

    <?= $form->field($model, 'dilute')->textInput() ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'is_main')->checkbox() ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'can_delete')->checkbox() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'has_in_storage')->checkbox([
                'disabled' => true,
                'id' => 'in-storage-chk'
            ]) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
