<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\Remain;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'can_delete',
        'label' => 'Можно удалить',
        'content' => function($model){
            if($model->can_delete){
                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
            } else {
                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
            }
        },
        'width' => '10px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ingredient_id',
        'value' => 'ingredient.name_rus',
        'content' => function($data){
            return Html::a($data->ingredient->name_rus, ['ingredient/view', 'id' => $data->ingredient_id], ['role' => 'modal-remote']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dilute',
        'format' => 'percent',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_main',
        'value' => function($model){
            return ArrayHelper::getValue(\app\models\FormulasIngredient::isMainLabels(), $model->is_main);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ingredient.description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ingredient.testable',
        'content' => function($model){
            if($model->ingredient->testable){
                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
            } else {
                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
            }
        },
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'has_in_storage',
        'content' => function($model){
            $marketId = Yii::$app->user->identity->market_id;

            if($marketId == null){
                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
            }

            $remain = Remain::find()->where(['market_id' => $marketId, 'ingredient_id' => $model->ingredient_id])->one();

            if($remain == null){
                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
            }

            if($remain->amount > 0){
                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
            }

//            if($model->has_in_storage){
//                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
//            } else {
//                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
//            }
        },
        'width' => '10px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'format' => 'text',
        'width' => '300px;',
        'contentOptions' => ['style' => 'white-space: pre-line; word-break: break-all;'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["formulas-ingredient/{$action}",'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   