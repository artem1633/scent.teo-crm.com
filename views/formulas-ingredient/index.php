<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FormulasIngredientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Formulas */

CrudAsset::register($this);

?>
<?php
try {
    echo GridView::widget([
        'id' => 'crud-datatable-formulas-ingredient',
        'dataProvider' => $dataProvider,
        //            'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_columns.php'),
        'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>',
                ['formulas-ingredient/create', 'formula_id' => $model->id],
                ['role' => 'modal-remote', 'title' => 'Добавить формулу', 'class' => 'btn btn-success']) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
        ],
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
} ?>


<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
