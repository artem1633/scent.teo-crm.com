<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FormulasIngredient */
?>
<div class="formulas-ingredient-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'ingredient_id',
                [
                    'attribute' => 'dilute',
                    'format' => 'percent'
                ],
                'count',
                'is_main',
                'comment:ntext',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
