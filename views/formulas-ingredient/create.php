<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FormulasIngredient */

?>
<div class="formulas-ingredient-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
