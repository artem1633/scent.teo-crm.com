<?php

use app\models\OrderIngredient;
use app\models\Remain;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/* @var $model app\models\Order */
/* @var $orderIngredientDataProvider \yii\data\ActiveDataProvider */
/* @var $client app\models\Client */

?>

    <h4>Окончательная формула аромата: </h4>
<?php
try {
    echo GridView::widget([
        'id' => 'crud-order-ingredient-datatable',
        'dataProvider' => $orderIngredientDataProvider,
        //            'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'can_delete',
                'label' => 'Можно удалить',
                'content' => function (OrderIngredient $model) {
                    if ($model->can_delete) {
                        return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
                    } else {
                        return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
                    }
                },
                'width' => '10px',
                'hAlign' => GridView::ALIGN_CENTER,
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient_id',
                'value' => 'ingredient.name_rus',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'dilute',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'count',
                'format' => 'decimal'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                //            'attribute'=>'count',
                'label' => 'Кол-во на складе',
                'content' => function ($data) use ($model) {
                    if ($model->isNewRecord) {
                        return '';
                    }

                    $ingredient = $data->ingredient;

                    $count = 0;

                    /** @var Remain $remain */
                    $remain = Remain::find()->where([
                        'market_id' => $model->manager->market_id,
                        'ingredient_id' => $ingredient->id
                    ])->one();

                    if ($remain) {
                        $count = $remain->amount;
                    }

                    $count = Yii::$app->formatter->asDecimal($count);

                    $output = $count;

                    if ($count <= 0) {
                        $output = "<span class='text-danger'>{$count}</span>";
                    }

                    return $output;
                },
                'format' => 'decimal'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'is_main',
                'value' => function ($model) {
                    return ArrayHelper::getValue(\app\models\FormulasIngredient::isMainLabels(), $model->is_main);
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient.description',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient.testable',
                'content' => function ($model) {
                    if ($model->ingredient->testable) {
                        return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
                    } else {
                        return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
                    }
                },
                'hAlign' => GridView::ALIGN_CENTER,
            ],
        ],
        'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>',
                ['order-ingredient/create', 'order_id' => $model->id],
                ['role' => 'modal-remote', 'title' => 'Добавить формулу', 'class' => 'btn btn-success']) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
        ],
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
} ?>

<?= Html::a('<i class="fa fa-lg fa-arrow-circle-left"></i>&nbsp;&nbsp;Назад: Корректировка аромата',
    ['step-back', 'id' => $model->id, 'value' => 1],
    ['class' => 'btn btn-primary pull-left']) ?>