<?php

use app\models\OrderFormulasIngredient;
use app\models\Remain;
use kartik\sortable\Sortable;

/** @var $model \app\models\Order $ids */

$ids = OrderFormulasIngredient::find()
    ->where(['order_id' => $model->id])
    ->select('formulas_ingredient_id')
    ->distinct()
    ->asArray()
    ->column();

/** @var OrderFormulasIngredient[] $formulasIngredients */
$formulasIngredients = OrderFormulasIngredient::find()
    ->andWhere(['formulas_ingredient_id' => $ids])
    ->andWhere(['order_id' => $model->id])
    ->orderBy('mark')
    ->all();

Yii::info('Найдено ингредиентов: ' . count($formulasIngredients), 'test');
$ingredientItems = [];
$ingredients = [];
$ing_ids = [];

if (count($formulasIngredients) == 0) {
    echo '<p class="text-danger">Ингредиенты не найдены!</p>';
}



$counter = 1;
/** @var OrderFormulasIngredient $item */
foreach ($formulasIngredients as $item) {
    Yii::info($item->toArray(), 'test');
    $ing = $item->formulasIngredient->ingredient;

    Yii::info($ing->attributes, 'test');

    $manager = $model->manager;
    /** @var Remain $remain */
    $remain = Remain::find()->where([
        'market_id' => $manager->market_id,
        'ingredient_id' => $ing->id
    ])->andWhere(['>', 'amount', 0])->one();

    $del_btn = \yii\helpers\Html::a('x', ['/order/delete-ingredient', 'id' => $item->id], [
//        'role' => 'modal-remote',
//        'title' => 'Удалить',
//        'data-confirm' => false,
//        'data-method' => false,// for overide yii data api
//        'data-request-method' => 'post',
//        'data-confirm-title' => 'Вы уверены?',
//        'data-confirm-message' => 'Вы действительно хотите удалить ' . $ing->name_rus . '?',
//        'data-confirm-ok' => 'Удалить',
//        'data-confirm-cancel' => 'Отмена'
    ]);


    $otherInfo = '';
    $count = 0;

    if ($remain) {
        $count = $remain->amount;
    }

    if ($count <= 0) {
        $otherInfo = "<i class='text-danger pull-right'>(Нет на складе)<span class='del-ing'>{$del_btn}</span></i>";
    } else {
        $otherInfo = "<span class='text-success pull-right'>" . Yii::$app->formatter->asDecimal($count) . "<span class='del-ing'>$del_btn</span></span>";
    }

//    $ingredientItems[] = ['content' => "<div class='ingredient-id' data-id='{$item->id}'>$ing->name_rus {$otherInfo}</div>"];

    if (!in_array($ing->id, $ing_ids)) {
        if($count > 0){
            $ingredientItems[] = ['content' => "<div class='ingredient-id' data-id='{$item->id}'><span class='counter'>$counter.</span> $ing->name_rus {$otherInfo}</div>"];
        }
        array_push($ing_ids, $ing->id);
        $counter++;
        if ($counter > 12) {
            break;
        }
    }


}

//Yii::info($ingredients, 'test');

//echo $form->field($model, 'formula_id')->textInput();

try {
    echo Sortable::widget([
        'id' => 'ingredients',
        'items' => $ingredientItems,
        'pluginEvents' => [
            'sortupdate' => 'function(e, e2){

                                var sortingArray = [];
                                $("#ingredients li").each(function(){
                                    var ingredientId = $(this).find(".ingredient-id").data("id");
                                    if(ingredientId != null){
                                        sortingArray.push(ingredientId);
                                    }
                                });

                                $.ajax({
                                    url: "/order/sorting-ingredients",
                                    method: "POST",
                                    data: {
                                        data: JSON.stringify(sortingArray),
                                        csrfParam: yii.getCsrfToken(),
                                    },
                                    success: function(response){
                                        console.log("response", response);
                                        var elems = $(".counter");
                                        var elemsTotal = elems.length;
                                        for(var i=0; i < elemsTotal; ++i){$(elems[i]).text((i + 1) + ".")}
                                    },
                                });

    //                            console.log(sortingArray);
                            }',
        ],
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
}
