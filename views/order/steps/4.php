<?php


use yii\helpers\Html;

/* @var $formulasSearchModel \app\models\FormulasSearch */
/* @var $formulasDataProvider \yii\data\ActiveDataProvider */
/* @var $model app\models\Order */


echo Html::a('Добавить ингредиент', ['view-ingredients', 'order_id' => $model->id], [
    'class' => 'btn btn-success',
     'role' => 'modal-remote',
    'data-pjax' => 1,
    'style' => 'margin-bottom: 20px;',
]);

?>


<div class="ingredients-4">
    <?= $this->render('_step_4_ingredients.php', [
        'model' => $model,
    ]) ?>
</div>

<?= $this->render('@app/views/order/_formulas', [
    'searchModel' => $formulasSearchModel,
    'dataProvider' => $formulasDataProvider
]) ?>


<?= Html::a('<i class="fa fa-lg fa-arrow-circle-left"></i>&nbsp;&nbsp;Назад: Анкета ароматов',
    ['step-back', 'id' => $model->id, 'value' => 1],
    ['class' => 'btn btn-primary pull-left']) ?>
<?php //Pjax::end() ?>

<?php

$script = <<<JS
$(document).on('click', '#add-ingredient', function(){
    var ingredient_id = $('#select-ingredient-4').val();
    var order_id = $(this).attr('data-order');
    $.get(
        '/ingredient/get-ingredient',
        {id: ingredient_id, order_id: order_id},
        function(response) {
            $('#ingredients').prepend(response);
        }
    )
});

$(document).on('click', '.del-ing', function(e) {
    e.preventDefault();
    var span = $(this);
    var href = span.find('a').attr('href');
   
    console.log(href);
    console.log(span.html());
    
   if (confirm("Действительно удалить?"))
      {
        $.post(
            href,
            function(response) {
                console.log(response);
                if (response['success'] === 1){
                    var target_li = span.parents('li');
                    console.log(target_li);
                    var test = target_li.html();
                    console.log(test);
                    target_li.remove();
                }                    
            }
        )
      }
      return false;
})
JS;

$this->registerJs($script);
