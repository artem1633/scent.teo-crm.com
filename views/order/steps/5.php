<?php


use yii\helpers\Html;

/* @var $client app\models\Client */
/* @var $formulasSearchModel \app\models\FormulasSearch */
/* @var $formulasDataProvider \yii\data\ActiveDataProvider */
/* @var $model app\models\Order */

//\yii\helpers\VarDumper::dump($formulasDataProvider, 30, true);

?>
<?php if ($model->formula_id != null): ?>
    <h4>Ваша формула: <?= $model->formula->name ?? 'Ошибка' ?></h4>
<?php endif; ?>

<?php if (in_array('formula_id', array_keys($model->errors))): ?>
    <div class="alert alert-danger">
        <b>Ошибка</b> <?= $model->errors['formula_id'][0] ?>
    </div>
<?php endif; ?>

<div class="hidden">
    <?= $form->field($model, 'formula_id')->textInput([
        'id' => 'order-formula_id',
    ]) ?>
</div>

<?= $this->render('@app/views/order/_formulas', [
    'searchModel' => $formulasSearchModel,
    'dataProvider' => $formulasDataProvider,
    'model' => $model->formula,
]) ?>

<?php

$formulaScript = <<<JS
    var grid = $('#crud-datatable');
    grid.on('grid.radiochecked', function(ev, key, val) {
        console.log("Key = " + key + ", Val = " + val);
        var row = $("tr[data-key='" + key + "']");
        var radio = row.find("input[type='radio']");
        var id = radio.attr('data-id');
        $('#order-formula_id').val(id);
    });
    
    grid.on('grid.radiocleared', function(ev, key, val) {
       $('#order-formula_id').val('');    
    });
    
    check();
    
    //check radio
    function check(){
        console.log('Check function');
        var formula_id = "{$model->formula_id}";
        console.log('Formula id ' + formula_id);
        var target_radio = $('[data-id="' + formula_id + '"]');
        target_radio.attr('checked', true);
            
    };
JS;

$this->registerJs($formulaScript, \yii\web\View::POS_READY);

?>

<?= Html::a('<i class="fa fa-lg fa-arrow-circle-left"></i>&nbsp;&nbsp;Назад: Дегустация ингредиентов',
    ['step-back', 'id' => $model->id, 'value' => 1],
    ['class' => 'btn btn-primary pull-left']) ?>
