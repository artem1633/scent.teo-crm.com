<?php

/* @var $model app\models\Order */
/* @var $client app\models\Client */

?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'client_mark')->dropDownList([
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10,
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'mark_client_comment')->textarea() ?>
    </div>
</div>