<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FormulasIngredientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\FormulasIngredient */

//CrudAsset::register($this);
//\yii\helpers\VarDumper::dump($dataProvider->getModels(), 10, true);
?>
<?php
try {
    echo GridView::widget([
        'id' => 'crud-order-datatable',
        'dataProvider' => $dataProvider,
        //            'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient_id',
                'value' => 'ingredient.name_rus',
                'content' => function($data){
                    return Html::a($data->ingredient->name_rus, ['ingredient/view', 'id' => $data->ingredient_id], ['role' => 'modal-remote']);
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'dilute',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'count',
                'format' => 'decimal'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient.place_num',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'comment',
                'format' => 'text',
                'width' => '300px;',
                'contentOptions' => ['style' => 'white-space: pre-line; word-break: break-all;'],
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign' => 'middle',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(["order-ingredient/{$action}", 'id' => $key]);
                },
                'template' => '{update}{delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                            'role' => 'modal-remote',
                            'title' => 'Удалить',
                            'data-confirm' => false,
                            'data-method' => false,// for overide yii data api
                            'data-request-method' => 'post',
                            'data-confirm-title' => 'Вы уверены?',
                            'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                                'role' => 'modal-remote',
                                'title' => 'Изменить',
                                'data-confirm' => false,
                                'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                            ]) . "&nbsp;";
                    }
                ],
            ],
        ],
        'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>',
                ['order-ingredient/create', 'order_id' => $model->id],
                ['role' => 'modal-remote', 'title' => 'Добавить формулу', 'class' => 'btn btn-success']) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
        ],
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
} ?>


<!---->
<?php //Modal::begin([
//    "id"=>"ajaxCrudModal",
//    "footer"=>"",// always need it for jquery plugin
//])?>
<?php //Modal::end(); ?>
