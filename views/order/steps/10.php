<?php

/* @var $model app\models\Order */
/* @var $client app\models\Client */
/* @var $stepSearchModel app\models\OrderStepSearch */
/* @var $stepDataProvider \yii\data\ActiveDataProvider */

?>

<?= $this->render('@app/views/order/_history', [
    'model' => $model,
    'searchModel' => $stepSearchModel,
    'dataProvider' => $stepDataProvider,
    'changeLogSearchModel' => $changeLogSearchModel ?? null,
    'changeLogDataProvider' => $changeLogDataProvider ?? null,
]) ?>