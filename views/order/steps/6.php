<?php

/* @var $model app\models\Order */

use app\models\Remain;
use yii\helpers\Html;

/* @var $client app\models\Client */
/* @var $orderIngredientSearchModel \app\models\OrderIngredientSearch */
/* @var $orderIngredientDataProvider \yii\data\ActiveDataProvider */
/* @var $model \app\models\Order */

?>
<?php

$likeEnableBtn = true;
$orderIngredientDataProvider->pagination = false;

if ($model->isNewRecord == false) {
    foreach ($orderIngredientDataProvider->models as $orderIngredient) {
        /** @var \app\models\Ingredient $ingredient */
        $ingredient = $orderIngredient->ingredient;

        $count = 0;

        /** @var Remain $remain */
        $remain = Remain::find()->where([
            'market_id' => $model->manager->market_id,
            'ingredient_id' => $ingredient->id
        ])->one();

        if ($remain) {
            $count = $remain->amount;
        }

        Yii::info('Ингредиент: ' . $ingredient->name_rus . ', Кол-во: ' . $count, 'test');

        if ($count <= 0) {
            $likeEnableBtn = false;
            break;
        }
    }
}

?>

<?= $this->render('_step_6_ingredients', [
    'searchModel' => $orderIngredientSearchModel,
    'dataProvider' => $orderIngredientDataProvider,
    'model' => $model,
]) ?>

<div class="form-group pull-left">
    <?php
    //        echo Html::a('Корректировка', ['update'], [
    //            'class' => 'btn btn-danger',
    //        ])
    ?>
    <?php if ($likeEnableBtn): ?>
        <?= Html::a('<i class="fa fa-lg fa-thumbs-o-up"></i>&nbsp;&nbsp;Нравится',
            ['update', 'id' => $model->id], [
                'class' => 'btn btn-success',
            ]) ?>
    <?php else: ?>
        <?= Html::a('<i class="fa fa-lg fa-thumbs-o-up"></i>&nbsp;&nbsp;Нравится', '#', [
            'class' => 'btn btn-default',
            'onclick' => '
                                event.preventDefault();
                                alert("На складе нет ингредиентов");
                            '
        ]) ?>
    <?php endif; ?>
    <?= Html::a('<i class="fa fa-lg fa-arrow-circle-left"></i>&nbsp;&nbsp;Назад: Подбор формулы',
        ['step-back', 'id' => $model->id, 'value' => 1],
        ['class' => 'btn btn-primary pull-left']) ?>
</div>
