<?php


use app\models\OrderIngredient;
use app\models\Remain;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $client app\models\Client */
/* @var $orderIngredientSearchModel \app\models\OrderIngredientSearch */
/* @var $orderIngredientDataProvider \yii\data\ActiveDataProvider */
/* @var $model app\models\Order */

//Yii::info('Step 7:', 'test');
//Yii::info($model->attributes, 'test');
//Yii::info('Формула заказа:', 'test');
//Yii::info($model->formula->attributes, 'test');


?>

<?php if (Yii::$app->session->get('ingredient_deleted')):?>
    <?= Html::a('<i class="fa fa-lg fa-arrow-left"></i>&nbsp;&nbsp;Вернуться к смешиванию',
        ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning pull-right',
        ]); ?>

    <?php Yii::$app->session->set('ingredient_deleted', 0) ?>
<?php endif;?>

<?= Html::a('<i class="fa fa-lg fa-close"></i>&nbsp;&nbsp;Заменить формулу',
    ['update', 'id' => $model->id], [
        'class' => 'btn btn-danger pull-right',
    ]); ?>


<?php
?>
<?php
try {
    echo GridView::widget([
        'id' => 'crud-order-ingredient-datatable',
        'dataProvider' => $orderIngredientDataProvider,
//            'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'can_delete',
                'label' => 'Можно удалить',
                'content' => function (OrderIngredient $model) {
                    if ($model->can_delete) {
                        return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
                    } else {
                        return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
                    }
                },
                'width' => '10px',
                'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient_id',
                'value' => 'ingredient.name_rus',
                'content' => function($data){
                    return Html::a($data->ingredient->name_rus, ['ingredient/view', 'id' => $data->ingredient_id], ['role' => 'modal-remote']);
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'dilute',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'count',
                'format' => 'decimal',
                'content' => function($data) use($model){
                    $output = '';
                    $formulaIngredient = \app\models\FormulasIngredient::find()->where(['formula_id' => $model->formula_id, 'ingredient_id' => $data->ingredient_id])->one();

                    $baseCount = 0;

                    if($formulaIngredient->count != null){
                        $baseCount = $formulaIngredient->count;
                    }

                    $output = $baseCount;

                    if($data->count != null){
                        $output .= ' <span class="text-success">(+'.$data->count.')</span>';
                    }

                    return $output;
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
//            'attribute'=>'count',
                'label' => 'Кол-во на складе',
                'content' => function ($data) use ($model) {
                    if ($model->isNewRecord) {
                        return '';
                    }

                    $ingredient = $data->ingredient;

                    $count = 0;

                    $remain = Remain::find()->where([
                        'market_id' => $model->manager->market_id,
                        'ingredient_id' => $ingredient->id
                    ])->one();

                    if ($remain) {
                        $count = $remain->amount;
                    }

                    $count = Yii::$app->formatter->asDecimal($count);

                    $output = $count;

                    if ($count <= 0) {
                        $output = "<span class='text-danger'>{$count}</span>";
                    }

                    return $output;
                },
                'format' => 'decimal'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'is_main',
                'value' => function ($model) {
                    return ArrayHelper::getValue(\app\models\FormulasIngredient::isMainLabels(), $model->is_main);
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient.description',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient.testable',
                'content' => function ($model) {
                    if ($model->ingredient->testable) {
                        return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
                    } else {
                        return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
                    }
                },
                'hAlign' => GridView::ALIGN_CENTER,
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'comment',
                'format' => 'text',
                'width' => '300px;',
                'contentOptions' => ['style' => 'white-space: pre-line; word-break: break-all;'],
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign' => 'middle',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(["order-ingredient/{$action}", 'id' => $key]);
                },
                'template' => '{increase} {decrease} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                            'role' => 'modal-remote',
                            'title' => 'Удалить',
                            'data-confirm' => false,
                            'data-method' => false,// for overide yii data api
                            'data-request-method' => 'post',
                            'data-confirm-title' => 'Вы уверены?',
                            'data-confirm-message' => 'ВНИМАНИЕ! После удаления ингредиента потребуется заново смешать образец! Перед смешиванием, необходимо применить все прочие инструменты! <br>Вы действительно хотите удалить данную запись?',
                            'data-confirm-ok' => 'Удалить',
                            'data-confirm-cancel' => 'Отмена',
                        ]);
                    },
                    'increase' => function ($url, $model) {
                        return Html::a('<i class="fa fa-caret-square-o-up text-primary" style="font-size: 16px;"></i>',
                                $url, [
                                    'role' => 'modal-remote',
                                    'title' => 'Увеличить',
//                                'data-confirm' => false,
//                                'data-method' => false,// for overide yii data api
//                                'data-request-method' => 'post',
                                ]) . "&nbsp;";
                    },
                    'decrease' => function ($url, $model) {
                        return Html::a('<i class="fa fa-caret-square-o-down text-primary" style="font-size: 16px;"></i>',
                                $url, [
                                    'role' => 'modal-remote',
                                    'title' => 'Уменьшить',
//                                    'data-confirm' => false,
//                                    'data-method' => false,// for overide yii data api
//                                    'data-request-method' => 'post',
                                ]) . "&nbsp;";
                    }
                ],
            ],
        ],
        'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>',
                ['order-ingredient/create', 'order_id' => $model->id],
                ['role' => 'modal-remote', 'title' => 'Добавить формулу', 'class' => 'btn btn-success']) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
        ],
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
} ?>

<?= Html::a('<i class="fa fa-lg fa-arrow-circle-left"></i>&nbsp;&nbsp;Назад: Смешивание и дегустация',
    ['step-back', 'id' => $model->id, 'value' => 1],
    ['class' => 'btn btn-primary pull-left']) ?>


