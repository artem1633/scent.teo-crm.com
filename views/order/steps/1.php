<?php

use app\models\Client;
use app\models\Order;
use app\models\Price;
use app\models\PriceCost;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;

/* @var $model app\models\Order */

Yii::info($model->toArray(), 'test');

?>

    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'type')->dropDownList(Order::typeLabels(), [
                        'class' => 'form-control for-calc',
                        'id' => 'type-order'
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>Сумма заказа: <span id="total-price"><?= $model->price ?></span>&nbsp;руб.</h4>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'solvent')->dropDownList($model::solventLabels(), [
                        'id' => 'solvent-order',
                        'prompt' => 'Выберите значение...',
                    ]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'value')->dropDownList(PriceCost::getVolumesList(), [
                        'id' => 'value-order',
                        'class' => 'form-control for-calc',
                        'prompt' => 'Выберите значение...',
                    ]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'price_id')->dropDownList(Price::getList(), [
                        'id' => 'concentration-order',
                        'class' => 'form-control for-calc',
                        'prompt' => 'Выберите значение...',
                    ]) ?>
                </div>
            </div>
        </div>

    </div>
    <div class="row" style="display: flex; align-items: center;">
        <div class="col-md-10">
            <?= $form->field($model, 'client_id')->widget(Select2::className(), [
                'data' => Client::getList(),
                'options' => [
                    'id' => 'sel-clients',
                    'placeholder' => 'Выберите клиента...'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
                'pluginEvents' => [
                    'select2:closing' => "function(){
                                    var btn = $('#client-orders-btn');
                                    if ($(this).val() > 0 && $('#type-order').val() == 2){
                                        btn.attr('disabled', false);
                                        btn.removeClass('disabled');
                                    } else {
                                        btn.attr('disabled', true);
                                        btn.addClass('disabled');
                                    }
                                }"
                ],
            ])->label('Клиент'); ?>
        </div>
        <div class="col-md-2">
            <?= Html::button('Заказы клиента', [
                'id' => 'client-orders-btn',
                'class' => 'btn btn-warning btn-block disabled',
                'disabled' => (bool)!$model->client_id,
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-8 text-right">
            <?= $form->field($model, 'base_order_id')->hiddenInput(['id' => 'base-order'])->label(false); ?>
        </div>
    </div>

<?= Html::a('Это скрытая кнопка', ['client-orders'], [
    'id' => 'client-orders-btn-hidden',
    'class' => 'hidden',
    'role' => 'modal-remote',
    'method' => 'post',
]) ?>

<?php
$script = <<<JS
$(document).on('click', '#client-orders-btn', function() {
        var submit_btn = $('#client-orders-btn-hidden');
        var href = submit_btn.attr('href');
        submit_btn.attr('href', href + '?id=' + $('#sel-clients').val());
        setTimeout(function(){
            submit_btn.trigger('click');
        }, 300);
});

$(document).on('click', '#select-order-btn', function() {
    var base_order_id = $(this).attr('data-id');
    var btn = $('#client-orders-btn');
    btn.removeClass('btn-warning');
    btn.addClass('btn-success');
    btn.text('Заказ выбран');
    $('#base-order').val(base_order_id);
});

JS;

$this->registerJS($script, View::POS_READY);