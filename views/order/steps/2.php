<?php

use app\models\Order;
use kartik\typeahead\Typeahead;
use yii\helpers\Html;
use yii\web\View;

/* @var $model app\models\Order */
/* @var $client app\models\Client */

?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($client, 'firstname')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($client, 'lastname')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($client, 'phone')->widget(Typeahead::class, [
                'dataset' => [
                    [
                        'local' => $client->getPhones(),
                        'limit' => 10
                    ]
                ],
                'options' => [
                    'autocomplete' => 'off'
                ],
                'pluginEvents' => [
                    'typeahead:select' => 'function() {
                                        $.get("/client/search?phone="+$(this).val(), function(response){
                                            if(response.id != undefined){
                                                $("#order-clientname").val(response.firstname);
                                                $("#order-clientlastname").val(response.lastname);
                                                $("#order-clientemail").val(response.email);
                                                $("#order-clientsex").val(response.male);
                                                $("#order-clientdatebirth").val(response.birthday);
                                                $("#order-client_id").val(response.id);
                                                if(response.allergy == 1){
                                                    $("#order-clienthasallergic").prop("checked", true);
                                                } else {
                                                    $("#order-clienthasallergic").prop("checked", false);
                                                }
                                                $("#order-clientallergicdesc").val(response.allergy_name);
                                            }
                                        });
                                    }',
                    "typeahead:open" => "function() { console.log('typeahead:open'); }",
                    "typeahead:autocomplete" => "function() { console.log('typeahead:autocomplete'); }",
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($client, 'email')->textInput() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($client, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($client, 'male')->dropDownList(Order::sexLabels()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($client, 'birthday')->input('date') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($client, 'allergy')->checkbox(['id' => 'allergy-chk']) ?>
        </div>
    </div>

    <div class="row" id="desc-allergy" style="display: none;">
        <div class="col-md-12">
            <?= $form->field($client, 'allergy_name')->textarea([
                'rows' => 4,
            ]) ?>
        </div>
    </div>

    <div class="hidden">
        <?= $form->field($client, 'id') ?>
    </div>

<?= Html::a('<i class="fa fa-lg fa-arrow-circle-left"></i>&nbsp;&nbsp;Назад: Общая информация',
    ['step-back', 'id' => $model->id, 'value' => 1],
    ['class' => 'btn btn-primary pull-left']) ?>

<?php
$script = <<<JS
$(document).on('change', '#allergy-chk', function() {
        if ($(this).prop('checked')){
            $('#desc-allergy').slideToggle(300);
        } else {
           $('#desc-allergy').slideToggle(300);
        }
});

JS;

$this->registerJS($script, View::POS_READY);