<?php

/* @var $model app\models\Order */

use app\models\DirectionFlavor;
use app\models\ImportantFlover;
use app\models\IntensiveFlavor;
use app\models\Season;
use app\models\StyleFlavor;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $client app\models\Client */

$flavors_list = (new ImportantFlover)->getList();

Yii::info($model->likeFlavorFlower, 'test');


?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'current_flavors_count')->input('number', ['class' => 'form-control']) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'style_flavor_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(StyleFlavor::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'placeholder' => 'Выберите'
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'intensive_flavor_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(IntensiveFlavor::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'placeholder' => 'Выберите'
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'season_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Season::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'placeholder' => 'Выберите'
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'direction_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(DirectionFlavor::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'placeholder' => 'Выберите'
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4>Какие ароматы нравятся</h4>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
//        $formatJs = <<< JS
//                var formatRepo = function (repo) {
//                    console.log(repo);
//                    if (repo.loading) {
//                        return repo.text;
//                    }
//
//                    var markup =
//                '<div class="row">' +
//                    '<div class="col-sm-12">' +
//                        '<img src="'+repo.thumbnail+'" style="width: 30px; height: 30px; border-radius: 100%; border: 1px solid #cecece; object-fit: cover;"/>' +
//                        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 14px;">' + repo.name + '</p>' +
//                        '<p class="text-warning" style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 12px;">' + repo.rating + '</p>' +
//                    '</div>' +
//                '</div>';
//                    return '<div style="overflow:hidden;">' + markup + '</div>';
//                };
//                var formatRepoSelection = function (repo) {
//                    return repo.name;
//                }
//JS;
//
//        // Register the formatting script
//        $this->registerJs($formatJs, \yii\web\View::POS_HEAD);
//
//
//        // script to parse the results into the format expected by Select2
//        $resultsJs = "
//                            function (data, params) {
//                                data.items = Object.values(data.items);
//                                params.page = params.page || 1;
//                                return {
//                                    results: data.items,
//                                    pagination: {
//                                        more: (params.page * 30) < data.count
//                                    }
//                                };
//                            }
//                            ";
        ?>
        <?php
        $formatJs = <<< JS
var formatRepo = function (repo) {
    debugger;
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' +
    '<div class="col-sm-12">' +
        '<img src="'+repo.thumbnail+'" style="width: 30px; height: 30px; border-radius: 100%; border: 1px solid #cecece; object-fit: cover;"/>' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 14px;">' + repo.name + '</p>' +
        '<p class="text-warning" style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 12px;">' + repo.rating + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    $.get(
        "/perfume/get-name",
        {id:repo.id},
        function(response) {
            console.log('Response: ' + response);
           $("li[title='" + repo.id + "']").prepend(response);
        }
    );
    return repo.name;
}
JS;

        // Register the formatting script
        $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


        // script to parse the results into the format expected by Select2
        $resultsJs = "
            function (data, params) {
                data.items = Object.values(data.items);
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.count
                    }
                };
            }
            ";
        echo $form->field($model, 'likePerfume')->widget(Select2::className(), [
            'options' => ['placeholder' => 'Выберите парфюмы', 'multiple' => true],
            'pluginOptions' => [
                'ajax' => [
                    'url' => Url::toRoute(['perfume/search']),
                    'dataType' => 'json',
                    'delay' => 250,
                    'data' => new JsExpression('function(params) {
                                        return {q:params.term};
                                    }'),
                    'processResults' => new JsExpression($resultsJs),
                    'cache' => true
                ],
                'minimumInputLength' => 3,
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('formatRepo'),
                'templateSelection' => new JsExpression('formatRepoSelection'),
            ],
        ])?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'important_flavor')->widget(Select2::class, [
            'data' => $model->importantLabels(),
            'pluginOptions' => [
                'placeholder' => 'Выберите',
                'allowClear' => true,
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'likeFlavorFlower')->widget(Select2::class, [
            'data' => $flavors_list,
            'value' => $model->likeFlavorFlower,
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'placeholder' => 'Выберите',
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'likeFlavorFruit')->widget(Select2::class, [
            'data' => $flavors_list,
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'placeholder' => 'Выберите',
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'likeFlavorDrink')->widget(Select2::class, [
            'data' => $flavors_list,
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'placeholder' => 'Выберите',
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'likeFlavorFood')->widget(Select2::class, [
            'data' =>$flavors_list,
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'placeholder' => 'Выберите',
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'likeFlavorSpice')->widget(Select2::class, [
            'data' => $flavors_list,
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'placeholder' => 'Выберите',
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'likeFlavorTree')->widget(Select2::class, [
            'data' => $flavors_list,
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'placeholder' => 'Выберите',
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'notLikeFlavor')->widget(Select2::class, [
            'data' => $flavors_list,
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'placeholder' => 'Выберите',
            ],
        ]) ?>
    </div>
</div>

<?= Html::a('<i class="fa fa-lg fa-arrow-circle-left"></i>&nbsp;&nbsp;Назад: Анкета клиента',
    ['step-back', 'id' => $model->id, 'value' => 1],
    ['class' => 'btn btn-primary pull-left']) ?>

<?php

