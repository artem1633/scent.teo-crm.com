<?php

use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $client \app\models\Client */
/* @var $formulasSearchModel \app\models\FormulasSearch */
/* @var $formulasDataProvider \yii\data\ActiveDataProvider */
/* @var $orderIngredientSearchModel \app\models\OrderIngredientSearch */
/* @var $orderIngredientDataProvider \yii\data\ActiveDataProvider */
/* @var $stepSearchModel \app\models\OrderStepSearch */
/* @var $stepDataProvider \yii\data\ActiveDataProvider */

CrudAsset::register($this);

?>

    <div class="order-form">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="order-steps">
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 1 ? 'active' : ''; ?>" title="Общая информация">1</a>
                        <p class="step-label <?= $model->step != 1 ? 'hidden' : ''; ?>">Общая информация</p>
                    </div>
                    <div class="step">
                        <a class="step-num <?= $model->step == 2 ? 'active' : ''; ?>" title="Анкета клиента">2</a>
                        <p class="step-label <?= $model->step != 2 ? 'hidden' : ''; ?>">Анкета клиента</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 3 ? 'active' : ''; ?>"
                           title="Анкета аромата">3</a>
                        <p class="step-label <?= $model->step != 3 ? 'hidden' : ''; ?>">Анкета аромата</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 4 ? 'active' : ''; ?>"
                           title="Дегустация ингредиентов">4</a>
                        <p class="step-label <?= $model->step != 4 ? 'hidden' : ''; ?>">Дегустация ингредиентов</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 5 ? 'active' : ''; ?>"
                           title="Подбор формулы">5</a>
                        <p class="step-label <?= $model->step != 5 ? 'hidden' : ''; ?>">Подбор формулы</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 6 ? 'active' : ''; ?>"
                           title="Смешивание и дегустация">6</a>
                        <p class="step-label <?= $model->step != 6 ? 'hidden' : ''; ?>">Смешивание и дегустация</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 7 ? 'active' : ''; ?>"
                           title="Корректировка аромата">7</a>
                        <p class="step-label <?= $model->step != 7 ? 'hidden' : ''; ?>">Корректировка аромата</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 8 ? 'active' : ''; ?>"
                           title="Приготовление аромата">8</a>
                        <p class="step-label <?= $model->step != 8 ? 'hidden' : ''; ?>">Приготовление аромата</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 9 ? 'active' : ''; ?>" title="Оценка">9</a>
                        <p class="step-label <?= $model->step != 9 ? 'hidden' : ''; ?>">Оценка</p>
                    </div>
                    <div class="step">
                        <a href="#" class="step-num <?= $model->step == 10 ? 'active' : ''; ?>" title="История заказа">10</a>
                        <p class="step-label <?= $model->step != 10 ? 'hidden' : ''; ?>">История заказа</p>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <?php if(count($errors) > 0): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $errors[0][0] ?>
                    </div>
                <?php endif; ?>

                <?php $form = ActiveForm::begin(['id' => 'order-form', 'action' => "/order/update?id={$model->id}"]); ?>

                <h3 class="pull-left"><?= \yii\helpers\ArrayHelper::getValue($model->stepLabels(), $model->step) ?></h3>
                <div class="col-md-2 pull-right">
                    <?= Html::a('Списать', ['/remain/write-off', 'order_id' => $model->id], [
                        'class' => 'btn btn-block btn-warning pull-right',
                        'role' => 'modal-remote'
                    ]) ?>
                </div>
                <div class="clearfix"></div>
                <?php Yii::info('Order in form', 'test'); ?>
                <?php Yii::info($model->toArray(), 'test'); ?>
                <hr>
                <?php
                Yii::info('Is updated record', 'test');
                echo $this->render('steps/' . $model->step, [
                    'model' => $model,
                    'form' => $form,
                    'client' => $client ?? null,
                    'formulasSearchModel' => $formulasSearchModel ?? null,
                    'formulasDataProvider' => $formulasDataProvider ?? null,
                    'orderIngredientSearchModel' => $orderIngredientSearchModel ?? null,
                    'orderIngredientDataProvider' => $orderIngredientDataProvider ?? null,
                    'stepSearchModel' => $stepSearchModel ?? null,
                    'stepDataProvider' => $stepDataProvider ?? null,
                    'changeLogSearchModel' => $changeLogSearchModel ?? null,
                    'changeLogDataProvider' => $changeLogDataProvider ?? null,
                ]);
                ?>

                <?= Html::button('Далее: ' . $model->stepLabels()[$model->next_step] . '&nbsp;&nbsp;<i class="fa fa-lg fa-arrow-circle-right"></i>',
                    ['class' => 'btn btn-primary pull-right', 'type' => "submit"]) ?>

                <?= $form->field($model, 'step')->hiddenInput()->label(false); ?>
                <?= $form->field($model, 'next_step')->hiddenInput()->label(false); ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>


    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php
$script = <<<JS

$(document).ready(function() {
  

var old_value = '';
var new_value = '';
var step = "$model->step";

function log(new_value, type, name){
     $.post(
       'log',
       {
           order_id: "$model->id",
           step: step,
           type: type,
           name: name,
           old_value: old_value,
           new_value: new_value
       })
}

$(document).on('focus', 'input[type="text"], input[type="number"], textarea', function(){
    old_value = $(this).val();
    console.log('Old value: ' + old_value);
});

$(document).on('focusout', 'input[type="text"], input[type="number"], textarea', function(){
    new_value = $(this).val();
    var type = $(this).attr('type');
    var name = $(this).attr('name');
    log(new_value, type, name);
});

$(document).on('focus', 'select', function(){
        old_value = $('#' + $(this).attr('id') + ' option:selected').text();
        console.log('Old value: ' + old_value);
});
$(document).on('change', 'select', function(){
    new_value = $('#' + $(this).attr('id') + ' option:selected').text();
    // new_value = $(this).val();
    var type = 'select';
    var name = $(this).attr('name');
    log(new_value, type, name);
});

$(document).on('focus', 'input[type="radio"]', function(){
    old_value = $(this).val();
    console.log('Old value: ' + old_value);
});

$(document).on('focusout', 'input[type="radio"]', function(){
    new_value = $(this).val();
    var type = $(this).attr('type');
    var name = $(this).attr('name');
    // log(new_value, type, name);
});


$(document).on('mousedown', '.selection', function(){
    var parent = $(this).parent('span');
    var sel = parent.siblings('select');
    var select_id = sel.attr('id');
    //Текущее значение
    old_value = $('#' + select_id + ' option:selected').text();
    console.log('Old value: ' + old_value);
});

$(document).on('click', '.content button, .content a', function(){
    //При клике в форме отключаем предупреждение
     $(window).off('beforeunload');
});

 $(window).on('beforeunload', function(){
         return "Данные будут утеряны. Вы уверенные что хотите покинуть страницу?";
    });
})
JS;
$this->registerJs($script);
