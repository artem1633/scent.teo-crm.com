<?php

/* @var $this yii\web\View */

use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;


CrudAsset::register($this);

/* @var $model app\models\Order */

$this->title = 'Создание заказа';

?>
<div class="order-create">
    <?= $this->render('_form', [
        'model' => $model,
        'formulasSearchModel' => $formulasSearchModel,
        'formulasDataProvider' => $formulasDataProvider,
        'orderIngredientSearchModel' => $orderIngredientSearchModel,
        'orderIngredientDataProvider' => $orderIngredientDataProvider,
        'stepSearchModel' => $stepSearchModel,
        'stepDataProvider' => $stepDataProvider
    ]) ?>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>