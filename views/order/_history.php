<?php

use app\models\ChangeLog;
use app\models\OrderStep;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $searchModel app\models\OrderStepSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $changeLogDataProvider yii\data\ActiveDataProvider */


CrudAsset::register($this);

//$order_info = $model->getOrderInfo();

$ingredient_cost = $model->getIngredientCost();

$manager_name = $model->manager->name;

if (!$manager_name){
    $manager_name = $model->manager->login;
}
?>

<h3>Заказ №<?= $model->id ?></h3>

<div class="order-info">
    <p>Длительность заказа: <b><?= $model->getAllTime(); ?></b></p>
    <p>Клиент: <b><?= $model->client->getFullName(); ?></b></p>
    <p>Менеджер: <b><?= $manager_name; ?></b></p>
</div>
    <hr>
    <h3>Этапы заказа:</h3>
<?php
try {
    echo GridView::widget([
        'id' => 'crud-datatable-steps',
        'dataProvider' => $dataProvider,
        //                'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'id',
            // ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'step',
                'content' => function (OrderStep $model) {
                    if ((new Order())->stepLabels()[$model->step] ?? null) {
                        if ($model->isRepeated()){
                            return (new Order())->stepLabels()[$model->step] . ' (повторно)';
                        }
                        return (new Order())->stepLabels()[$model->step];
                    }
                    return null;
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'datetime',
                'label' => 'Длительность',
                'content' => function (OrderStep $model){
                    return $model->getDuration();
                }
            ],
        ],
        'panelBeforeTemplate' => '',
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after' => '',
        ]
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
} ?>

<hr>
<h3>Себестоимость ингредиентов: <?= $ingredient_cost['all'] ?></h3>

<ul>
    <?php foreach ($ingredient_cost['ingredients'] as $name => $info):?>
    <li>
        <?= $name; ?> - <?= $info['volume']; ?> / <?= $info['cost']; ?>
    </li>
    <?php endforeach; ?>
</ul>
<hr>
<h3>Журнал изменений</h3>
<?php
try {
    echo GridView::widget([
        'id' => 'crud-datatable-actions',
        'dataProvider' => $changeLogDataProvider,
        //                'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'step',
                'content' => function(ChangeLog $model){
                    return (new Order)->stepLabels()[$model->step] ?? null;
                },
                'label' => 'Раздел'

            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'field',

            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'action',
                'content' => function(ChangeLog $model){
                    return (new Order)->actionsLabels()[$model->action] ?? null;
                },

            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'old_value',

            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'new_value',

            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'event_time',
                'format' => 'datetime'

            ],
        ],
        'panelBeforeTemplate' => '',
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after' => '',
        ]
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
} ?>