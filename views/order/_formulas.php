<?php

use app\models\Formulas;
use app\models\Season;
use app\models\StyleFlavor;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var array $model Formulas[] */

try {
    echo GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        //    'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            [
                'class' => '\kartik\grid\RadioColumn',
                'radioOptions' => function ($model) {
                    return [
                        'data-id' => $model['id'],
                    ];
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'name',
                'label' => 'Наименование',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'directionFlovers',
                'label' => 'Направление',
                'value' => function (array $model) {
                    return (new Formulas())->getStringDirectionFlavors($model['id']) ?? null;
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'age_id',
                'label' => 'Возраст',
                'value' => function (array $model) {
                    return (new Formulas)->ageLabels($model['id']);
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'sex',
                'label' => 'Пол',
                'value' => function (array $model) {
                    return Formulas::sexLabels()[$model['sex']];
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'season_id',
                'label' => 'Сезон',
                'content' => function (array $model) {
                    return (new Season())->getNames($model['seasons']);
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'style_flavor_id',
                'label' => 'Стиль',
                'content' => function (array $model) {
                    return (new StyleFlavor())->getNames($model['style_flavors']);
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'description_emotion',
                'label' => 'Эмоциональное описание'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'label' => 'Рейтинг',
                'attribute' => 'rate',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'floversMatchPercent',
                'label' => 'Процент ароматов которые нравятся'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'notFloversMatchPercent',
                'label' => 'Процент ароматов которые не нравятся'
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign' => 'middle',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(["/formulas/{$action}", 'id' => $model['id']]);
                },
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-eye text-primary" style="font-size: 16px;"></i>', $url, [
                            'role' => 'modal-remote',
                            'title' => 'Просмотр',
                        ]);
                    }
                ],
            ],
        ],
        'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                ['role' => 'modal-remote', 'title' => 'Добавить формулу', 'class' => 'btn btn-success']) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => null,
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
}