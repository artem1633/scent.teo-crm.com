<?php


/* @var $this yii\web\View */

use app\models\Client;
use app\models\DirectionFlavor;
use app\models\ImportantFlover;
use app\models\IntensiveFlavor;
use app\models\Order;
use app\models\OrderFormulasIngredient;
use app\models\OrderImportantFlover;
use app\models\Price;
use app\models\PriceCost;
use app\models\Remain;
use app\models\Season;
use app\models\StyleFlavor;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\sortable\Sortable;
use kartik\typeahead\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $formulasSearchModel \app\models\FormulasSearch */
/* @var $formulasDataProvider \yii\data\ActiveDataProvider */
/* @var $orderIngredientDataProvider \yii\data\ActiveDataProvider */
/* @var $orderIngredientSearchModel \app\models\OrderIngredientSearch */
/* @var $stepSearchModel \app\models\OrderStepSearch */
/* @var $stepDataProvider \yii\data\ActiveDataProvider */


$clientData = ArrayHelper::getColumn(Client::find()->all(), 'phone');


if ($model->isNewRecord == false) {
    $model->likeFlavorFlower = ArrayHelper::getColumn(OrderImportantFlover::find()->where([
        'order_id' => $model->id,
        'type' => OrderImportantFlover::TYPE_FLOWER
    ])->all(), 'important_flover_id');
    $model->likeFlavorFruit = ArrayHelper::getColumn(OrderImportantFlover::find()->where([
        'order_id' => $model->id,
        'type' => OrderImportantFlover::TYPE_FRUIT
    ])->all(), 'important_flover_id');
    $model->likeFlavorDrink = ArrayHelper::getColumn(OrderImportantFlover::find()->where([
        'order_id' => $model->id,
        'type' => OrderImportantFlover::TYPE_DRINK
    ])->all(), 'important_flover_id');
    $model->likeFlavorFood = ArrayHelper::getColumn(OrderImportantFlover::find()->where([
        'order_id' => $model->id,
        'type' => OrderImportantFlover::TYPE_FOOD
    ])->all(), 'important_flover_id');
    $model->likeFlavorSpice = ArrayHelper::getColumn(OrderImportantFlover::find()->where([
        'order_id' => $model->id,
        'type' => OrderImportantFlover::TYPE_SPICE
    ])->all(), 'important_flover_id');
    $model->likeFlavorTree = ArrayHelper::getColumn(OrderImportantFlover::find()->where([
        'order_id' => $model->id,
        'type' => OrderImportantFlover::TYPE_TREE
    ])->all(), 'important_flover_id');
    $model->notLikeFlavor = ArrayHelper::getColumn(OrderImportantFlover::find()->where([
        'order_id' => $model->id,
        'type' => OrderImportantFlover::TYPE_NOT_LIKE
    ])->all(), 'important_flover_id');
}

$ingredientItems = [];

if ($model->isNewRecord == false) {
    /** @var OrderFormulasIngredient[] $formulasIngredients */
    $formulasIngredients = OrderFormulasIngredient::find()->where(['order_id' => $model->id])->orderBy('mark asc')->all();

    foreach ($formulasIngredients as $item) {
        $ing = $item->formulasIngredient->ingredient;

        $manager = $model->manager;
        $remain = \app\models\Remain::find()->where([
            'market_id' => $manager->market_id,
            'ingredient_id' => $ing->id
        ])->one();

        $otherInfo = '';
        $count = 0;

        if ($remain) {
            $count = $remain->amount;
        }

        if ($count <= 0) {
            $otherInfo = "<i class='text-danger pull-right'>(Нет на складе)</i>";
        } else {
            $otherInfo = "<span class='text-success pull-right'>" . Yii::$app->formatter->asDecimal($count) . "</span>";
        }


        $ingredientItems[] = ['content' => "<div class='ingredient-id' data-id='{$item->id}'>$ing->name_rus {$otherInfo}</div>"];
    }
}

?>

<div class="order-form">

    <?php $form = ActiveForm::begin(['id' => 'order-form', 'enableClientScript' => false]); ?>

    <div id="wizard" class="sw-main sw-theme-default">
        <!-- begin wizard-step -->
        <ul class="nav nav-tabs step-anchor">
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_START ? 'active' : '' ?>">
                <a href="#step-1" class="nav-link">
                    <span class="info">
                        1. Общая информация
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_CLIENT ? 'active' : '' ?>">
                <a href="#step-2" class="nav-link">
                    <span class="info">
                        2. Анкета клиента
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_FLOWER ? 'active' : '' ?>">
                <a href="#step-3" class="nav-link">
                    <span class="info">
                       3. Анкета аромата
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_INGREDIENTS ? 'active' : '' ?>">
                <a href="#step-3" class="nav-link">
                    <span class="info">
                        4. Дегустация ингредиентов
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_TASTING ? 'active' : '' ?>">
                <a href="#step-4" class="nav-link">
                    <span class="info">
                       5. Подбор формулы
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_MIXING_AND_TASTING ? 'active' : '' ?>">
                <a href="#step-3" class="nav-link">
                    <span class="info">
                       6. Смешивание и дегустация
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_CORRECT ? 'active' : '' ?>">
                <a href="#step-3" class="nav-link">
                    <span class="info">
                        7. Корректировка аромата
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_PREPARATION ? 'active' : '' ?>">
                <a href="#step-3" class="nav-link">
                    <span class="info">
                        8. Приготовление аромата
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_MARK ? 'active' : '' ?>">
                <a href="#step-3" class="nav-link">
                    <span class="info">
                        9. Оценка
                    </span>
                </a>
            </li>
            <li class="nav-item <?= $model->scenario == Order::SCENARIO_HISTORY ? 'active' : '' ?>">
                <a href="#step-3" class="nav-link">
                    <span class="info">
                        10. История заказа
                    </span>
                </a>
            </li>
        </ul>
        <!-- end wizard-step -->
        <!-- begin wizard-content -->
        <div class="sw-container tab-content" style="min-height: 348px;">
            <!-- begin step-1 -->
            <div id="step-1" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_START ? 'display: block' : 'display: none;' ?>">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'type')->dropDownList(Order::typeLabels(), [
                                    'class' => 'form-control for-calc',
                                    'id' => 'type-order'
                                ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Сумма заказа: <span id="total-price"><?= $model->price ?></span>&nbsp;руб.</h4>
                            </div>
                        </div>
                        <!--                        <div class="row">-->
                        <!--                            <div class="col-md-12">-->
                        <!--                                --><? //= Html::button('Расчитать', [
                        //                                        'id' => 'calculate-order',
                        //                                        'class' => 'btn btn-info',
                        //                                ]) ?>
                        <!--                            </div>-->
                        <!--                        </div>-->
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'solvent')->dropDownList([
                                    0 => 'Спирт',
                                    1 => 'ДПГ',
                                    2 => 'Воск'
                                ], [
                                    'id' => 'solvent-order',
                                    'prompt' => 'Выберите значение...',
                                ]) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'value')->dropDownList(PriceCost::getVolumesList(), [
                                    'id' => 'value-order',
                                    'class' => 'form-control for-calc',
                                    'prompt' => 'Выберите значение...',
                                ]) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'price_id')->dropDownList(Price::getList(), [
                                    'id' => 'concentration-order',
                                    'class' => 'form-control for-calc',
                                    'prompt' => 'Выберите значение...',
                                ]) ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row" style="display: flex; align-items: center;">
                    <div class="col-md-10">
                        <?= $form->field($model, 'old_client_id')->widget(Select2::className(), [
                            'data' => Client::getList(),
                            'options' => [
                                'id' => 'sel-clients',
                                'placeholder' => 'Выберите клиента...'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
//                            'pluginEvents' => [
//                                'select2:closing' => "function(){
//                                    var btn = $('#client-orders-btn');
//                                    if ($(this).val() > 0){
//                                        btn.attr('disabled', false);
//                                        btn.removeClass('disabled');
//                                    } else {
//                                        btn.attr('disabled', true);
//                                        btn.addClass('disabled');
//                                    }
//                                }"
//                            ],
                        ])->label('Клиент'); ?>
                    </div>
                    <div class="col-md-2">
                        <?= Html::a('Заказы клиента', ['client-orders'], [
                            'id' => 'client-orders-btn',
                            'class' => 'btn btn-info btn-block',
                            'role' => 'modal-remote',
                            'method' => 'post',
                        ]) ?>
                    </div>
                </div>


                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <!-- end step-1 -->
            <!-- begin step-2 -->
            <div id="step-2" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_CLIENT ? 'display: block' : 'display: none;' ?>">

                <?php

                if ($model->client_id != null) {
                    $client = Client::findOne($model->client_id);
                    $model->clientName = $client->firstname;
                    $model->clientLastName = $client->lastname;
                    $model->clientPhone = $client->phone;
                    $model->clientEmail = $client->email;
                    $model->clientDateBirth = $client->birthday;
                    $model->clientHasAllergic = $client->allergy;
                    $model->clientAllergicDesc = $client->allergy_name;
                }

                ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'clientName')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'clientLastName')->textInput() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?php if (count($clientData) > 0): ?>
                            <?= $form->field($model, 'clientPhone')->widget(Typeahead::class, [
                                'dataset' => [
                                    [
                                        'local' => $clientData,
                                        'limit' => 10
                                    ]
                                ],
                                'options' => [
                                    'autocomplete' => 'off'
                                ],
                                'pluginEvents' => [
                                    'typeahead:select' => 'function() {
                                        $.get("/client/search?phone="+$(this).val(), function(response){
                                            if(response.id != undefined){
                                                $("#order-clientname").val(response.firstname);
                                                $("#order-clientlastname").val(response.lastname);
                                                $("#order-clientemail").val(response.email);
                                                $("#order-clientsex").val(response.male);
                                                $("#order-clientdatebirth").val(response.birthday);
                                                $("#order-client_id").val(response.id);
                                                if(response.allergy == 1){
                                                    $("#order-clienthasallergic").prop("checked", true);
                                                } else {
                                                    $("#order-clienthasallergic").prop("checked", false);
                                                }
                                                $("#order-clientallergicdesc").val(response.allergy_name);
                                            }
                                        });
                                    }',
                                    "typeahead:open" => "function() { console.log('typeahead:open'); }",
                                    "typeahead:autocomplete" => "function() { console.log('typeahead:autocomplete'); }",
                                ],
                            ]) ?>
                        <?php else: ?>
                            <?= $form->field($model, 'clientPhone')->textInput() ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'clientEmail')->textInput() ?>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'clientSex')->dropDownList(Order::sexLabels()) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'clientDateBirth')->input('date') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'clientHasAllergic')->checkbox() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'clientAllergicDesc')->textarea(['rows' => 8]) ?>
                    </div>
                </div>

                <div class="hidden">
                    <?= $form->field($model, 'client_id') ?>
                </div>

                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton('Назад', [
                            'class' => 'btn btn-white',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(-1);
                            $("#order-form").submit();
                        '
                        ]) ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <!--            Анкета аромата-->
            <div id="step-3" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_FLOWER ? 'display: block' : 'display: none;' ?>">

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'current_flavors_count')->input('number') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'style_flavor_id')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(StyleFlavor::find()->all(), 'id', 'name'),
                            'pluginOptions' => [
                                'placeholder' => 'Выберите'
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'intensive_flavor_id')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(IntensiveFlavor::find()->all(), 'id', 'name'),
                            'pluginOptions' => [
                                'placeholder' => 'Выберите'
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'season_id')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(Season::find()->all(), 'id', 'name'),
                            'pluginOptions' => [
                                'placeholder' => 'Выберите'
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'direction_id')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(DirectionFlavor::find()->all(), 'id', 'name'),
                            'pluginOptions' => [
                                'placeholder' => 'Выберите'
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Какие ароматы нравятся</h4>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' +
    '<div class="col-sm-12">' +
        '<img src="'+repo.thumbnail+'" style="width: 30px; height: 30px; border-radius: 100%; border: 1px solid #cecece; object-fit: cover;"/>' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 14px;">' + repo.name + '</p>' +
        '<p class="text-warning" style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 12px;">' + repo.rating + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.name;
}
JS;

                        // Register the formatting script
                        $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                        // script to parse the results into the format expected by Select2
                        $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                        ?>
                        <?= $form->field($model, 'likePerfume')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Выберите парфюмы', 'multiple' => true],
                            'pluginOptions' => [
                                'ajax' => [
                                    'url' => Url::toRoute(['perfume/search']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'data' => new JsExpression('function(params) {
                                        return {q:params.term};
                                    }'),
                                    'processResults' => new JsExpression($resultsJs),
                                    'cache' => true
                                ],
                                'minimumInputLength' => 3,
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('formatRepo'),
                                'templateSelection' => new JsExpression('formatRepoSelection'),
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'importantFlavor')->widget(Select2::class, [
                            'data' => ['Бренд', 'Шлейф', 'Стойкость'],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                                'allowClear' => true,
                            ],
                        ])->label('Что для вас важно при выборе аромата') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'likeFlavorFlower')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'likeFlavorFruit')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'likeFlavorDrink')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'likeFlavorFood')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'likeFlavorSpice')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'likeFlavorTree')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'notLikeFlavor')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(ImportantFlover::find()->all(), 'id', 'name'),
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                </div>


                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton('Назад', [
                            'class' => 'btn btn-white',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(0);
                            $("#order-form").submit();
                        '
                        ]) ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <!--                //Анкета аромата-->

            <!--            Дегустация ингредиентов-->
            <div id="step-4" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_INGREDIENTS ? 'display: block' : 'display: none;' ?>">

                <?php
                try {
                    echo Sortable::widget([
                        'id' => 'ingredients',
                        'items' => $ingredientItems,
                        'pluginEvents' => [
                            'sortupdate' => 'function(e, e2){

                                var sortingArray = [];
                                $("#ingredients li").each(function(){
                                    var ingredientId = $(this).find(".ingredient-id").data("id");
                                    if(ingredientId != null){
                                        sortingArray.push(ingredientId);
                                    }
                                });

                                $.ajax({
                                    url: "/order/sorting-ingredients",
                                    method: "POST",
                                    data: {
                                        data: JSON.stringify(sortingArray),
                                        csrfParam: yii.getCsrfToken(),
                                    },
                                    success: function(response){
                                        console.log("response", response);
                                    },
                                });

    //                            console.log(sortingArray);
                            }',
                        ],
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                } ?>

                <?= $this->render('_formulas', [
                    'searchModel' => $formulasSearchModel,
                    'dataProvider' => $formulasDataProvider
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            </div>
            <!--                //Дегустация ингредиентов-->

            <!--            Подбор формулы-->
            <div id="step-5" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_TASTING ? 'display: block' : 'display: none;' ?>">

                <?php if ($model->formula_id != null): ?>
                    <h4>Ваша формула: <?= $model->formula->name ?></h4>
                <?php endif; ?>

                <!--                <div class="hidden">-->
                <?php if (in_array('formula_id', array_keys($model->errors))): ?>
                    <div class="alert alert-danger">
                        <b>Ошибка</b> <?= $model->errors['formula_id'][0] ?>
                    </div>
                <?php endif; ?>

                <div class="hidden">
                    <?= $form->field($model, 'formula_id')->hiddenInput() ?>
                </div>

                <?= $this->render('_formulas', [
                    'searchModel' => $formulasSearchModel,
                    'dataProvider' => $formulasDataProvider
                ]) ?>

                <?php

                $formulaScript = "
                    var grid = $('#crud-datatable');
                    
                    grid.on('grid.radiochecked', function(ev, key, val) {
                        $('#order-formula_id').val(14);
                    });
                    
                    grid.on('grid.radiocleared', function(ev, key, val) {
                       $('#order-formula_id').val('');    
                    });
                ";

                $this->registerJs($formulaScript, \yii\web\View::POS_READY);

                ?>
                <!--                </div>-->

                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton('Назад', [
                            'class' => 'btn btn-white',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(1);
                            $("#order-form").submit();
                        '
                        ]) ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <!--            //Подбор формулы-->

            <!--            Смешивание и дегустация-->
            <div id="step-6" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_MIXING_AND_TASTING ? 'display: block' : 'display: none;' ?>">

                <?php

                $likeEnableBtn = true;
                $orderIngredientDataProvider->pagination = false;

                if ($model->isNewRecord == false) {
                    foreach ($orderIngredientDataProvider->models as $orderIngredient) {
                        $ingredient = $orderIngredient->ingredient;

                        $count = 0;

                        $remain = Remain::find()->where([
                            'market_id' => $model->manager->market_id,
                            'ingredient_id' => $ingredient->id
                        ])->one();

                        if ($remain) {
                            $count = $remain->amount;
                        }

                        if ($count <= 0) {
                            $likeEnableBtn = false;
                            break;
                        }
                    }
                }

                ?>

                <?= $this->render('_ingredients', [
                    'searchModel' => $orderIngredientSearchModel,
                    'dataProvider' => $orderIngredientDataProvider,
                    'model' => $model,
                ]) ?>

                <div class="hidden">
                    <?= $form->field($model, 'nextHardStep')->hiddenInput() ?>
                </div>

                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton('Замена', [
                            'class' => 'btn btn-danger',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(2);
                            $("#order-form").submit();
                        '
                        ]) ?>
                        <?= Html::submitButton('Корретировка', [
                            'class' => 'btn btn-info',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(4);
                            $("#order-form").submit();
                        '
                        ]) ?>

                        <?php if ($likeEnableBtn): ?>
                            <?= Html::submitButton('Нравится', [
                                'class' => 'btn btn-primary',
                                'onclick' => '
                                event.preventDefault();
                                $("#order-nexthardstep").val(5);
                                $("#order-form").submit();
                            '
                            ]) ?>
                        <?php else: ?>
                            <?= Html::a('Нравиться', '#', [
                                'class' => 'btn btn-default',
                                'onclick' => '
                                event.preventDefault();
                                alert("На складе нет ингредиентов");
                            '
                            ]) ?>
                        <?php endif; ?>
                    </div>
                <?php } ?>
            </div>
            <!--            //Смешивание и дегустация-->

            <!--            Корректировкка аромата-->
            <div id="step-7" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_CORRECT ? 'display: block' : 'display: none;' ?>">

                <?= $this->render('_ingredients', [
                    'searchModel' => $orderIngredientSearchModel,
                    'dataProvider' => $orderIngredientDataProvider,
                    'model' => $model,
                ]) ?>

                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton('Замена', [
                            'class' => 'btn btn-danger',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(2);
                            $("#order-form").submit();
                        '
                        ]) ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <!--            //Корректировкка аромата-->

            <!--            Приготовление аромата-->
            <div id="step-8" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_PREPARATION ? 'display: block' : 'display: none;' ?>">

                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton('Назад', [
                            'class' => 'btn btn-white',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(3);
                            $("#order-form").submit();
                        '
                        ]) ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <!--            //Приготовление аромата-->

            <!--            Оценка-->
            <div id="step-9" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_MARK ? 'display: block' : 'display: none;' ?>">

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'client_mark')->dropDownList([
                            1 => 1,
                            2 => 2,
                            3 => 3,
                            4 => 4,
                            5 => 5,
                            6 => 6,
                            7 => 7,
                            8 => 8,
                            9 => 9,
                            10 => 10,
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'mark_client_comment')->textarea() ?>
                    </div>
                </div>

                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton('Назад', [
                            'class' => 'btn btn-white',
                            'onclick' => '
                            event.preventDefault();
                            $("#order-nexthardstep").val(5);
                            $("#order-form").submit();
                        '
                        ]) ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Далее',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <!--            //Оценка-->

            <!--            История заказа-->
            <div id="step-10" class="tab-pane step-content"
                 style="<?= $model->scenario == Order::SCENARIO_HISTORY ? 'display: block' : 'display: none;' ?>">

                <?= $this->render('_history', [
                    'searchModel' => $stepSearchModel,
                    'dataProvider' => $stepDataProvider
                ]) ?>

            </div>
            <!--            //История заказа-->

            <div class="row">

                <div class="col-md-2 pull-right"
                     style="<?= $model->scenario != Order::SCENARIO_START ? 'display: block' : 'display: none;' ?>">
                    <?= Html::a('Списать', ['/remain/write-off', 'order_id' => $model->id], [
                        'class' => 'btn btn-block btn-warning pull-right',
                    ]) ?>
                </div>
            </div>

            <!-- end step-1 -->
            <!-- begin step-2 --
            <!-- end step-4 -->
            <!-- end wizard-content -->
        </div>

        <div class="hidden">
            <?= $form->field($model, 'step')->hiddenInput() ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
