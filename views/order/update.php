<?php


/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $client app\models\Client */
/* @var $formulasSearchModel \app\models\FormulasSearch */
/* @var $formulasDataProvider \yii\data\ActiveDataProvider */
/* @var $orderIngredientSearchModel \app\models\OrderIngredientSearch */
/* @var $orderIngredientDataProvider \yii\data\ActiveDataProvider */
/* @var $stepSearchModel \app\models\OrderStepSearch */
/* @var $stepDataProvider \yii\data\ActiveDataProvider */

$this->title = "Заказ №{$model->id}";

Yii::info('Step: ' . $model->step, 'test');
Yii::info('Next step: ' . $model->next_step, 'test');

//Пишем в логи начало шага
$model->loggingStep();

?>
<div class="order-update">

    <?= $this->render('_form2', [
        'model' => $model,
        'errors' => $errors,
        'formulasSearchModel' => $formulasSearchModel ?? null,
        'formulasDataProvider' => $formulasDataProvider ?? null,
        'orderIngredientSearchModel' => $orderIngredientSearchModel ?? null,
        'orderIngredientDataProvider' => $orderIngredientDataProvider ?? null,
        'stepSearchModel' => $stepSearchModel ?? null,
        'stepDataProvider' => $stepDataProvider ?? null,
        'client' => $client ?? null,
        'changeLogSearchModel' => $changeLogSearchModel ?? null,
        'changeLogDataProvider' => $changeLogDataProvider ?? null,
    ]) ?>

</div>
