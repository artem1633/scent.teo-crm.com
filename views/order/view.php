<?php

use app\models\Order;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
?>
<div class="order-view">
    <h3>Заказ №<?= $model->id; ?></h3>
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'type',
                    'value' => $model::typeLabels()[$model->type],
                ],
                [
                    'attribute' => 'solvent',
                    'value' => $model::solventLabels()[$model->solvent],
                ],
                'value',
                [
                    'attribute' => 'all_time',
                    'value' => $model->getAllTime(),
                ],
                'created_at',
                'formula_id',
                'price',
                'prime_cost',
                'client_mark',
                'mark_client_comment',
                [
                    'attribute' => 'client_name',
                    'value' => function(Order $model){
                        $client = $model->client ?? null;
                        if ($client){
                            \Yii::info($model->client, 'test');
                            return $model->client->getFullName() ?? null;
                        }
                        return null;
                    },
                ],
                [
                    'attribute' => 'manager_id',
                    'value' => function(Order $model){
                        $client = $model->manager ?? null;
                        if ($client){
                            \Yii::info($model->manager, 'test');
                            return $model->manager->name;
                        }
                        return null;
                    },
                ],
                [
                    'attribute' => 'formula_name',
                    'value' => $model->formula->name ?? null,
                ],
                [
                    'attribute' => 'manager_name',
                    'value' => $model->manager->name ?? null,
                ],
                'intensive_flavor_id',
                [
                    'attribute' => 'season_id',
                    'value' => $model->season->name ?? null,
                ],
                [
                    'attribute' => 'direction_id',
                    'value' => $model->direction->name ?? null,
                ],
                [
                    'attribute' => 'market_id',
                    'value' => $model->market->name ?? null,
                ],
                'created_at',

            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
