<?php

use app\models\OrderIngredient;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Remain;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FormulasIngredientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Formulas */

try {
    echo GridView::widget([
        'id' => 'crud-order-datatable',
        'dataProvider' => $dataProvider,
        //            'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'id',
            // ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'can_delete',
                'label' => 'Можно удалить',
                'content' => function (OrderIngredient $model) {
                    if ($model->can_delete) {
                        return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
                    } else {
                        return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
                    }
                },
                'width' => '10px',
                'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient_id',
                'value' => 'ingredient.name_rus',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'dilute',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'count',
                'format' => 'decimal'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                //            'attribute'=>'count',
                'label' => 'Кол-во на складе',
                'content' => function ($data) use ($model) {
                    if ($model->isNewRecord) {
                        return '';
                    }

                    $ingredient = $data->ingredient;

                    $count = 0;

                    $remain = Remain::find()->where([
                        'market_id' => $model->manager->market_id,
                        'ingredient_id' => $ingredient->id
                    ])->one();

                    if ($remain) {
                        $count = $remain->amount;
                    }

                    $count = Yii::$app->formatter->asDecimal($count);

                    $output = $count;

                    if ($count <= 0) {
                        $output = "<span class='text-danger'>{$count}</span>";
                    }

                    return $output;
                },
                'format' => 'decimal'
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'is_main',
                'value' => function ($model) {
                    return ArrayHelper::getValue(\app\models\FormulasIngredient::isMainLabels(), $model->is_main);
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient.description',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'ingredient.testable',
                'content' => function ($model) {
                    if ($model->ingredient->testable) {
                        return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
                    } else {
                        return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
                    }
                },
                'hAlign' => GridView::ALIGN_CENTER,
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'comment',
                'format' => 'text',
                'width' => '300px;',
                'contentOptions' => ['style' => 'white-space: pre-line; word-break: break-all;'],
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign' => 'middle',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(["order-ingredient/{$action}", 'id' => $key]);
                },
                'template' => '{update}{delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                            'role' => 'modal-remote',
                            'title' => 'Удалить',
                            'data-confirm' => false,
                            'data-method' => false,// for overide yii data api
                            'data-request-method' => 'post',
                            'data-confirm-title' => 'Вы уверены?',
                            'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                                'role' => 'modal-remote',
                                'title' => 'Изменить',
                                'data-confirm' => false,
                                'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                            ]) . "&nbsp;";
                    }
                ],
            ],
        ],
        'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>',
                ['order-ingredient/create', 'order_id' => $model->id],
                ['role' => 'modal-remote', 'title' => 'Добавить формулу', 'class' => 'btn btn-success']) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
        ],
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
} ?>