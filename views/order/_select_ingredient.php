<?php

use kartik\select2\Select2;

try {
    echo Select2::widget([
        'name' => 'select-ingredient-4',
        'data' => (new \app\models\Ingredient())->getList(),
        'options' => [
            'id' => 'select-ingredient-4',
            'placeholder' => 'Выберите ингредиент...'
        ],
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
}

