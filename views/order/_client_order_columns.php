<?php

use app\models\Order;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'solvent',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'value',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'value' => function ($model) {
            return $model->costPrice;
        },
        'label' => 'Цена',
        'format' => ['currency', 'rub'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'market_id',
        'value' => 'market.name'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'formula_id',
        'value' => 'formula.name'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'content' => function (Order $model){
            return Html::button('Выбрать', [
                'id' => 'select-order-btn',
                'data-id' => $model->id,
                'class' => 'btn btn-success',
                'data-dismiss'=>'modal',
            ]);
        }
    ],


];   