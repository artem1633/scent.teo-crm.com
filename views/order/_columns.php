<?php

use app\models\Order;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'type' => DatePicker::TYPE_INPUT,
            'options' => [
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ],
        'format' => 'datetime',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_name',
        'content' => function (Order $model) {
            $client = $model->client ?? null;
            if ($client){
                \Yii::info($model->client, 'test');
                return $model->client->getFullName() ?? null;
            }
           return null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'formula_name',
        'value' => 'formula.name'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'price',
        'label' => 'Стоимость',
        'format' => ['currency', 'rub'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'prime_cost',
        'label' => 'Себестоимость',
        'format' => ['currency', 'rub'],
        'value' => function (Order $model) {
            return $model->costPrice;
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'manager_name',
        'value' => 'manager.name'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'all_time',
        'value' => function (Order $model) {
            return $model->getAllTime();
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_mark',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{view}&nbsp;{update}&nbsp;{delete}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-primary" style="font-size: 16px;"></i>', $url, [
                        'title' => 'Просмотр',
                        'data-pjax' => 0,
                    ]);
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Удалить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
//                        'role' => 'modal-remote',
                        'title' => 'Изменить',
                        'data-pjax' => 0,
//                        'data-confirm' => false,
//                        'data-method' => false,// for overide yii data api
//                        'data-request-method' => 'post',
                    ]);
            }
        ],
    ],

];   