<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $searchModel app\models\ClientSearch*/
/* @var $dataProvider \yii\data\ActiveDataProvider */

Yii::info((file_exists(Yii::$app->basePath . "/views/client/_columns.php"))?"Существует":"Не существует", '_test')

?>

<?php
try {
    echo GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_client_order_columns.php'),
        'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                ['data-pjax' => '0', 'title' => 'Добавить заказ', 'class' => 'btn btn-success']) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
        ]
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
} ?>

