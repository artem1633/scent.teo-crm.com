<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Parfume */
?>
<div class="parfume-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
