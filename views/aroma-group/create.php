<?php

/* @var $this yii\web\View */
/* @var $model app\models\AromaGroup */

?>
<div class="aroma-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
