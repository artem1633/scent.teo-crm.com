<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AromaGroup */
?>
<div class="aroma-group-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                [
                    'attribute' => 'parent_id',
                    'value' => $model->parent->name ?? null,
                    'label' => 'Оснвная группа'
                ]
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
