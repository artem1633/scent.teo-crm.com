<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderStep */
?>
<div class="order-step-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
