<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrderStep */

?>
<div class="order-step-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
