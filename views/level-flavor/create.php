<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LevelFlavor */

?>
<div class="level-flavor-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
