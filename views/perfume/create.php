<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Perfume */

?>
<div class="perfume-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
