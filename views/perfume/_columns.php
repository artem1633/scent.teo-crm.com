<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Perfume;
use yii\helpers\ArrayHelper;
use app\models\PerfumeImportantFlover;
use app\models\ImportantFlover;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($model){

            $output = '<img src="'.$model->thumbnail.'" style="width: 30px; height: 30px; object-fit: cover; border-radius: 100%; border: 1px solid #cecece;"> ';

            $output .= $model->name;

            return $output;
        },
        'vAlign' => GridView::ALIGN_MIDDLE,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sex',
        'value' => function($model){
            if($model->sex == Perfume::SEX_MALE){
                return 'Мужской';
            } elseif($model->sex == Perfume::SEX_FEMALE){
                return 'Женский';
            } elseif($model->sex == Perfume::SEX_UNISEX){
                return 'Унисекс';
            }

            return null;
        },
        'vAlign' => GridView::ALIGN_MIDDLE,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rating',
        'vAlign' => GridView::ALIGN_MIDDLE,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Ароматы',
        'content' => function($model){
            $output = '';
            $map = ArrayHelper::map(PerfumeImportantFlover::find()->where(['perfume_id' => $model->id])->all(),'important_flover_id', 'rating');

            $pks = array_keys($map);

            $flovers = ImportantFlover::findAll($pks);

            foreach ($flovers as $flover){
                $output .= "<p style='margin-bottom: 3px;'>{$flover->name} <span class='text-warning' title='Рейтинг'>{$map[$flover->id]}</span></p>";
            }

            return $output;
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'year',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'url',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   