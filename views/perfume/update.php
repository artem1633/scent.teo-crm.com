<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Perfume */
?>
<div class="perfume-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
