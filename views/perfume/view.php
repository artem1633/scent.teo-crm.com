<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Perfume */
?>
<div class="perfume-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'external_id',
            'sex',
            'thumbnail',
            'rating',
            'year',
            'url:url',
        ],
    ]) ?>

</div>
