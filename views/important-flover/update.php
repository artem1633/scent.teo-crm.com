<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ImportantFlover */
?>
<div class="important-flover-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
