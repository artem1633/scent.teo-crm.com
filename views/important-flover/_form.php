<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\ImportantFlover;

/* @var $this yii\web\View */
/* @var $model app\models\ImportantFlover */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="important-flover-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group')->widget(Select2::class, [
        'data' => ImportantFlover::groupLabels(),
        'pluginOptions' => [
            'placeholder' => 'Выберите группу',
        ],
    ]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
