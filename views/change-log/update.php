<?php

/* @var $this yii\web\View */
/* @var $model app\models\ChangeLog */
?>
<div class="change-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
