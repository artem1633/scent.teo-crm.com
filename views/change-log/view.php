<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ChangeLog */
?>
<div class="change-log-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'order_id',
                'step',
                'field',
                'action',
                'old_value:ntext',
                'new_value:ntext',
                'event_time',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
