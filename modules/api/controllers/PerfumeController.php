<?php

namespace app\modules\api\controllers;

use app\helpers\ImportPerfume;
use app\models\Ingredient;
use app\models\Perfume;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class PerfumeController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionParse()
    {
        $count = Perfume::find()->count();

//        $iterator = ceil(57399 / 30) - ceil($count / 30);
        $iterator = ceil($count / 30);

        for ($i = 0; $i < 30; $i++) {
            $page = $iterator + ($i + 1);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,
                "https://fgvi612dfz-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20instantsearch.js%20(3.7.0)%3B%20Vue%20(2.6.10)%3B%20Vue%20InstantSearch%20(2.6.0)%3B%20JS%20Helper%20(2.28.0)&x-algolia-application-id=FGVI612DFZ&x-algolia-api-key=NTFjYjQ5MmExN2ZlNWU2M2YwN2NjYTc3YmE2ZDQzODQxZjMwYjA2MTY1MjE0YmUzNDM3MjVjZjliN2E3MjVhN3ZhbGlkVW50aWw9MTU3Nzg2Mzc5MA%3D%3D");
//        curl_setopt($ch, CURLOPT_URL,"https://fgvi612dfz-dsn.algolia.net/1/indexes/fragrantica_perfumes/611?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20instantsearch.js%20(3.7.0)%3B%20Vue%20(2.6.10)%3B%20Vue%20InstantSearch%20(2.6.0)%3B%20JS%20Helper%20(2.28.0)&x-algolia-application-id=FGVI612DFZ&x-algolia-api-key=NTFjYjQ5MmExN2ZlNWU2M2YwN2NjYTc3YmE2ZDQzODQxZjMwYjA2MTY1MjE0YmUzNDM3MjVjZjliN2E3MjVhN3ZhbGlkVW50aWw9MTU3Nzg2Mzc5MA%3D%3D");
//        curl_setopt($ch, CURLOPT_URL,"https://fgvi612dfz-dsn.algolia.net/1/indexes/fragrantica_perfumes/facets/ingredients.RU/query?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20instantsearch.js%20(3.7.0)%3B%20Vue%20(2.6.10)%3B%20Vue%20InstantSearch%20(2.6.0)%3B%20JS%20Helper%20(2.28.0)&x-algolia-application-id=FGVI612DFZ&x-algolia-api-key=NTFjYjQ5MmExN2ZlNWU2M2YwN2NjYTc3YmE2ZDQzODQxZjMwYjA2MTY1MjE0YmUzNDM3MjVjZjliN2E3MjVhN3ZhbGlkVW50aWw9MTU3Nzg2Mzc5MA%3D%3D");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                '{"requests":[{"indexName":"fragrantica_perfumes","params": "query=&hitsPerPage=80&page=' . $page . '&attributesToRetrieve=*"}]}');

//        curl_setopt($ch, CURLOPT_POSTFIELDS, '{"params": ""}');

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = json_decode(curl_exec($ch), true);

//        curl_close ($ch);

//            $names = [];

            $exists = 0;
            $saved = 0;


            foreach ($server_output['results'][0]['hits'] as $item) {
                $checkModel = Perfume::find()->where(['name' => $item['naslov']])->one();
                if ($checkModel) {
                    $exists++;
                    continue;
                }

                $sex = null;

                if ($item['spol'] == 'male') {
                    $sex = Perfume::SEX_MALE;
                } else {
                    if ($item['spol'] == 'female') {
                        $sex = Perfume::SEX_FEMALE;
                    } else {
                        if ($item['spol'] == 'unisex') {
                            $sex = Perfume::SEX_UNISEX;
                        }
                    }
                }

                $url = null;

                if (isset($item['url']['RU'][0])) {
                    $url = $item['url']['RU'][0];
                }

                $perfume = new Perfume([
                    'external_id' => $item['id'],
                    'name' => $item['naslov'],
                    'sex' => $sex,
                    'year' => $item['godina'],
                    'rating' => $item['rating'],
                    'thumbnail' => $item['thumbnail'],
                    'url' => $url,
                ]);

                if ($perfume->save(false)) {
                    $saved++;
                };

                $names[] = $item['naslov'];
            }


            $countNow = Perfume::find()->count();


            echo "Saved: {$count}<br>Exists: {$exists}<br>Count now: {$countNow}<br>Page {$iterator}";
        }
    }

    /**
     * Импорт парфюмов из файлов
     * @param string $file_name Имя файла
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionImport($file_name = '')
    {
        $result = (new ImportPerfume)->import($file_name);

        echo 'Импорт завершен<br>'
            . 'Обработано файлов: ' . $result['count_files'] . '<br>'
            . 'Добавлено записей: ' . $result['count_items'] . '<br>'
            . 'Пропущено записей: ' . $result['skip_items'] . '<br>';

        if (count($result['errors']) > 0) {
            echo 'Ошибки импорта:<br>';
            VarDumper::dump($result['errors'], 10, true);
            die;
        }

        return true;
    }

    /**
     *
     */
    public function actionCorrectIngredientInputLevel()
    {
        /** @var Ingredient[] $ingredients */
        $ingredients = Ingredient::find()->where(['>', 'input_level', 100])->all();

//                VarDumper::dump($ingredients, 10, true);
//        exit;

        $counter = 0;
        foreach ($ingredients as $ingredient){
            $ingredient->input_level = $ingredient->input_level / 100;
            if($ingredient->save(false)){
                $counter++;
            }
        }

        echo "ingredients: ".count($ingredients).", changed: {$counter}";
    }

    /**
     * Очистка таблиц perfume_important_flover, perfume_to_perfume_accord от дублей
     */
    public function actionClean()
    {

    }
}
